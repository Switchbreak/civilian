package Minigames.Blockade 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.World;
	import net.flashpunk.graphics.Image;
	import UI.DialogBox;
	import mx.utils.StringUtil;
	import mx.resources.ResourceManager;
	import UI.FloaterText;
	
	/**
	 * World object for the Blockade minigame
	 * 
	 * @author Switchbreak
	 */
	public class Blockade extends World 
	{
		// Entities
		private var player:Player;
		
		// Static variables
		public static var sim:Sim;
		public static var paused:Boolean;
		
		/**
		 * Create and initialize blockade
		 * 
		 * @param setSim
		 * Sim object that is running the game
		 * @param eventEscaped
		 * Callback function to handle minigame win event
		 */
		public function Blockade( setSim:Sim, eventEscaped:Function = null ) 
		{
			// Set values
			paused = false;
			sim = setSim;
			sim.EventFamilyDied = EventDeath;
			sim.EventLost = EventLost;
			
			// Add player entity
			player = new Player( 10, 50, eventEscaped );
			add( player );
			
			// Add borders
			add( new Wall( 0, 0, FP.width, 1 ) );
			add( new Wall( 0, FP.height - 1, FP.width, 1 ) );
			add( new Wall( 0, 0, 1, FP.height ) );
			add( new Wall( FP.width - 1, 0, 1, FP.height ) );
			
			// Add walls
			add( new Wall( 100, 0, 40, 340 ) );
			add( new Wall( 100, 440, 40, 40 ) );
			add( new Wall( 500, 0, 40, 40 ) );
			add( new Wall( 500, 140, 40, 340 ) );
			
			// Add obstacles
			add( new Wall( 225, 50, 50, 50 ) );
			add( new Wall( 375, 50, 50, 50 ) );
			add( new Wall( 225, 200, 50, 50 ) );
			add( new Wall( 375, 200, 50, 50 ) );
			add( new Wall( 225, 350, 50, 50 ) );
			add( new Wall( 375, 350, 50, 50 ) );
			
			// Add goal
			var goal:Entity = new Entity( 550, 420, Image.createRect( 80, 50 ) );
			goal.type = Player.GOAL_COLLISION_TYPE;
			goal.active = false;
			goal.setHitboxTo( goal.graphic );
			add( goal );
			
			// Add enemy guards
			add( new Guard( 175, 15, 455, 435 ) );
			add( new Guard( 455, 435, 175, 15 ) );
			
			// Add enemy guard dogs
			add( new Dog( 310, 145 ) );
			add( new Dog( 310, 195 ) );
		}
		
		//
		// Events
		//
		
		/**
		 * This function is called to inform the player that a family member has
		 * died
		 */
		public function EventDeath( deceasedName:String ):void
		{
			add( new FloaterText( player.x + player.halfWidth, player.y, StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "DIALOG_FAMILY_DIED" ), deceasedName ) ) );
		}
		
		/**
		 * This function is called when the player has lost
		 */
		public function EventLost():void
		{
			paused = true;
			
			var lostDialog:DialogBox = new DialogBox( ResourceManager.getInstance().getString( "resources", "DIALOG_LOST" ) );
			add( lostDialog );
		}
		
		/**
		 * This function is called when the player cancels out of a dialog box
		 */
		public function CancelDialog():void
		{
			paused = false;
		}
	}

}