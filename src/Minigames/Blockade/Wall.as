package Minigames.Blockade 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	
	/**
	 * Defines a wall entity to block player movement
	 * 
	 * @author Switchbreak
	 */
	public class Wall extends Entity 
	{
		
		// Constants
		private static const WALL_COLOR:uint = 0xFFD6CC;
		
		/**
		 * Creates and initializes the wall entity
		 */
		public function Wall( setX:int, setY:int, setWidth:int, setHeight:int )
		{
			// Set object values
			x = setX;
			y = setY;
			active = false;
			
			// Create wall graphic
			graphic = Image.createRect( setWidth, setHeight, WALL_COLOR );
			
			// Set collision type and hitbox
			type = Player.WALL_COLLISION_TYPE;
			setHitboxTo( graphic );
		}
		
	}

}