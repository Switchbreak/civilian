package Minigames.Blockade 
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.motion.LinearPath;
	
	/**
	 * Patrolling guard entity
	 * 
	 * @author Switchbreak
	 */
	public class Guard extends Entity 
	{
		// Constants
		private static const GUARD_COLOR:uint = 0xFFFF00;
		private static const GUARD_SIZE:int = 20;
		private static const PATROL_SPEED:Number = 150;
		private static const FIRING_RATE:Number = 0.2;
		private static const REACTION_TIME:Number = 0.1;
		private static const SIGHT_LINE_WIDTH:Number = 1;
		private static const TARGET_TYPES:Array = [Player.PLAYER_COLLISION_TYPE];
		
		// Private variables
		private var patrol:LinearPath;
		private var facingX:int;
		private var facingY:int;
		private var fireCooldown:Number = REACTION_TIME;
		
		/**
		 * Create and initialize the guard entity
		 * 
		 * @param	setX
		 * Initial position of the guard and top-left corner of patrol route
		 * @param	setY
		 * Initial position of the guard and top-left corner of patrol route
		 * @param	setPatrolCornerX
		 * Bottom right corner of patrol route
		 * @param	setPatrolCornerY
		 * Bottom right corner of patrol route
		 */
		public function Guard( setX:int, setY:int, patrolCornerX:int, patrolCornerY:int )
		{
			// Create guard and set hitbox
			super( setX, setY, Image.createRect( GUARD_SIZE, GUARD_SIZE, GUARD_COLOR ) );
			type = Player.GUARD_COLLISION_TYPE;
			setHitboxTo( graphic );
			
			// Initialize patrol waypoint list
			patrol = CreateRectangularPatrol( x, y, patrolCornerX, patrolCornerY );
			addTween( patrol );
			patrol.setMotionSpeed( PATROL_SPEED );
		}
		
		/**
		 * Guard movement
		 */
		override public function update():void 
		{
			if ( !Blockade.paused )
			{
				// Move guard along patrol
				x = patrol.x;
				y = patrol.y;
				
				// If a target has been spotted, stop patrolling and shoot
				var target:Entity = TargetSpotted();
				if ( target )
				{
					// Stop patrolling
					patrol.active = false;
					
					// Fire bullets at target at specified rate
					if ( fireCooldown <= 0 )
					{
						Shoot();
						fireCooldown = FIRING_RATE;
					}
					else
					{
						fireCooldown -= FP.elapsed;
					}
				}
				else
				{
					fireCooldown = REACTION_TIME;
					patrol.active = true;
				}
			}
			
			super.update();
		}
		
		/**
		 * Checks if a target is in line of sight of the guard
		 * 
		 * @return
		 * First target spotted, if any. If none, returns null
		 */
		private function TargetSpotted():Entity
		{
			// Get list of all entities in the world with types that the guard
			// has in the target types list
			var targets:Vector.<Entity> = new Vector.<Entity>();
			for each( var targetType:String in TARGET_TYPES )
			{
				FP.world.getType( targetType, targets );
			}
			
			// Check if any targets are in line of sight
			var targetSpotted:Entity = null;
			for each( var target:Entity in targets )
			{
				// Check for line of sight along vertical axis by checking if
				// the target collides with a vertically aligned rectangle that
				// spans the whole screen height at the guard's x coordinate.
				if ( target.collideRect( target.x, target.y, x + halfWidth - (SIGHT_LINE_WIDTH >> 1), 0, SIGHT_LINE_WIDTH, FP.height ) )
				{
					// Check if the line of sight is unbroken by walls
					if ( FP.world.collideLine( Player.WALL_COLLISION_TYPE, x + halfWidth, y + halfHeight, x, target.y ) == null )
					{
						// Set target spotted
						targetSpotted = target;
						
						// Set facing to face target
						facingX = 0;
						facingY = (target.y > y ? 1 : -1);
						
						break;
					}
				}
				
				// Do a similar check along the horizontal axis.
				if ( target.collideRect( target.x, target.y, 0, y + halfHeight - (SIGHT_LINE_WIDTH >> 1), FP.width, SIGHT_LINE_WIDTH ) )
				{
					// Check if the line of sight is unbroken by walls
					if ( FP.world.collideLine( Player.WALL_COLLISION_TYPE, x + halfWidth, y + halfHeight, target.x, y ) == null )
					{
						// Set target spotted
						targetSpotted = target;
						
						// Set facing to face target
						facingX = (target.x > x ? 1 : -1);
						facingY = 0;
						
						break;
					}
				}
			}
			
			return targetSpotted;
		}
		
		/**
		 * Fires a bullet at a target
		 */
		private function Shoot():void
		{
			FP.world.add( new Bullet( x + halfWidth - (Bullet.BULLET_SIZE >> 1), y + halfHeight - (Bullet.BULLET_SIZE >> 1), facingX, facingY ) );
		}
		
		/**
		 * Creates a set of waypoints for patrolling a rectangle
		 * 
		 * @param	x1
		 * Coordinates of the top-left corner of the patrol rectangle
		 * @param	y1
		 * Coordinates of the top-left corner of the patrol rectangle
		 * @param	x2
		 * Coordinates of the bottom-right corner of the patrol rectangle
		 * @param	y2
		 * Coordinates of the bottom-right corner of the patrol rectangle
		 */
		public static function CreateRectangularPatrol( x1:int, y1:int, x2:int, y2:int, clockwise:Boolean = true ):LinearPath
		{
			// Create new waypoint list
			var patrol:LinearPath = new LinearPath( null, Tween.LOOPING );
			
			// Check if patrol is clockwise or counterclockwise
			if ( clockwise )
			{
				// Add the four corners of the rectangle in clockwise order
				patrol.addPoint( x1, y1 );
				patrol.addPoint( x2, y1 );
				patrol.addPoint( x2, y2 );
				patrol.addPoint( x1, y2 );
				patrol.addPoint( x1, y1 );
			}
			else
			{
				// Add the corners in counterclockwise order
				patrol.addPoint( x1, y1 );
				patrol.addPoint( x1, y2 );
				patrol.addPoint( x2, y2 );
				patrol.addPoint( x2, y1 );
				patrol.addPoint( x1, y1 );
			}
			
			return patrol;
		}
		
	}

}