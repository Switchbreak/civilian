package Minigames.Blockade 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	
	/**
	 * Bullet object, fired by Guards
	 * 
	 * @author Switchbreak
	 */
	public class Bullet extends Entity 
	{
		// Public constants
		public static const BULLET_SIZE:int = 5;
		
		// Private constants
		private static const BULLET_COLOR:uint = 0xFFFFFF;
		private static const BULLET_SPEED:int = 300;
		
		// Private variables
		private var moveX:Number;
		private var moveY:Number;
		
		/**
		 * Create and initialize the bullet
		 * 
		 * @param	setX
		 * Starting location of the bullet
		 * @param	setY
		 * Starting location of the bullet
		 * @param	facingX
		 * Normalized, axially aligned bullet movement vector. Each coordinate
		 * should be either -1, 0 or 1.
		 * @param	facingY
		 * Normalized, axially aligned bullet movement vector. Each coordinate
		 * should be either -1, 0 or 1.
		 */
		public function Bullet( setX:int, setY:int, facingX:int, facingY:int )
		{
			// Set object properties
			moveX = facingX * BULLET_SPEED;
			moveY = facingY * BULLET_SPEED;
			
			// Create bullet and set hitbox
			super( setX, setY, Image.createRect( BULLET_SIZE, BULLET_SIZE, BULLET_COLOR ) );
			type = Player.GUARD_COLLISION_TYPE;
			setHitboxTo( graphic );
		}
		
		/**
		 * Bullet movement
		 */
		override public function update():void 
		{
			if ( !Blockade.paused )
			{
				// Move bullet by vector
				moveBy( moveX * FP.elapsed, moveY * FP.elapsed );
				
				// Check for collision with a wall
				if ( collide( Player.WALL_COLLISION_TYPE, x, y ) )
				{
					FP.world.remove( this );
				}
			}
			
			super.update();
		}
		
	}

}