package Minigames.Blockade 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.utils.*;
	import net.flashpunk.graphics.Stamp;
	
	/**
	 * Player entity
	 * 
	 * @author Switchbreak
	 */
	public class Player extends Entity 
	{
		// Embeds
		[Embed (source = '../../../img/Farmer.png')] private static const FARMER:Class;
		
		// Public constants
		public static const PLAYER_COLLISION_TYPE:String = "player";
		public static const WALL_COLLISION_TYPE:String = "wall";
		public static const GOAL_COLLISION_TYPE:String = "goal";
		public static const GUARD_COLLISION_TYPE:String = "guard";
		public static const BULLET_COLLISION_TYPE:String = "bullet";
		
		// Private constants
		private static const PLAYER_SPEED:Number = 150;
		private static const INVINCIBLE_TIME:Number = 2;
		private static const BLINK_RATE:Number = 0.01;
		
		// Events
		private var eventWon:Function;
		
		// Private variables
		private var invincibleTimer:Number = 0;
		private var blinkTimer:Number = 0;
		
		// Graphics
		private var playerSprite:Spritemap;
		
		/**
		 * Create and initialize the player entity
		 * 
		 * @param setX
		 * Player starting position
		 * @param setY
		 * Player starting position
		 * @param setEventDeath
		 * Callback function to handle player death
		 * @param setEventLost
		 * Callback function to handle game loss event
		 * @param setEventWon
		 * Callback function to handle game win event
		 */
		public function Player( setX:int = 0, setY:int = 0, setEventWon:Function = null )
		{
			// Set object values
			eventWon = setEventWon;
			
			// Create object and set hitbox
			playerSprite = new Spritemap( FARMER );
			super( setX, setY, playerSprite );
			type = PLAYER_COLLISION_TYPE;
			setHitboxTo( playerSprite );
		}
		
		/**
		 * Handle player input and movement
		 */
		override public function update():void 
		{
			if ( !Blockade.paused )
			{
				// Local variables
				var vectorX:Number = 0, vectorY:Number = 0;							// Movement vector
				
				//
				// Input
				//
				
				if ( Input.check( Key.UP ) || Input.check( Key.W ) )
				{
					vectorY -= 1.0;
				}
				if ( Input.check( Key.DOWN ) || Input.check( Key.S ) )
				{
					vectorY += 1.0;
				}
				if ( Input.check( Key.LEFT ) || Input.check( Key.A ) )
				{
					vectorX -= 1.0;
					playerSprite.flipped = true;
				}
				if ( Input.check( Key.RIGHT ) || Input.check( Key.D ) )
				{
					vectorX += 1.0;
					playerSprite.flipped = false;
				}
				
				//
				// Movement/collision detection
				//
				
				// Check that player is moving, to avoid divide by zero on normalize
				if ( vectorX != 0 || vectorY != 0 )
				{
					// Normalize movement vector and scale by movement speed
					var scaler:Number = (PLAYER_SPEED * FP.elapsed) / FP.distance( vectorX, vectorY );
					vectorX *= scaler;
					vectorY *= scaler;
					
					moveBy( vectorX, vectorY, WALL_COLLISION_TYPE );
				}
				
				//
				// Handle events
				//
				
				// Handle damage events if the player is not in invincible mode
				if ( invincibleTimer <= 0 )
				{
					// Check for player collision with guard or bullets
					if ( collideTypes( [GUARD_COLLISION_TYPE, BULLET_COLLISION_TYPE], x, y ) != null )
					{
						// Kill a game character
						Blockade.sim.KillCharacter();
						
						// Set the player to invincible
						invincibleTimer = INVINCIBLE_TIME;
					}
				}
				else
				{
					// If the player is in invincible mode, decrement the timer
					invincibleTimer -= FP.elapsed;
					
					// If the invincibility hasn't worn off, make the sprite blink
					if ( invincibleTimer > 0 )
					{
						if ( blinkTimer > 0 )
						{
							// Decrement the blink timer
							blinkTimer -= FP.elapsed;
						}
						else
						{
							// Blink timer has counted down, toggle the graphic
							// between visibility and invisibility
							graphic.visible = !graphic.visible;
							blinkTimer = BLINK_RATE;
						}
					}
					else
					{
						blinkTimer = 0;
						graphic.visible = true;
					}
				}
				
				// Check for win state - player reaches goal
				if ( collide( GOAL_COLLISION_TYPE, x, y ) != null )
				{
					// Fire win event
					if ( eventWon != null )
						eventWon();
					
					// Remove minigame from world and return to main game
					FP.world = GameWorld.gameWorld;
				}
			}
			
			super.update();
		}
	}

}