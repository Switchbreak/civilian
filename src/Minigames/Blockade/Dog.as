package Minigames.Blockade 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	
	/**
	 * Entity for military dogs
	 * 
	 * @author Switchbreak
	 */
	public class Dog extends Entity 
	{
		// Embeds
		[Embed (source = '../../../img/DogSheet.png')]	private static const DOG_SHEET:Class;
		
		// Constants
		private static const MOVE_SPEED:Number = 25;
		private static const MOVE_DISTANCE:int = 100;
		private static const CHASE_RADIUS:Number = 100;
		private static const CHASE_SPEED:Number = 100;
		private static const WAIT_TIME:Number = 2;
		private static const TARGET_TYPES:Array = [Player.PLAYER_COLLISION_TYPE];
		private static const DOG_WIDTH:int = 58;
		private static const DOG_HEIGHT:int = 30;
		private static const DOG_IDLE:String = "idle";
		private static const DOG_SIDE_WALK:String = "side-walk";
		private static const DOG_UP_WALK:String = "up-walk";
		private static const DOG_DOWN_WALK:String = "down-walk";
		private static const DOG_SIDE_CHASE:String = "side-chase";
		private static const DOG_DOWN_CHASE:String = "down-chase";
		
		// States
		private static const STATE_WALK:int = 0;
		private static const STATE_CHASE:int = 1;
		private static const STATE_WAIT:int = 2;
		
		// Private variables
		private var targetX:int;
		private var targetY:int;
		private var state:int;
		private var waitTimer:Number;
		
		// Graphics
		private var dogSprite:Spritemap;
		
		/**
		 * Create and initialize the dog entity
		 */
		public function Dog( setX:int, setY:int )
		{
			// Initialize animation
			dogSprite = new Spritemap( DOG_SHEET, DOG_WIDTH, DOG_HEIGHT );
			dogSprite.add( DOG_IDLE, [1], 0, false );
			dogSprite.add( DOG_SIDE_WALK, [0, 1], 10, true );
			dogSprite.add( DOG_UP_WALK, [2, 3], 10, true );
			dogSprite.add( DOG_DOWN_WALK, [4, 5], 10, true );
			dogSprite.add( DOG_SIDE_CHASE, [8, 9, 10], 10, true );
			dogSprite.add( DOG_DOWN_CHASE, [12, 13, 14, 15], 10, true );
			
			// Create entity and set hitbox
			super( setX, setY, dogSprite );
			type = Player.GUARD_COLLISION_TYPE;
			layer = FP.height - (y + dogSprite.height);
			
			// Choose a random point to walk to
			NewWalkTarget();
			state = STATE_WALK;
		}
		
		/**
		 * Handle movement
		 */
		override public function update():void 
		{
			if ( !Blockade.paused )
			{
				// If a target is within radius, always enter the chase state
				var target:Entity = ChaseTargetDetected();
				if ( target )
				{
					state = STATE_CHASE;
				}
				else if( state == STATE_CHASE )
				{
					// In chase mode but the target was lost, go to waiting state
					state = STATE_WAIT;
					waitTimer = WAIT_TIME;
					dogSprite.play( DOG_IDLE, true );
					setHitbox( 33, 29, -16, -1 );
				}
				
				// AI simple state machine
				switch( state )
				{
					case STATE_WALK:
						// Non-alerted, simple random walk state
						moveTowards( targetX, targetY, MOVE_SPEED * FP.elapsed, Player.WALL_COLLISION_TYPE );
						layer = FP.height - (y + dogSprite.height);
						
						// If the destination has been reached, wait before walking
						// again
						if ( Math.abs(x - targetX) < 2 && Math.abs(y - targetY) < 2 )
						{
							state = STATE_WAIT;
							waitTimer = WAIT_TIME;
							dogSprite.play( DOG_IDLE, true );
							setHitbox( 33, 29, -16, -1 );
						}
						break;
					case STATE_WAIT:
						// Waiting for cooldown before entering random walk
						if ( waitTimer <= 0 )
						{
							NewWalkTarget();
							state = STATE_WALK;
						}
						else
						{
							waitTimer -= FP.elapsed;
						}
						break;
					case STATE_CHASE:
						// Set chasing animation
						var deltaX:Number = target.x - x;
						var deltaY:Number = target.y - y;
						
						if ( Math.abs( deltaX ) > Math.abs( deltaY ) )
						{
							dogSprite.play( DOG_SIDE_CHASE, false );
							if ( deltaX > 0 )
								dogSprite.flipped = false;
							else
								dogSprite.flipped = true;
							setHitboxTo( dogSprite );
						}
						else
						{
							if ( deltaY > 0 )
								dogSprite.play( DOG_DOWN_CHASE, false );
							else
								dogSprite.play( DOG_UP_WALK, false );
							setHitbox( 17, 27, -23, 0 );
						}
						
						// Chase after the target entity
						moveTowards( target.x, target.y, CHASE_SPEED * FP.elapsed, Player.WALL_COLLISION_TYPE );
						layer = FP.height - (y + dogSprite.height);
						
						break;
				}
			}
			
			super.update();
		}
		
		/**
		 * Handle collision
		 */
		override public function moveCollideX(e:Entity):Boolean 
		{
			// If the dog is in random walk state, change direction when a wall
			// is hit
			if ( state == STATE_WALK )
			{
				NewWalkTarget();
			}
			
			return true;
		}
		
		/**
		 * Handle collision
		 */
		override public function moveCollideY(e:Entity):Boolean 
		{
			// If the dog is in random walk state, change direction when a wall
			// is hit
			if ( state == STATE_WALK )
			{
				NewWalkTarget();
			}
			
			return true;
		}
		
		/**
		 * Picks a new destination for random walk
		 */
		private function NewWalkTarget():void
		{
			// Randomly set distance to walk
			var moveDistance:int = FP.rand( MOVE_DISTANCE << 1 ) - MOVE_DISTANCE;
			
			// Randomly pick whether movement will be along the X or Y axis
			if ( FP.random >= 0.5 )
			{
				targetX = x;
				targetY = y + moveDistance;
				
				if( moveDistance > 0 )
					dogSprite.play( DOG_DOWN_WALK, true );
				else
					dogSprite.play( DOG_UP_WALK, true );
				setHitbox( 14, 27, -23, 0 );
			}
			else
			{
				targetX = x + moveDistance;
				targetY = y;
				
				dogSprite.play( DOG_SIDE_WALK, true );
				setHitbox( 33, 29, -16, -1 );
				if ( moveDistance > 0 )
					dogSprite.flipped = false;
				else
					dogSprite.flipped = true;
			}
		}
		
		/**
		 * Checks if any entities in the list of target types are within the
		 * chasing radius
		 * 
		 * @return
		 * First target entity found
		 */
		private function ChaseTargetDetected():Entity
		{
			// Get list of all entities in the world with types that the guard
			// has in the target types list
			var targets:Vector.<Entity> = new Vector.<Entity>();
			for each( var targetType:String in TARGET_TYPES )
			{
				FP.world.getType( targetType, targets );
			}
			
			// Check the distance to all potential targets
			for each( var target:Entity in targets )
			{
				if ( distanceFrom( target, true ) < CHASE_RADIUS )
					return target;
			}
			
			// No target found
			return null;
		}
		
	}

}