package Minigames.Escape 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import net.flashpunk.tweens.misc.Alarm;
	
	/**
	 * Tank object
	 * 
	 * @author Switchbreak
	 */
	public class Tank extends Entity 
	{
		// Constants
		private static const TANK_COLOR:uint = 0xFFFF00;
		private static const TANK_SIZE:int = 60;
		private static const FIRE_RATE:Number = 3.0;
		private static const MIN_X:int = 150;
		private static const MAX_X:int = 520;
		private static const MIN_Y:int = 180;
		private static const MAX_Y:int = 410;
		
		/**
		 * Create and initialize tank object
		 * 
		 * @param	setX
		 * Initial position of the tank
		 * @param	setY
		 * Initial position of the tank
		 */
		public function Tank( setX:int, setY:int ) 
		{
			// Initialize object
			super( setX, setY, Image.createRect( TANK_SIZE, TANK_SIZE, TANK_COLOR ) );
			type = Soldier.SOLDIER_TYPE;
			setHitboxTo( graphic );
			layer = y + height;
			
			// Set the tank to fire on a timer
			var fireTween:Alarm = new Alarm( FIRE_RATE, Fire, LOOPING );
			addTween( fireTween, true );
		}
		
		/**
		 * Fire the rocket at a random destination point
		 */
		public function Fire():void
		{
			// Only fire if the game isn't paused
			if ( !Escape.paused )
			{
				// Choose a target point at random
				var destX:int = FP.rand( MAX_X - MIN_X ) + MIN_X;
				var destY:int = FP.rand( MAX_Y - MIN_Y ) + MIN_Y;
				FP.world.add( new Rocket( x + halfWidth, y + halfHeight, destX, destY ) );
			}
		}
		
	}

}