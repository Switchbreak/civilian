package Minigames.Escape 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.FP;
	
	/**
	 * Explosion entity
	 * 
	 * @author Switchbreak
	 */
	public class Explosion extends Entity 
	{
		// Private constants
		public static const EXPLOSION_SIZE:int = 50;
		public static const EXPLODE_TYPE:String = "explosion";
		private static const EXPLOSION_TIME:Number = 0.5;
		private static const EXPLOSION_COLOR:uint = 0xFFFFFF;
		
		/**
		 * Create and initialize the explosion entity
		 * 
		 * @param	setX
		 * Position of the explosion
		 * @param	setY
		 * Position of the explosion
		 */
		public function Explosion( setX:int, setY:int ) 
		{
			// Create graphic and initialize the entity
			super( setX, setY, Image.createCircle( EXPLOSION_SIZE, EXPLOSION_COLOR ) );
			setHitboxTo( graphic );
			layer = FP.height - (y + height);
			type = EXPLODE_TYPE;
			
			// Set time for explosion to linger
			addTween( new Alarm( EXPLOSION_TIME, Remove, ONESHOT ), true );
		}
		
		/**
		 * Remove the grenade from the world
		 */
		protected function Remove():void
		{
			FP.world.remove( this );
		}
		
	}

}