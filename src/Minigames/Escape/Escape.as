package Minigames.Escape 
{
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import Minigames.Escape.FieldTile;
	import mx.utils.StringUtil;
	import mx.resources.ResourceManager
	import UI.FloaterText;
	import UI.DialogBox;
	import Types.Farm;
	
	/**
	 * Object to run the escape minigame
	 * 
	 * @author Switchbreak
	 */
	public class Escape extends World 
	{
		// Embeds
		[Embed (source = '../../../img/FarmBackground.png')]		private static const FARM_BACKGROUND:Class;
		[Embed (source = '../../../img/FarmBackground-Winter.png')]	private static const FARM_BACKGROUND_WINTER:Class;
		
		// Constants
		private static const TANK_X:int = 30;
		private static const TANK_Y:int = 245;
		private static const TRACTOR_WIDTH:int = 80;
		private static const TRACTOR_HEIGHT:int = 50;
		private static const TRACTOR_X:int = 320;
		private static const TRACTOR_Y:int = 420;
		private static const SOLDIER_COUNT:int = 6;
		private static const GRID_LEFT:int = 152;
		private static const GRID_TOP:int = 182;
		private static const PLAYFIELD_TOP:int = 182;
		private static const PLAYFIELD_BOTTOM:int = 479;
		private static const PLAYFIELD_LEFT:int = 50;
		private static const PLAYFIELD_RIGHT:int = 590;
		
		// Public constants
		public static const BACKGROUND_LAYER:int = 1000;
		public static const HUD_LAYER:int = -100;
		
		// Static variables
		public static var sim:Sim;
		public static var escapeWorld:Escape;
		public static var paused:Boolean;
		public static var leftSoldiers:Vector.<Soldier>;
		public static var rightSoldiers:Vector.<Soldier>;
		public static var tank:Tank;
		
		// Private variables
		private var playerX:int;
		private var playerY:int;
		
		// Entities
		private var player:Player;
		private var eventEscaped:Function;
		private var gasScoreDisplay:Text;
		private var belongingsScoreDisplay:Text;
		
		/**
		 * Creates and initializes the escape minigame object
		 * 
		 * @param	setSim
		 * Reference to the overall Sim object
		 * @param	farmerX
		 * Starting position of the farmer
		 * @param	farmerY
		 * Starting position of the farmer
		 * @param	setEventEscaped
		 * Callback to trigger when the player wins the escape minigame
		 */
		public function Escape( setSim:Sim, farmerX:int, farmerY:int, setEventEscaped:Function = null ) 
		{
			// Set values
			paused = false;
			sim = setSim;
			playerX = farmerX;
			playerY = farmerY;
			escapeWorld = this;
			eventEscaped = setEventEscaped;
			sim.EventFamilyDied = EventDeath;
			sim.EventLost = EventLost;
		}
		
		override public function begin():void 
		{
			super.begin();
			
			// Draw the farm background
			FarmGraphics();
			
			//
			// Add playfield walls
			//
			
			// Top wall
			var wall:Entity = new Entity();
			wall.setHitbox( FP.width, PLAYFIELD_TOP );
			wall.active = false;
			wall.type = Player.WALL_COLLISION_TYPE;
			add( wall );
			
			// Bottom wall
			wall = new Entity( 0, PLAYFIELD_BOTTOM );
			wall.setHitbox( FP.width, FP.height - PLAYFIELD_BOTTOM );
			wall.active = false;
			wall.type = Player.WALL_COLLISION_TYPE;
			add( wall );
			
			// Left wall
			wall = new Entity();
			wall.setHitbox( PLAYFIELD_LEFT, FP.height );
			wall.active = false;
			wall.type = Player.WALL_COLLISION_TYPE;
			add( wall );
			
			// Right wall
			wall = new Entity( PLAYFIELD_RIGHT );
			wall.setHitbox( FP.width - PLAYFIELD_RIGHT, FP.height );
			wall.active = false;
			wall.type = Player.WALL_COLLISION_TYPE;
			add( wall );
			
			// Add tractor
			var tractor:Entity = new Entity( TRACTOR_X, TRACTOR_Y, Image.createRect( TRACTOR_WIDTH, TRACTOR_HEIGHT ) );
			tractor.type = Player.TRACTOR_COLLISION_TYPE;
			tractor.active = false;
			tractor.setHitboxTo( tractor.graphic );
			tractor.layer = tractor.y;
			add( tractor );
			
			// Add score GUI elements
			gasScoreDisplay = new Text( " " );
			gasScoreDisplay.x = tractor.x;
			gasScoreDisplay.y = tractor.y - gasScoreDisplay.height;
			addGraphic( gasScoreDisplay, HUD_LAYER );
			
			belongingsScoreDisplay = new Text( " ", gasScoreDisplay.x, gasScoreDisplay.y );
			belongingsScoreDisplay.width = tractor.width;
			belongingsScoreDisplay.align = "right";
			addGraphic( belongingsScoreDisplay, HUD_LAYER );
			
			// Add player entity
			player = new Player( playerX, playerY, eventEscaped );
			add( player );
			
			// Add the tank
			tank = new Tank( TANK_X, TANK_Y );
			add( tank );
			
			// Add soldiers
			leftSoldiers = new Vector.<Soldier>();
			rightSoldiers = new Vector.<Soldier>();
			for ( var i:int = 0; i < SOLDIER_COUNT; i++ )
			{
				leftSoldiers.push( add( new Soldier( Soldier.SIDE_LEFT, SoldierDeath ) ) );
				rightSoldiers.push( add( new Soldier( Soldier.SIDE_RIGHT, SoldierDeath ) ) );
			}
		}
		
		/**
		 * Function to draw farm graphics for the background
		 */
		protected function FarmGraphics():void 
		{
			// Add background
			var farmBackground:Stamp;
			if ( sim.season == Sim.SEASON_WINTER )
				farmBackground = new Stamp( FARM_BACKGROUND_WINTER )
			else
				farmBackground = new Stamp( FARM_BACKGROUND );
			addGraphic( farmBackground, BACKGROUND_LAYER );
			
			// Add field graphics
			for ( var i:int = 0; i < Farm.FIELD_COUNT; i++ )
			{
				var fieldTile:FieldTile = new FieldTile( GRID_LEFT, GRID_TOP, sim.farm.fieldGridPositions[i], FieldTile.TYPE_FIELD, i );
				
				add( fieldTile );
			}
			
			// Add farmhouse
			var houseTile:FieldTile = new FieldTile( GRID_LEFT, GRID_TOP, sim.farm.houseGridPosition, FieldTile.TYPE_HOUSE );
			add( houseTile );
			
			// Add barn
			var barnTile:FieldTile = new FieldTile( GRID_LEFT, GRID_TOP, sim.farm.barnGridPosition, FieldTile.TYPE_BARN );
			add( barnTile );
			
			// Add silos
			var siloTile:FieldTile = new FieldTile( GRID_LEFT, GRID_TOP, sim.farm.siloGridPosition, FieldTile.TYPE_SILO );
			add( siloTile );
			
			// Add pasture
			var pastureTile:FieldTile = new FieldTile( GRID_LEFT, GRID_TOP, sim.farm.pastureGridPosition, FieldTile.TYPE_PASTURE );
			add( pastureTile );
		}
		
		/**
		 * Function called when a soldier dies
		 * 
		 * @param	soldier
		 * Reference to the soldier that died
		 */
		public function SoldierDeath( soldier:Soldier ):void
		{
			// Get the list of soldiers for the side the soldier was in
			var soldiers:Vector.<Soldier> = (soldier.side == Soldier.SIDE_LEFT ? leftSoldiers : rightSoldiers);
			
			// Remove the soldier from the list and the world
			soldiers.splice( soldiers.indexOf( soldier ), 1 );
			remove( soldier );
			
			// Spawn new soldier
			soldiers.push( add( new Soldier( soldier.side, SoldierDeath ) ) );
		}
		
		/**
		 * Function to set the gas score GUI element
		 * 
		 * @param	gasScore
		 * Score to display
		 * @param	gasMax
		 * Maximum score to display
		 */
		public function SetGasScore( gasScore:int, gasMax:int ):void
		{
			gasScoreDisplay.text = StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "SCORE_DISPLAY" ), gasScore, gasMax );
		}
		
		/**
		 * Function to set the belongings score GUI element
		 * 
		 * @param	belongingsScore
		 * Score to display
		 * @param	belongingsMax
		 * Maximum score to display
		 */
		public function SetBelongingsScore( belongingsScore:int, belongingsMax:int ):void
		{
			belongingsScoreDisplay.text = StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "SCORE_DISPLAY" ), belongingsScore, belongingsMax );
		}
		
		//
		// Events
		//
		
		/**
		 * This function is called to inform the player that a family member has
		 * died
		 */
		public function EventDeath( deceasedName:String ):void
		{
			add( new FloaterText( player.x + player.halfWidth, player.y, StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "DIALOG_FAMILY_DIED" ), deceasedName ) ) );
		}
		
		/**
		 * This function is called when the player has lost
		 */
		public function EventLost():void
		{
			paused = true;
			
			var lostDialog:DialogBox = new DialogBox( ResourceManager.getInstance().getString( "resources", "DIALOG_LOST" ) );
			add( lostDialog );
		}
		
		/**
		 * This function is called when the player cancels out of a dialog box
		 */
		public function CancelDialog():void
		{
			paused = false;
		}
		
	}

}