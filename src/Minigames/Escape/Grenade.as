package Minigames.Escape 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.tweens.misc.VarTween;
	import net.flashpunk.utils.Ease;
	
	/**
	 * Bullet object, fired by Guards
	 * 
	 * @author Switchbreak
	 */
	public class Grenade extends Entity 
	{
		// Public constants
		public static const GRENADE_SIZE:int = 10;
		
		// Private constants
		private static const GRENADE_COLOR:uint = 0x00FF00;
		private static const GRENADE_FLASH_COLOR:uint = 0xFFFFFF;
		private static const GRENADE_SPEED_X:int = 100;
		private static const GRENADE_SPEED_Y:int = 0;
		private static const GRENADE_SPEED_Z:int = 200;
		private static const GRENADE_SPEED_RANGE_X:int = 50;
		private static const GRENADE_SPEED_RANGE_Y:int = 10;
		private static const GRENADE_SPEED_RANGE_Z:int = 30;
		private static const GRAVITY:Number = 200;
		private static const DETONATION_TIME:Number = 3.0;
		private static const BEEP_START_TIME:Number = 0.3;
		private static const BEEP_DURATION:Number = 0.06;
		
		// Private variables
		private var moveX:Number;
		private var moveY:Number;
		private var moveZ:Number;
		private var z:Number;
		private var flying:Boolean = true;
		
		// Public variables
		public var beepTimer:Number = BEEP_START_TIME;
		
		// Graphics
		private var grenadeSprite:Image;
		
		/**
		 * Create and initialize the grenade
		 * 
		 * @param	setX
		 * Starting location of the grenade
		 * @param	setY
		 * Starting location of the grenade
		 * @param	facingX
		 * Normalized grenade movement vector
		 * @param	facingY
		 * Normalized grenade movement vector
		 */
		public function Grenade( setX:int, setY:int, facingX:Number )
		{
			// Set object properties
			moveX = facingX * GRENADE_SPEED_X + FP.random * (GRENADE_SPEED_RANGE_X << 1) - GRENADE_SPEED_RANGE_X;
			moveY = GRENADE_SPEED_Y + FP.random * (GRENADE_SPEED_RANGE_Y << 1) - GRENADE_SPEED_RANGE_Y;
			moveZ = GRENADE_SPEED_Z + FP.random * (GRENADE_SPEED_RANGE_Z << 1) - GRENADE_SPEED_RANGE_Z;
			z = 0;
			
			// Create bullet and set hitbox
			grenadeSprite = Image.createRect( GRENADE_SIZE, GRENADE_SIZE );
			grenadeSprite.color = GRENADE_COLOR;
			super( setX, setY, grenadeSprite );
			layer = FP.height - (y + GRENADE_SIZE);
		}
		
		/**
		 * Grenade movement
		 */
		override public function update():void 
		{
			if ( !Escape.paused && flying )
			{
				// Move grenade by vector
				moveBy( moveX * FP.elapsed, (moveY - moveZ) * FP.elapsed );
				z += moveZ * FP.elapsed;
				
				// Set render order
				layer = FP.height - (y + z + GRENADE_SIZE);
				
				// When the grenade hits the ground, start the detonation timer
				if ( z < 0 )
					StartFuse();
				
				// Apply gravity to grenade arc
				moveZ -= GRAVITY * FP.elapsed;
			}
			
			super.update();
		}
		
		/**
		 * Start the fuse to detonate the grenade
		 */
		protected function StartFuse():void
		{
			// Stop the grenade from flying through the air
			flying = false;
			
			// Decay the time between beeps over the course of DETONATION_TIME
			var timeDecay:VarTween = new VarTween( Explode, ONESHOT );
			timeDecay.tween( this, "beepTimer", 0, DETONATION_TIME, Ease.quadIn );
			addTween( timeDecay, true );
			
			// Set the next grenade flash event
			addTween( new Alarm( beepTimer, FlashGrenade, ONESHOT ), true );
		}
		
		/**
		 * Explode the grenade
		 */
		protected function Explode():void
		{
			// Center explosion around center of grenade
			var offset:Number = (GRENADE_SIZE >> 1) - Explosion.EXPLOSION_SIZE;
			
			// Spawn the new explosion and despawn the grenade
			FP.world.add( new Explosion( x + offset, y + offset ) );
			FP.world.remove( this );
		}
		
		/**
		 * Set flash color
		 */
		protected function FlashGrenade():void
		{
			// Set the color
			grenadeSprite.color = GRENADE_FLASH_COLOR;
			
			// Set timers for the end of this flash and the start of the next
			addTween( new Alarm( BEEP_DURATION, UnsetFlash, ONESHOT ), true );
			addTween( new Alarm( beepTimer, FlashGrenade, ONESHOT ), true );
		}
		
		/**
		 * Return grenade from flash color back to normal
		 */
		protected function UnsetFlash():void
		{
			grenadeSprite.color = GRENADE_COLOR;
		}
	}

}