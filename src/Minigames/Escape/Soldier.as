package Minigames.Escape 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.tweens.motion.*;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.FP;
	
	/**
	 * Soldier object
	 * 
	 * @author Switchbreak
	 */
	public class Soldier extends Entity 
	{
		// Public constants
		public static const SIDE_LEFT:int = 0;
		public static const SIDE_RIGHT:int = 1;
		public static const SOLDIER_TYPE:String = "soldier";
		public static const LEFT_BULLET_TYPE:String = "left bullet";
		public static const RIGHT_BULLET_TYPE:String = "right bullet";
		
		// Constants
		private static const SOLDIER_COLOR:uint = 0xFFFF00;
		private static const SOLDIER_SIZE:int = 20;
		private static const SOLDIER_SPEED:Number = 150;
		private static const RANDOM_ENTER_RANGE:int = 40;
		private static const SPEED_RANGE:Number = 75;
		private static const MARGIN:int = 120;
		private static const MIN_Y:int = 180;
		private static const MAX_Y:int = 410;
		private static const X_RANGE:int = 5;
		private static const FIRING_RATE:Number = 7;
		private static const FIRING_RATE_RANGE:Number = 2;
		private static const GRENADE_RATE:Number = 0.2;
		
		// Private variables
		private var enterTween:Motion;
		private var fireCooldown:Number;
		private var eventDeath:Function;
		
		// Public variables
		public var destX:int, destY:int;
		public var side:int;
		public var target:Soldier = null;
		
		/**
		 * Create and initialize the soldier
		 * 
		 * @param	setSide
		 * Side of the screen for the soldier to fight on
		 * @param	setEventDeath
		 * Function to call when the soldier dies
		 */
		public function Soldier( setSide:int, setEventDeath:Function ) 
		{
			super();
			
			// Set entity values
			type = SOLDIER_TYPE;
			side = setSide;
			eventDeath = setEventDeath;
			fireCooldown = FP.random * FIRING_RATE;
			
			// Initialize the soldier object
			graphic = Image.createRect( SOLDIER_SIZE, SOLDIER_SIZE, SOLDIER_COLOR );
			setHitboxTo( graphic );
			layer = FP.height - (y + height);
			
			// Set starting position - enter from the left or right side of the
			// screen at a y position randomly variant from the destination
			switch( side )
			{
				case SIDE_LEFT:
					x = -SOLDIER_SIZE;
					destX = MARGIN + FP.rand( X_RANGE << 1 ) - X_RANGE;
					break;
				case SIDE_RIGHT:
					x = FP.width;
					destX = FP.width - MARGIN + FP.rand( X_RANGE << 1 ) - X_RANGE;
					break;
			}
			
			// Set random y position and make sure it doesn't overlap
			var overlaps:Boolean = true;
			while ( overlaps )
			{
				// Set random position between MIN_Y and MAX_Y
				destY = MIN_Y + FP.rand( MAX_Y - MIN_Y );
				overlaps = false;
				
				// Go through all soldiers on the same side, and make sure the
				// destinations don't overlap
				var soldiers:Vector.<Soldier> = (side == SIDE_LEFT ? Escape.leftSoldiers : Escape.rightSoldiers);
				for each( var soldier:Soldier in soldiers )
				{
					if ( soldier.side == side && Math.abs(destY - soldier.destY) < SOLDIER_SIZE)
						overlaps = true;
				}
			}
			y = destY + FP.rand( RANDOM_ENTER_RANGE << 1 ) - RANDOM_ENTER_RANGE;
			
			// Go around the tank if on the left
			if ( side == SIDE_LEFT && destY >= Escape.tank.y - SOLDIER_SIZE && destY <= Escape.tank.y + Escape.tank.height )
			{
				if ( y + halfHeight < Escape.tank.y + Escape.tank.halfHeight )
				{
					y = Escape.tank.y - height;
					
					// Stage entrance is a path around the tank
					var enterPath:LinearPath = new LinearPath( null, ONESHOT );
					enterPath.addPoint( x, y );
					enterPath.addPoint( Escape.tank.x + Escape.tank.width, Escape.tank.y - height );
					enterPath.addPoint( destX, destY );
					enterPath.setMotionSpeed( SOLDIER_SPEED + FP.random * SPEED_RANGE * 2 - SPEED_RANGE, Ease.cubeOut );
					enterTween = enterPath;
				}
				else
				{
					y = Escape.tank.y + Escape.tank.height;
					
					// Stage entrance is a path around the tank
					enterPath = new LinearPath( null, ONESHOT );
					enterPath.addPoint( x, y );
					enterPath.addPoint( Escape.tank.x + Escape.tank.width, Escape.tank.y + Escape.tank.height );
					enterPath.addPoint( destX, destY );
					enterPath.setMotionSpeed( SOLDIER_SPEED + FP.random * SPEED_RANGE * 2 - SPEED_RANGE, Ease.cubeOut );
					enterTween = enterPath;
				}
			}
			else
			{
				// Not going around the tank, stage entrance is a straight line
				var enterMotion:LinearMotion = new LinearMotion( null, ONESHOT );
				enterMotion.setMotionSpeed( x, y, destX, destY, SOLDIER_SPEED + FP.random * SPEED_RANGE * 2 - SPEED_RANGE, Ease.cubeOut );
				enterTween = enterMotion;
			}
			
			addTween( enterTween, true );
		}
		
		/**
		 * Handle motion
		 */
		override public function update():void 
		{
			// Only move when the minigame is not paused
			if ( !Escape.paused )
			{
				// If entering the stage, follow then entrance movement
				if ( enterTween.active )
				{
					x = enterTween.x;
					y = enterTween.y;
					layer = FP.height - (y + height);
				}
				else
				{
					// If the entrance is finished, shoot at rival soldiers
					
					// Get a target if one hasn't been acquired yet, or if the
					// target has died
					if ( target == null || target.world == null )
						PickTarget();
					
					// If a target was successfully acquired, shoot at it
					if ( target != null )
					{
						// Decrement the cooldown timer between shots
						fireCooldown -= FP.elapsed;
						
						// When the cooldown finishes, shoot at target
						if ( fireCooldown <= 0 )
						{
							// Either shoot or throw grenade, randomly
							if( FP.random > GRENADE_RATE )
								Shoot();
							else
								ThrowGrenade();
							
							// Reset the cooldown
							fireCooldown = FIRING_RATE + FP.random * FIRING_RATE_RANGE * 2 - FIRING_RATE_RANGE;
						}
					}
					
					// If hit by a bullet from the opposite side, the guard dies
					var bulletType:String = (side == SIDE_LEFT ? RIGHT_BULLET_TYPE : LEFT_BULLET_TYPE)
					var bullet:Entity = collide( bulletType, x, y );
					if ( bullet != null )
					{
						FP.world.remove( bullet );
						eventDeath( this );
					}
						
					// Check for collision with explosions
					var explosions:Vector.<Explosion> = new Vector.<Explosion>();
					collideInto( Explosion.EXPLODE_TYPE, x, y, explosions );
					for each( var explosion:Explosion in explosions )
					{
						// Check if the distance to the explosion is less than
						// the radius of it
						if( distanceToPoint( explosion.x + Explosion.EXPLOSION_SIZE, explosion.y + Explosion.EXPLOSION_SIZE, true ) < Explosion.EXPLOSION_SIZE )
							eventDeath( this );
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Pick a target from the other side's soldiers, if available
		 */
		protected function PickTarget():void
		{
			// No target acquired, pick one at random from the other
			// side
			var soldiers:Vector.<Soldier> = (side == SIDE_LEFT ? Escape.rightSoldiers : Escape.leftSoldiers);
			
			if ( soldiers.length > 0 )
			{
				target = soldiers[FP.rand( soldiers.length )];
				fireCooldown = Math.max( FP.random * FIRING_RATE_RANGE, fireCooldown );
			}
		}
		
		/**
		 * Fire at the selected target
		 */
		protected function Shoot():void 
		{
			// Get the vector to the target
			var vectorX:Number = target.x - x;
			var vectorY:Number = target.y - y;
			
			// Normalize it
			var length:Number = Math.sqrt( vectorX * vectorX + vectorY * vectorY );
			vectorX /= length;
			vectorY /= length;
			
			// Create the bullet
			var bulletType:String = (side == SIDE_LEFT ? LEFT_BULLET_TYPE : RIGHT_BULLET_TYPE)
			FP.world.add( new Bullet( bulletType, x + halfWidth - (Bullet.BULLET_SIZE >> 1), y + halfHeight - (Bullet.BULLET_SIZE >> 1), vectorX, vectorY ) );
		}
		
		/**
		 * Throw a grenade
		 */
		protected function ThrowGrenade():void 
		{
			// Determine horizontal direction of throw based on direction to
			// the target
			var facingX:Number = (target.x > x ? 1 : -1);
			
			// Create the bullet
			var bulletType:String = (side == SIDE_LEFT ? LEFT_BULLET_TYPE : RIGHT_BULLET_TYPE)
			FP.world.add( new Grenade( x + halfWidth - (Grenade.GRENADE_SIZE >> 1), y + halfHeight - (Grenade.GRENADE_SIZE >> 1), facingX ) );
		}
		
	}

}