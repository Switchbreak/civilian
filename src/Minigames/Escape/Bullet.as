package Minigames.Escape 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	
	/**
	 * Bullet object, fired by Guards
	 * 
	 * @author Switchbreak
	 */
	public class Bullet extends Entity 
	{
		// Public constants
		public static const BULLET_SIZE:int = 5;
		
		// Private constants
		private static const BULLET_COLOR:uint = 0;
		private static const BULLET_SPEED:int = 100;
		
		// Private variables
		private var moveX:Number;
		private var moveY:Number;
		
		/**
		 * Create and initialize the bullet
		 * 
		 * @param	bulletType
		 * The collision type of the bullet
		 * @param	setX
		 * Starting location of the bullet
		 * @param	setY
		 * Starting location of the bullet
		 * @param	facingX
		 * Normalized bullet movement vector
		 * @param	facingY
		 * Normalized bullet movement vector
		 */
		public function Bullet( bulletType:String, setX:int, setY:int, facingX:Number, facingY:Number )
		{
			// Set object properties
			moveX = facingX * BULLET_SPEED;
			moveY = facingY * BULLET_SPEED;
			
			// Create bullet and set hitbox
			super( setX, setY, Image.createRect( BULLET_SIZE, BULLET_SIZE, BULLET_COLOR ) );
			type = bulletType;
			setHitboxTo( graphic );
			//layer = FP.height - (y + height);
		}
		
		/**
		 * Bullet movement
		 */
		override public function update():void 
		{
			if ( !Escape.paused )
			{
				// Move bullet by vector
				moveBy( moveX * FP.elapsed, moveY * FP.elapsed );
				
				// Set the render order
				//layer = FP.height - (y + height);
				
				// Check if bullet went off the map
				if ( x < -BULLET_SIZE || x > FP.width || y < -BULLET_SIZE || y > FP.height )
				{
					FP.world.remove( this );
				}
			}
			
			super.update();
		}
		
	}

}