package Minigames.Escape 
{
	import flash.display.Stage;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.utils.Input;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	import Types.Field;
	
	/**
	 * Field tile UI element for farm HUD
	 * 
	 * @author Switchbreak
	 */
	public class FieldTile extends Entity
	{
		// Embeds
		[Embed (source = '../../../img/TileField.png')]		private static const TILE_FIELD:Class;
		[Embed (source = '../../../img/TileHouse.png')]		private static const TILE_HOUSE:Class;
		[Embed (source = '../../../img/TileBarn.png')]		private static const TILE_BARN:Class;
		[Embed (source = '../../../img/TileSilo.png')]		private static const TILE_SILO:Class;
		[Embed (source = '../../../img/TilePasture.png')]	private static const TILE_PASTURE:Class;
		
		// Constants
		public static const TILE_WIDTH:int = 109;
		public static const TILE_HEIGHT:int = 63;
		
		// Tile types
		public static const TYPE_FIELD:int = 0;
		public static const TYPE_HOUSE:int = 1;
		public static const TYPE_BARN:int = 2;
		public static const TYPE_SILO:int = 3;
		public static const TYPE_PASTURE:int = 4;
		
		// Field frames
		public static const FRAME_EMPTY:int = 0;
		public static const FRAME_SEED:int = 1;
		public static const FRAME_SPROUT:int = 2;
		public static const FRAME_TENDED:int = 3;
		public static const FRAME_RIPE:int = 4;
		public static const FRAME_WITHERED:int = 5;
		
		// Private constants
		private static const PASTURE_WIDTH:int = 59;
		private static const PASTURE_HEIGHT:int = 59;
		private static const HOUSE_WIDTH:int = 102;
		private static const HOUSE_HEIGHT:int = 61;
		private static const BARN_WIDTH:int = 42;
		private static const BARN_HEIGHT:int = 64;
		
		// Private variables
		private var fieldIndex:int;
		private var childEntities:Vector.<Entity> = new Vector.<Entity>();
		
		//
		// Public methods
		//
		
		/**
		 * Creates and initializes the field tile
		 * 
		 * @param	gridX
		 * Coordinates of the top-left of the tile grid
		 * @param	gridY
		 * Coordinates of the top-left of the tile grid
		 * @param	gridPos
		 * Position on the grid, represented as a number 1-9
		 * @param	tileType
		 * Type of tile, should be one of the TYPE static constants
		 * @param	fieldIndex
		 * Index of the field this tile represents
		 */
		public function FieldTile( gridX:int, gridY:int, gridPos:int, tileType:int, fieldIndex:int = -1 ) 
		{
			// Set object values
			x = gridX + ((gridPos - 1) % 3) * TILE_WIDTH + 3;
			y = gridY + Math.floor((gridPos - 1) / 3) * TILE_HEIGHT;
			active = false;
			
			// Create tile image based on tile type
			switch( tileType )
			{
				case TYPE_FIELD:
					// Set field to spritemap of growing field
					var tileImage:Spritemap = new Spritemap( TILE_FIELD, TILE_WIDTH, TILE_HEIGHT );
					
					// Check if the field is empty
					if ( Escape.sim.farm.fields[fieldIndex] == null )
					{
						tileImage.frame = FRAME_EMPTY;
					}
					else
					{
						// For full fields, spritemap frame is set based on age
						switch( Escape.sim.farm.fields[fieldIndex].age )
						{
							case Field.AGE_SEED:
								tileImage.frame = FRAME_SEED;
								break;
							case Field.AGE_SPROUT:
								if ( !Escape.sim.farm.fields[fieldIndex].tended )
									tileImage.frame = FieldTile.FRAME_SPROUT;
								else
									tileImage.frame = FieldTile.FRAME_TENDED;
									
								break;
							case Field.AGE_RIPE:
								tileImage.frame = FRAME_RIPE;
								break;
							case Field.AGE_WITHERED:
								tileImage.frame = FRAME_WITHERED;
								break;
						}
					}
					
					graphic = tileImage;
					setHitboxTo( tileImage );
					
					// Set the render order
					layer = FP.height - y;
					
					break;
				case TYPE_HOUSE:
					// Set the house graphic and collision
					tileImage = new Spritemap( TILE_HOUSE, HOUSE_WIDTH, HOUSE_HEIGHT );
					tileImage.frame = (Escape.sim.season == Sim.SEASON_WINTER ? 1 : 0);
					type = Player.WALL_COLLISION_TYPE;
					graphic = tileImage;
					setHitbox( 91, 22, -5, -33 );
					
					// Center the sprite on the tile
					y += (TILE_HEIGHT >> 1) - ((graphic as Object).height >> 1);
					x += (TILE_WIDTH >> 1) - ((graphic as Object).width >> 1);
					
					// Add house door entity as gas can gather point
					var houseDoor:Entity = new Entity( x + 36, y + 39 );
					houseDoor.setHitbox( 16, 5 );
					houseDoor.type = Player.HOUSE_COLLISION_TYPE;
					houseDoor.active = false;
					childEntities.push( FP.world.add( houseDoor ) );
					
					// Set the render order
					layer = FP.height - (y + tileImage.height);
					
					break;
				case TYPE_BARN:
					// Set barn graphic and collision
					tileImage = new Spritemap( TILE_BARN, BARN_WIDTH, BARN_HEIGHT );
					tileImage.frame = (Escape.sim.season == Sim.SEASON_WINTER ? 1 : 0);
					type = Player.WALL_COLLISION_TYPE;
					graphic = tileImage;
					setHitbox( 38, 38, -2, -23 );
					
					// Center the sprite on the tile
					y += (TILE_HEIGHT >> 1) - ((graphic as Object).height >> 1);
					x += (TILE_WIDTH >> 1) - ((graphic as Object).width >> 1);
					
					// Add barn door entity as gas can gather point
					var barnDoor:Entity = new Entity( x + 12, y + 31 );
					barnDoor.setHitbox( 10, 5 );
					barnDoor.type = Player.BARN_COLLISION_TYPE;
					barnDoor.active = false;
					childEntities.push( FP.world.add( barnDoor ) );
					
					// Set the render order
					layer = FP.height - (y + tileImage.height);
					
					break;
				case TYPE_SILO:
					// Set silo graphic and collision
					graphic = new Stamp( TILE_SILO );
					type = Player.WALL_COLLISION_TYPE;
					setHitbox( 17, 10, -5, -50 );
					
					// Center the sprite on the tile
					y += (TILE_HEIGHT >> 1) - ((graphic as Object).height >> 1);
					x += (TILE_WIDTH >> 1) - ((graphic as Object).width >> 1);
					
					// Add second silo collision entity
					var secondSilo:Entity = new Entity( x + 45, y + 47 );
					secondSilo.setHitbox( 14, 10 );
					secondSilo.type = Player.WALL_COLLISION_TYPE;
					secondSilo.active = false;
					childEntities.push( FP.world.add( secondSilo ) );
					
					// Set the render order
					layer = FP.height - (y + (graphic as Stamp).height);
					
					break;
				case TYPE_PASTURE:
					tileImage = new Spritemap( TILE_PASTURE, PASTURE_WIDTH, PASTURE_HEIGHT );
					tileImage.frame = Escape.sim.season;
					graphic = tileImage;
					type = Player.WALL_COLLISION_TYPE;
					setHitbox( 10, 6, -23, -48 );
					
					// Center the sprite on the tile
					y += (TILE_HEIGHT >> 1) - ((graphic as Object).height >> 1);
					
					// Set the render order
					layer = FP.height - (y + tileImage.height);
					
					break;
			}
		}
		
		/**
		 * Remove child entities when despawned
		 */
		override public function removed():void 
		{
			FP.world.removeList( childEntities );
			
			super.removed();
		}
	}

}