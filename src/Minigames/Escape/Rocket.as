package Minigames.Escape 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import net.flashpunk.tweens.motion.LinearMotion;
	
	/**
	 * Entity for tank rockets
	 * 
	 * @author Switchbreak
	 */
	public class Rocket extends Entity 
	{
		
		// Private constants
		private static const ROCKET_LENGTH:int = 20;
		private static const ROCKET_WIDTH:int = 10;
		private static const ROCKET_COLOR:uint = 0xFFFFFF;
		private static const TARGET_SIZE:int = 10;
		private static const TARGET_COLOR:uint = 0xFF0000;
		private static const ROCKET_SPEED:Number = 150;
		
		// Private variables
		private var rocketFlight:LinearMotion;
		
		/**
		 * Create and initialize the rocket
		 * 
		 * @param	setX
		 * Rocket starting position
		 * @param	setY
		 * Rocket starting position
		 * @param	destX
		 * Rocket ending position
		 * @param	destY
		 * Rocket ending position
		 */
		public function Rocket( setX:int, setY:int, destX:int, destY:int ) 
		{
			// Create rocket graphic and point it at the destination
			var rocketSprite:Image = Image.createRect( ROCKET_LENGTH, ROCKET_WIDTH, ROCKET_COLOR );
			rocketSprite.centerOrigin();
			rocketSprite.angle = FP.angle( setX, setY, destX, destY );
			
			// Create the target graphic at the destination
			var targetImage:Image = Image.createCircle( TARGET_SIZE, TARGET_COLOR );
			targetImage.relative = false;
			targetImage.x = destX;
			targetImage.y = destY;
			targetImage.centerOrigin();
			
			// Initialize the entity
			super( setX, setY, new Graphiclist( rocketSprite, targetImage ) );
			
			// Set up the rocket flight motion
			rocketFlight = new LinearMotion( Explode, ONESHOT );
			rocketFlight.setMotionSpeed( x, y, destX, destY, ROCKET_SPEED );
			addTween( rocketFlight, true );
		}
		
		/**
		 * Explode the rocket
		 */
		protected function Explode():void
		{
			// Spawn an explosion
			FP.world.add( new Explosion( x - Explosion.EXPLOSION_SIZE, y - Explosion.EXPLOSION_SIZE ) );
			
			// Despawn the rocket
			FP.world.remove( this );
		}
		
		/**
		 * Handle rocket motion
		 */
		override public function update():void 
		{
			// Move rocket to follow flight path
			x = rocketFlight.x;
			y = rocketFlight.y;
			
			super.update();
		}
		
	}

}