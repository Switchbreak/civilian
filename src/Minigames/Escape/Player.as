package Minigames.Escape 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.utils.*;
	import net.flashpunk.graphics.Stamp;
	
	/**
	 * Player entity
	 * 
	 * @author Switchbreak
	 */
	public class Player extends Entity 
	{
		// Embeds
		[Embed (source = '../../../img/Farmer.png')] private static const FARMER:Class;
		
		// Public constants
		public static const PLAYER_COLLISION_TYPE:String = "player";
		public static const WALL_COLLISION_TYPE:String = "wall";
		public static const BARN_COLLISION_TYPE:String = "barn";
		public static const HOUSE_COLLISION_TYPE:String = "house";
		public static const SILO_COLLISION_TYPE:String = "silo";
		public static const TRACTOR_COLLISION_TYPE:String = "tractor";
		public static const TRAILER_COLLISION_TYPE:String = "trailer";
		
		public static const CARRY_NOTHING:int = 0;
		public static const CARRY_BELONGINGS:int = 1;
		public static const CARRY_GAS:int = 2;
		public static const CARRY_CROPS:int = 3;
		
		// Private constants
		private static const PLAYER_SPEED:Number = 150;
		private static const INVINCIBLE_TIME:Number = 2;
		private static const BLINK_RATE:Number = 0.01;
		private static const CARRY_ICON_SIZE:int = 5;
		private static const CARRY_ICON_COLOR:uint = 0xffff00;
		private static const BELONGINGS_GOAL:int = 3;
		private static const GAS_GOAL:int = 5;
		private static const PICKUP_TIME:Number = 1.0;
		private static const PICKUP_BAR_LENGTH:int = 20;
		private static const PICKUP_BAR_WIDTH:int = 5;
		private static const PICKUP_BAR_BACKCOLOR:uint = 0xffffff;
		private static const PICKUP_BAR_COLOR:uint = 0x00ff00;
		
		// Events
		private var eventWon:Function;
		
		// Private variables
		private var invincibleTimer:Number = 0;
		private var blinkTimer:Number = 0;
		private var _carrying:int = CARRY_NOTHING;
		private var trailerAvailable:Boolean = false;
		private var gasGathered:int = 0;
		private var belongingsGathered:int = 0;
		private var _pickupTimer:Number = 0;
		
		// Graphics
		private var farmer:Spritemap;
		private var carrySprite:Image;
		private var pickupBar:Image;
		private var pickupBarFill:Image;
		
		/**
		 * Create and initialize the player entity
		 * 
		 * @param setX
		 * Player starting position
		 * @param setY
		 * Player starting position
		 * @param setEventDeath
		 * Callback function to handle player death
		 * @param setEventLost
		 * Callback function to handle game loss event
		 * @param setEventWon
		 * Callback function to handle game win event
		 */
		public function Player( setX:int = 0, setY:int = 0, setEventWon:Function = null )
		{
			// Set object values
			eventWon = setEventWon;
			
			// Create graphic for farmer 
			farmer = new Spritemap( FARMER );
			
			// Create graphic for carrying icon
			carrySprite = Image.createRect( CARRY_ICON_SIZE, CARRY_ICON_SIZE, CARRY_ICON_COLOR );
			carrySprite.y = -CARRY_ICON_SIZE;
			carrySprite.visible = false;
			
			// Create graphic for pickup bar
			pickupBar = Image.createRect( PICKUP_BAR_LENGTH, PICKUP_BAR_WIDTH, PICKUP_BAR_BACKCOLOR );
			pickupBar.x = (farmer.width >> 1) - (PICKUP_BAR_LENGTH >> 1);
			pickupBar.y = -PICKUP_BAR_WIDTH;
			pickupBar.visible = false;
			pickupBarFill = Image.createRect( 1, PICKUP_BAR_WIDTH, PICKUP_BAR_COLOR );
			pickupBarFill.x = pickupBar.x;
			pickupBarFill.y = pickupBar.y;
			pickupBarFill.visible = false;
			
			// Create object and set hitbox
			super( setX, setY, new Graphiclist( farmer, carrySprite, pickupBar, pickupBarFill ) );
			type = PLAYER_COLLISION_TYPE;
			setHitboxTo( farmer );
			
			// Initialize the score display
			Escape.escapeWorld.SetGasScore( gasGathered, GAS_GOAL );
			Escape.escapeWorld.SetBelongingsScore( belongingsGathered, BELONGINGS_GOAL );
		}
		
		/**
		 * Handle player input and movement
		 */
		override public function update():void 
		{
			if ( !Escape.paused )
			{
				// Local variables
				var vectorX:Number = 0, vectorY:Number = 0;						// Movement vector
				
				//
				// Input
				//
				
				if ( Input.check( Key.UP ) || Input.check( Key.W ) )
				{
					vectorY -= 1.0;
				}
				if ( Input.check( Key.DOWN ) || Input.check( Key.S ) )
				{
					vectorY += 1.0;
				}
				if ( Input.check( Key.LEFT ) || Input.check( Key.A ) )
				{
					farmer.flipped = true;
					vectorX -= 1.0;
				}
				if ( Input.check( Key.RIGHT ) || Input.check( Key.D ) )
				{
					farmer.flipped = false;
					vectorX += 1.0;
				}
				
				//
				// Movement/collision detection
				//
				
				// Check that player is moving, to avoid divide by zero on normalize
				if ( vectorX != 0 || vectorY != 0 )
				{
					// Normalize movement vector and scale by movement speed
					var scaler:Number = (PLAYER_SPEED * FP.elapsed) / FP.distance( vectorX, vectorY );
					vectorX *= scaler;
					vectorY *= scaler;
					
					// Move and set new render order
					setHitbox( farmer.width, 10, 0, -(farmer.height - 10) );
					moveBy( vectorX, vectorY, WALL_COLLISION_TYPE );
					setHitboxTo( farmer );
					layer = FP.height - (y + farmer.height);
				}
				
				//
				// Handle events
				//
				
				// Handle damage events if the player is not in invincible mode
				if ( invincibleTimer <= 0 )
				{
					var bullet:Entity = collideTypes( [Soldier.LEFT_BULLET_TYPE, Soldier.RIGHT_BULLET_TYPE], x, y );
					if ( bullet != null )
						FP.world.remove( bullet );
					
					// Check for player collision with guard or bullets
					if ( collide( Soldier.SOLDIER_TYPE, x, y ) != null || bullet != null )
					{
						Hurt();
					}
					
					// Check for player collision with explosions
					var explosions:Vector.<Explosion> = new Vector.<Explosion>();
					collideInto( Explosion.EXPLODE_TYPE, x, y, explosions );
					for each( var explosion:Explosion in explosions )
					{
						// Check if the distance to the explosion is less than
						// the radius of it
						if( distanceToPoint( explosion.x + Explosion.EXPLOSION_SIZE, explosion.y + Explosion.EXPLOSION_SIZE, true ) < Explosion.EXPLOSION_SIZE )
							Hurt();
					}
				}
				else
				{
					Blink();
				}
				
				// If not carrying any load, check for player collision with
				// pick-ups to carry
				if ( _carrying == CARRY_NOTHING )
				{
					if ( gasGathered < GAS_GOAL && collide( BARN_COLLISION_TYPE, x, y ) != null )
					{
						PickUp( CARRY_GAS );
					}
					else if ( belongingsGathered < BELONGINGS_GOAL && collide( HOUSE_COLLISION_TYPE, x, y ) != null )
					{
						PickUp( CARRY_BELONGINGS );
					}
					else if ( trailerAvailable && collide( SILO_COLLISION_TYPE, x, y ) != null )
					{
						PickUp( CARRY_CROPS );
					}
					else
					{
						pickupTimer = 0;
					}
				}
				else if( _carrying == CARRY_GAS || _carrying == CARRY_BELONGINGS )
				{
					// Drop off gas or belongings at tractor
					if ( collide( TRACTOR_COLLISION_TYPE, x, y ) != null )
						DropOff();
				}
				else if ( _carrying == CARRY_CROPS )
				{
					// Drop off crops or belongings at the trailer
					if ( collide( TRAILER_COLLISION_TYPE, x, y ) != null )
						DropOff();
				}
			}
			
			super.update();
		}
		
		/**
		 * Function to pick up a type of object and carry it
		 * 
		 * @param	carryType
		 * Type of object to pick up
		 */
		private function PickUp( carryType:int ):void
		{
			pickupTimer += FP.elapsed;
			if ( _pickupTimer > PICKUP_TIME )
			{
				carrying = carryType;
				pickupTimer = 0;
			}
		}
		
		/**
		 * Function to drop off the object the player is carrying
		 */
		private function DropOff():void
		{
			// Increment counters for items gathered
			switch( _carrying )
			{
				case CARRY_BELONGINGS:
					belongingsGathered++;
					Escape.escapeWorld.SetBelongingsScore( belongingsGathered, BELONGINGS_GOAL );
					break;
				case CARRY_GAS:
					gasGathered++;
					Escape.escapeWorld.SetGasScore( gasGathered, GAS_GOAL );
					break;
			}
			
			// Reset the farmer carrying state
			carrying = CARRY_NOTHING;
			
			// Check for win state
			if ( belongingsGathered >= BELONGINGS_GOAL && gasGathered >= GAS_GOAL )
			{
				// Call the win event
				if ( eventWon != null )
					eventWon();
				
				// Hop out of minigame back to main game
				FP.world = GameWorld.gameWorld;
			}
		}
		
		/**
		 * Hurt the player when a hostile object is encountered
		 */
		private function Hurt():void
		{
			// Kill a game character
			Escape.sim.KillCharacter();
			
			// Set the player to invincible
			invincibleTimer = INVINCIBLE_TIME;
		}
		
		/**
		 * Display player blinking when invincible
		 */
		private function Blink():void
		{
			// If the player is in invincible mode, decrement the timer
			invincibleTimer -= FP.elapsed;
			
			// If the invincibility hasn't worn off, make the sprite blink
			if ( invincibleTimer > 0 )
			{
				if ( blinkTimer > 0 )
				{
					// Decrement the blink timer
					blinkTimer -= FP.elapsed;
				}
				else
				{
					// Blink timer has counted down, toggle the graphic
					// between visibility and invisibility
					farmer.visible = !farmer.visible;
					blinkTimer = BLINK_RATE;
				}
			}
			else
			{
				blinkTimer = 0;
				farmer.visible = true;
			}
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets or sets the current carried object
		 */
		private function set carrying( value:int ):void
		{
			// Set carrying value
			_carrying = value;
			
			// Show what the player is carrying
			if ( _carrying == CARRY_NOTHING )
				carrySprite.visible = false;
			else
				carrySprite.visible = true;
		}
		
		private function get carrying():int
		{
			return _carrying;
		}
		
		/**
		 * Gets or sets the current carried object
		 */
		private function set pickupTimer( value:Number ):void
		{
			// Set carrying value
			_pickupTimer = value;
			
			// Show what the player is carrying
			if ( _pickupTimer > 0 )
			{
				pickupBarFill.scaleX = (_pickupTimer / PICKUP_TIME) * PICKUP_BAR_LENGTH;
				pickupBar.visible = true;
				pickupBarFill.visible = true;
			}
			else
			{
				pickupBar.visible = false;
				pickupBarFill.visible = false;
			}
		}
		
		private function get pickupTimer():Number
		{
			return _pickupTimer;
		}
	}

}