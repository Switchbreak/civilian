package UI
{
	import net.flashpunk.graphics.Stamp;
	import Types.*;
	import UI.Elements.*;
	import mx.resources.ResourceManager
	import mx.utils.StringUtil;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	/**
	 * Entity to display Buy dialog box on screen
	 * 
	 * @author Switchbreak
	 */
	public class BuyScreen extends Entity 
	{
		// Assets
		[Embed(source = "../../img/UI/StoreScreen.png")] private static const STORE_SCREEN:Class;
		
		// Constants
		private static const WIDTH:int						= 205;
		private static const SPACING:int					= 5;
		private static const BACK_COLOR:uint				= 0;
		private static const BUTTON_WIDTH:int				= 60;
		private static const BUTTON_HEIGHT:int				= 25;
		private static const CART_COLOR:uint				= 0xFFFFFF;
		private static const CART_UNAVAILABLE_COLOR:uint	= 0xFF0000;
		private static const PANEL_X:int					= 220;
		private static const PANEL_Y:int					= 20;
		private static const LIST_HEIGHT:int				= 297;
		private static const QUANTITIES:Array				= [1, 40];
		
		private var itemType:int;
		private var cart:Vector.<Product>;
		private var listY:int;
		
		private var buyButton:Button;
		private var cancelButton:Button;
		private var items:Vector.<LineItem>;
		private var cartDisplay:Vector.<LineItem>;
		private var itemHeader:Text;
		private var storage:Text;
		private var price:Text;
		
		/**
		 * Create and display the Buy screen
		 */
		public function BuyScreen( okCallback:Function = null, cancelCallback:Function = null ) 
		{
			// Default type of item to buy is seed
			itemType = Product.TYPE_SEED;
			
			// Initialize cart
			cart = new Vector.<Product>();
			
			layer			= GameWorld.HUD_LAYER;
			graphic			= new Graphiclist();
			graphic.scrollX	= 0;
			graphic.scrollY	= 0;
			listY			= PANEL_Y;
			
			addGraphic( new Stamp( STORE_SCREEN ) );
			
			CreateItemList();
			PopulateItemList();
			
			CreateShoppingCart();
			UpdateShoppingCart();
			
			CreateButtons( okCallback, cancelCallback );
			addGraphic( new SpeechBox( "What can I sell you?" ) );
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Detect if an item was clicked
			if ( Input.mousePressed )
			{
				for ( var i:int = 0; i < items.length; i++ )
				{
					if ( Input.mouseX >= x + items[i].itemName.x && Input.mouseX <= x + items[i].itemName.x + WIDTH
						&& Input.mouseY >= y + items[i].itemName.y && Input.mouseY <= y + items[i].itemName.y + items[i].itemName.height )
					{
						AddToCart( i );
					}
				}
				
				for ( i = 0; i < cartDisplay.length; i++ )
				{
					if ( Input.mouseX >= x + cartDisplay[i].itemName.x && Input.mouseX <= x + cartDisplay[i].itemName.x + WIDTH
						&& Input.mouseY >= y + cartDisplay[i].itemName.y && Input.mouseY <= y + cartDisplay[i].itemName.y + cartDisplay[i].itemName.height )
					{
						RemoveFromCart( i );
					}
				}
				
				// Detect if the crop/seed toggle has been pressed
				if( Input.mouseX >= x + itemHeader.x && Input.mouseX <= x + itemHeader.x + itemHeader.width &&
					Input.mouseY >= y + itemHeader.y && Input.mouseY <= y + itemHeader.y + itemHeader.height )
				{
					SwitchItemType();
				}
				
				// Detect if the OK button has been pressed
				if ( !buyButton.buttonSelected && 
					Input.mouseX >= x + buyButton.x && Input.mouseX <= x + buyButton.x + BUTTON_WIDTH &&
					Input.mouseY >= y + buyButton.y && Input.mouseY <= y + buyButton.y + BUTTON_HEIGHT )
				{
					buyButton.buttonSelected = true;
				}
				
				// Detect if the cancel button has been pressed
				if ( !cancelButton.buttonSelected && 
					Input.mouseX >= x + cancelButton.x && Input.mouseX <= x + cancelButton.x + BUTTON_WIDTH &&
					Input.mouseY >= y + cancelButton.y && Input.mouseY <= y + cancelButton.y + BUTTON_HEIGHT )
				{
					cancelButton.buttonSelected = true;
				}
			}
				
			if ( Input.mouseUp )
			{
				if ( buyButton.buttonSelected )
				{
					buyButton.buttonSelected = false;
					
					if ( buyButton.callback != null &&
						Input.mouseX >= x + buyButton.x && Input.mouseX <= x + buyButton.x + BUTTON_WIDTH &&
						Input.mouseY >= y + buyButton.y && Input.mouseY <= y + buyButton.y + BUTTON_HEIGHT )
					{
						Buy();
					}
				}
				if ( cancelButton.buttonSelected )
				{
					cancelButton.buttonSelected = false;
					
					if ( cancelButton.callback != null &&
						Input.mouseX >= x + cancelButton.x && Input.mouseX <= x + cancelButton.x + BUTTON_WIDTH &&
						Input.mouseY >= y + cancelButton.y && Input.mouseY <= y + cancelButton.y + BUTTON_HEIGHT )
					{
						FP.world.remove( this );
						
						cancelButton.callback();
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Function to add an item from the Buy menu to the cart
		 * 
		 * @param	itemIndex
		 * Index of item on Buy menu
		 */
		private function AddToCart( itemIndex:int ):void
		{
			// Search through cart for a matching product
			var foundProduct:Boolean	= false;
			var itemQuantity:int		= QUANTITIES[itemType];
			
			var rowY:int = listY;
			for ( var i:int = 0; i < cart.length; i++ )
			{
				// If a matching product is found, tick up its quantity and update the display
				if ( cart[i].type == itemType && cart[i].plantType == itemIndex )
				{
					cart[i].quantity += itemQuantity;
					cartDisplay[i].amount.text = cart[i].quantity.toString();
					foundProduct = true;
					
					break;
				}
				
				rowY += cartDisplay[i].itemName.height + SPACING;
			}
			
			// If no matching product was found, add it to the cart as a new product
			if( !foundProduct )
			{
				var product:Product = CreateProduct( itemType, itemIndex, itemQuantity );
				cart.push( product );
				
				var lineItem:LineItem = CreateLineItem( GetPlantName( product ), itemQuantity, PANEL_X + WIDTH + SPACING, rowY );
				cartDisplay.push( lineItem );
			}
			
			UpdateShoppingCart();
		}
		
		/**
		 * Remove an item from the cart
		 * 
		 * @param	itemIndex
		 * Index of the item in the cart
		 */
		private function RemoveFromCart( itemIndex:int ):void
		{
			var itemQuantity:int = QUANTITIES[cart[itemIndex].type];
			
			// If more than one of the selected item are in the cart, tick the quantity down by one
			if ( cart[itemIndex].quantity > itemQuantity )
			{
				cart[itemIndex].quantity -= itemQuantity;
				cartDisplay[itemIndex].amount.text = cart[itemIndex].quantity.toString();
			}
			else
			{
				(graphic as Graphiclist).remove( cartDisplay[itemIndex].itemName );
				(graphic as Graphiclist).remove( cartDisplay[itemIndex].amount );
				
				var shift:int = cartDisplay[i].itemName.height + SPACING;
				for ( var i:int = itemIndex + 1; i < cartDisplay.length; i++ )
				{
					cartDisplay[i].itemName.y -= shift;
					cartDisplay[i].amount.y -= shift;
				}
				
				cart.splice( itemIndex, 1 );
				cartDisplay.splice( itemIndex, 1 );
			}
			
			UpdateShoppingCart();
		}
		
		/**
		 * Toggles the item type between crop and seed
		 */
		private function SwitchItemType():void
		{
			if ( itemType == Product.TYPE_CROP )
				itemType = Product.TYPE_SEED;
			else
				itemType = Product.TYPE_CROP;
			
			PopulateItemList();
		}
		
		/**
		 * Refreshes the list of items of the selected item type for sale
		 */
		private function PopulateItemList():void
		{
			var quantity:Number = QUANTITIES[itemType];
			
			SetHeaderText( itemType );
			
			// Show crops and prices
			for ( var crop:int; crop < GameWorld.sim.crops.length; crop++ )
			{
				// Get price of plant, applying markup for buying crops
				var price:Number = GameWorld.sim.GetPrice( itemType, crop, Product.TYPE_CROP );
				
				items[crop].amount.text		= StringUtil.substitute( "${0}", (price * quantity).toFixed( 2 ) );
				items[crop].amount.align	= "right";
				items[crop].amount.width	= WIDTH - (SPACING << 1);
			}
		}
		
		/**
		 * Shades items in cart that cannot be bought due to money or storage limitations
		 */
		private function UpdateShoppingCart():void
		{
			var totalStorage:int = 0;
			var totalPrice:Number = 0;
			
			// Count up items and mark all items after the storage or money
			// limit is reached
			for ( var i:int = 0; i < cart.length; i++ )
			{
				// Get price and storage space taken up by product in cart
				totalPrice += GameWorld.sim.GetPrice( cart[i].type, cart[i].plantType, Product.TYPE_CROP ) * cart[i].quantity;
				totalStorage += GameWorld.sim.farm.GetStorageRequired( cart[i] );
				
				if ( totalStorage <= GameWorld.sim.farm.storageSpace && totalPrice <= GameWorld.sim.farmer.money )
				{
					cartDisplay[i].itemName.color = CART_COLOR;
					cartDisplay[i].amount.color = CART_COLOR;
				}
				else
				{
					cartDisplay[i].itemName.color = CART_UNAVAILABLE_COLOR;
					cartDisplay[i].amount.color = CART_UNAVAILABLE_COLOR;
				}
			}
			
			// Display and color code totals
			UpdateTotals( totalStorage, totalPrice );
		}
		
		/**
		 * Buy all items in the cart, if they are not over our availability limits
		 */
		private function Buy():void
		{
			if ( GameWorld.sim.BuyProducts( cart ) )
			{
				FP.world.remove( this );
				
				buyButton.callback();
			}
		}
		
		/**
		 * Show the total lines and color code them
		 * @param	totalStorage	Total storage to display
		 * @param	totalPrice		Total price to display
		 */
		private function UpdateTotals( totalStorage:int, totalPrice:Number ):void 
		{
			storage.text = StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "BUY_STORAGE" ), totalStorage.toString() );
			price.text = "$" + totalPrice.toFixed( 2 );
			
			if ( totalStorage <= GameWorld.sim.farm.storageSpace )
				storage.color = CART_COLOR;
			else
				storage.color = CART_UNAVAILABLE_COLOR;
			
			if ( totalPrice <= GameWorld.sim.farmer.money )
				price.color = CART_COLOR;
			else
				price.color = CART_UNAVAILABLE_COLOR;
		}
		
		/**
		 * Set header text and item quantity by product type displayed
		 * @param	itemType	Type of item being sold
		 */
		private function SetHeaderText( itemType:int ):void 
		{
			switch( itemType )
			{
				case Product.TYPE_CROP:
					itemHeader.text = ResourceManager.getInstance().getString( "resources", "HEADER_CROPS" );
					break;
				case Product.TYPE_SEED:
					itemHeader.text = ResourceManager.getInstance().getString( "resources", "HEADER_SEEDS" );
					break;
			}
		}
		
		/**
		 * Create a product to be added to the cart
		 * @param	itemType	Type, either TYPE_SEED or TYPE_CROP
		 * @param	plantType	Plant type
		 * @param	quantity	Quantity of item
		 * @return	New product
		 */
		private function CreateProduct( itemType:int, plantType:int, quantity:int ):Product 
		{
			var product:Product	= new Product();
			product.type		= itemType;
			product.plantType	= plantType;
			product.quantity	= quantity;
			product.storageTime	= Product.STORAGE_TIME_FRESH;
			
			return product;
		}
		
		/**
		 * Gets the name to display for a product
		 * @param	product		Product to get name of
		 * @return	Name of the product
		 */
		private function GetPlantName( product:Product ):String 
		{
			var plantType:String = GameWorld.sim.crops[product.plantType].cropName;
			
			var cropType:String;
			switch( product.type )
			{
				case Product.TYPE_SEED:
					cropType = ResourceManager.getInstance().getString( "resources", "TYPE_SEED" );
					break;
				case Product.TYPE_CROP:
					cropType = ResourceManager.getInstance().getString( "resources", "TYPE_CROP" );
					break;
			}
			
			return StringUtil.substitute( "{0} {1}", plantType, cropType );
		}
		
		/**
		 * Creates a line item to show in the item list or shopping cart
		 * @param	text		Text to display
		 * @param	quantity	Quantity to display
		 * @param	itemX		Position of the line item
		 * @param	itemY		Position of the line item
		 * @return	New line item
		 */
		private function CreateLineItem( text:String, quantity:int, itemX:int, itemY:int ):LineItem 
		{
			var lineItem:LineItem	= new LineItem();
			lineItem.itemName		= new Text( text, itemX, itemY );
			lineItem.amount			= new Text( quantity.toString(), itemX, itemY );
			lineItem.amount.align	= "right";
			lineItem.amount.width	= WIDTH - (SPACING << 1);
			
			addGraphic( lineItem.itemName );
			addGraphic( lineItem.amount );
			
			return lineItem;
		}
		
		/**
		 * Create list of items for sale
		 */
		private function CreateItemList():void 
		{
			var rowY:int = PANEL_Y;
			
			itemHeader = new Text( " ", PANEL_X + SPACING, rowY );
			addGraphic( itemHeader );
			
			items = new Vector.<LineItem>();
			rowY += itemHeader.height + SPACING;
			for ( var crop:int = 0; crop < GameWorld.sim.crops.length; crop++ )
			{
				var lineItem:LineItem = CreateLineItem( StringUtil.substitute( "{0}. {1}", crop + 1, GameWorld.sim.crops[crop].cropName ), 0, PANEL_X, rowY );
				items.push( lineItem );
				rowY += lineItem.itemName.height + SPACING;
			}
		}
		
		/**
		 * Create a list of items for the shopping cart
		 * @param	Height to place the totals at
		 */
		private function CreateShoppingCart():void 
		{
			var cartHeader:Text = new Text( ResourceManager.getInstance().getString( "resources", "HEADER_CART" ), PANEL_X + WIDTH + SPACING, listY );
			
			cartDisplay			= new Vector.<LineItem>();
			storage				= new Text( "", PANEL_X + WIDTH + SPACING, LIST_HEIGHT );
			price				= new Text( "", PANEL_X + WIDTH + SPACING, LIST_HEIGHT );
			price.align			= "right";
			price.width			= WIDTH - (SPACING << 1);
			
			addGraphic( cartHeader );
			addGraphic( storage );
			addGraphic( price );
			
			listY += cartHeader.height + SPACING;
		}
		
		/**
		 * Add the buttons to the dialogue box
		 * @param	okCallback			Event to fire on OK button
		 * @param	cancelCallback		Event to fire on cancel button
		 * @param	rowY				Position to place the button
		 */
		private function CreateButtons( okCallback:Function, cancelCallback:Function ):void 
		{
			// Add buttons
			buyButton = new Button( ResourceManager.getInstance().getString( "resources", "OK_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, okCallback );
			buyButton.x = PANEL_X + (WIDTH << 1) - SPACING - BUTTON_WIDTH;
			buyButton.y = LIST_HEIGHT + storage.height + SPACING;
			
			cancelButton = new Button( ResourceManager.getInstance().getString( "resources", "CANCEL_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, cancelCallback );
			cancelButton.x = buyButton.x - SPACING - BUTTON_WIDTH;
			cancelButton.y = buyButton.y;
			
			addGraphic( buyButton );
			addGraphic( cancelButton );
		}
		
	}
}

import net.flashpunk.graphics.Text;

/**
 * Object containing graphics for buy screen line items
 */
class LineItem
{
	public var itemName:Text;
	public var amount:Text;
}