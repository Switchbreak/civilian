package UI 
{
	import flash.display.InteractiveObject;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import Graphics.HexMap;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import Types.Country;
	import Types.Farm;
	import Types.Field;
	import Types.MapPosition;
	
	/**
	 * Farm tilemap game object
	 * @author Switchbreak
	 */
	public class FarmMap extends Entity 
	{
		// Assets
		[Embed(source = "../../img/FarmTiles/TileSheet.png")]		private static const FARM_TILES:Class;
		[Embed(source = "../../img/FarmTiles/SeedTile.png")]		private static const SEED_TILE:Class;
		[Embed(source = "../../img/FarmTiles/SproutTile.png")]		private static const SPROUT_TILE:Class;
		[Embed(source = "../../img/FarmTiles/TendedTile.png")]		private static const TENDED_TILE:Class;
		[Embed(source = "../../img/FarmTiles/WitheredTile.png")]	private static const WITHERED_TILE:Class;
		[Embed(source = "../../img/FarmTiles/RipeTile.png")]		private static const RIPE_TILE:Class;
		[Embed(source = "../../img/FarmTiles/TreeSheet.png")]		private static const TREE_TILE:Class;
		[Embed(source = "../../img/FarmBuildings/BarnTile.png")]	private static const BARN_TILE:Class;
		[Embed(source = "../../img/FarmBuildings/HouseTile.png")]	private static const HOUSE_TILE:Class;
		[Embed(source = "../../img/FarmBuildings/SiloTile.png")]	private static const SILO_TILE:Class;
		
		// Constants
		static private const TILE_WIDTH:int			= 64;
		static private const TILE_HEIGHT:int		= 35;
		static private const BASE_WIDTH:int			= 64;
		static private const BASE_HEIGHT:int		= 32;
		static private const TREE_WIDTH:int			= 148;
		static private const TREE_HEIGHT:int		= 160;
		static private const TREE_ORIGIN_X:int		= 66;
		static private const TREE_ORIGIN_Y:int		= 150;
		static private const TREE_LAYER_OFFSET:int	= 16;
		static private const BARN_WIDTH:int			= 57;
		static private const BARN_HEIGHT:int		= 56;
		static private const HOUSE_WIDTH:int		= 57;
		static private const HOUSE_HEIGHT:int		= 40;
		static private const SILO_WIDTH:int			= 59;
		static private const SILO_HEIGHT:int		= 100;
		
		static private const RIVER_ADJACENCY_TABLE:Array	= [[1, 2, 0], [6, 4, 5], [1, 3, 0]];
		
		static private const HEX_TILE_FIELD:int		= 1;
		static private const HEX_TILE_RIVER:int		= 2;
		static private const HEX_TILE_BRIDGE:int	= 9;
		static private const HEX_TILE_WINTER:int	= 12;
		
		// Variables
		public var	farmTiles:HexMap;
		private var mapObjects:Vector.<Entity>;
		private var country:Country;
		
		/**
		 * Create and initialize the farm map
		 * @param	setCountry		Starting country for the map
		 */
		public function FarmMap( setCountry:Country ) 
		{
			farmTiles = new HexMap( FARM_TILES, Country.TILE_MAP_WIDTH, Country.TILE_MAP_HEIGHT, TILE_WIDTH, TILE_HEIGHT, BASE_WIDTH, BASE_HEIGHT );
			farmTiles.floodFill( 0, 0 );
			
			graphic		= farmTiles;
			active		= false;
			mapObjects	= new Vector.<Entity>();
			country		= setCountry;
		}
		
		/**
		 * Add child objects when added to the world
		 */
		override public function added():void 
		{
			super.added();
			UpdateMap();
		}
		
		/**
		 * Remove child objects when removed from the world
		 */
		override public function removed():void 
		{
			super.removed();
			RemoveMapObjects();
		}
		
		/**
		 * Loads a map from a Farm object
		 * @param	farm	Farm containing map to show
		 */
		public function LoadCountryMap( setCountry:Country ):void
		{
			country = setCountry;
			UpdateMap();
		}
		
		/**
		 * Updates the map to reflect the current farm object
		 */
		public function UpdateMap():void
		{
			var grassTile:int = (GameWorld.sim.season == Sim.SEASON_WINTER ? HEX_TILE_WINTER : 0);
			farmTiles.setRect( 0, 0, farmTiles.columns, farmTiles.rows, grassTile );
			
			RemoveMapObjects();
			
			for ( var row:int = 0; row < Country.TILE_MAP_HEIGHT; ++row )
			{
				for ( var column:int = 0; column < Country.TILE_MAP_WIDTH; ++column )
				{
					AddMapTile( country.mapTiles[row * Country.TILE_MAP_WIDTH + column], row, column );
				}
			}
		}
		
		/**
		 * Adds a map tile specified by the country's tileset, called by
		 * UpdateMap function
		 * @param	tile		Type of tile to add, from Country tile types
		 * @param	row			Position of tile to add
		 * @param	column		Position of tile to add
		 */
		private function AddMapTile( tile:int, row:int, column:int ):void
		{
			switch( tile )
			{
				case Country.TILE_TREE:
					AddTree( row, column );
					break;
				case Country.TILE_SEED:
					AddField( row, column, new Stamp( SEED_TILE ) );
					break;
				case Country.TILE_SPROUT:
					AddField( row, column, new Stamp( SPROUT_TILE ) );
					break;
				case Country.TILE_TENDED:
					AddField( row, column, new Stamp( TENDED_TILE ) );
					break;
				case Country.TILE_RIPE:
					AddField( row, column, new Stamp( RIPE_TILE ) );
					break;
				case Country.TILE_WITHERED:
					AddField( row, column, new Stamp( WITHERED_TILE ) );
					break;
				case Country.TILE_RIVER:
					AddRiver( row, column );
					break;
				case Country.TILE_BRIDGE:
					AddBridge( row, column );
					break;
				case Country.TILE_FARM:
					if( GameWorld.sim.farm != null )
						farmTiles.setTile( column, row, HEX_TILE_FIELD );
					break;
				case Country.TILE_BARN:
					AddBuilding( row, column, BARN_TILE, BARN_WIDTH, BARN_HEIGHT );
					break;
				case Country.TILE_HOUSE:
					AddBuilding( row, column, HOUSE_TILE, HOUSE_WIDTH, HOUSE_HEIGHT );
					break;
				case Country.TILE_SILO:
					AddBuilding( row, column, SILO_TILE, SILO_WIDTH, SILO_HEIGHT );
					break;
			}
		}
		
		/**
		 * Add a tree to the map
		 * @param	position	Map position to place the tree
		 */
		public function AddTree( row:int, column:int ):void
		{
			var clipRect:Rectangle = new Rectangle( 0, 0, TREE_WIDTH, TREE_HEIGHT );
			if ( GameWorld.sim.season == Sim.SEASON_FALL )
				clipRect.x = TREE_WIDTH;
			else if ( GameWorld.sim.season == Sim.SEASON_WINTER )
				clipRect.x = (TREE_WIDTH << 1);
			
			var tree:Image = new Image( TREE_TILE, clipRect );
			tree.originX = TREE_ORIGIN_X;
			tree.originY = TREE_ORIGIN_Y;
			
			var point:Point = farmTiles.GetPositionAtTile( row, column );
			mapObjects.push( world.addGraphic( tree, -point.y - TREE_LAYER_OFFSET, point.x + (BASE_WIDTH >> 1), point.y + BASE_HEIGHT ) );
		}
		
		/**
		 * Add a building to the map
		 * @param	row				Map position to place the building
		 * @param	column			Map position to place the building
		 * @param	buildingImage	Building image
		 * @param	width			Image dimensions
		 * @param	height			Image dimensions
		 */
		public function AddBuilding( row:int, column:int, buildingImage:Class, width:int, height:int ):void
		{
			if ( GameWorld.sim.farm == null )
				return;
			
			var clipRect:Rectangle = new Rectangle( 0, 0, width, height );
			if ( GameWorld.sim.season == Sim.SEASON_WINTER )
			{
				clipRect.x = width;
				farmTiles.setTile( column, row, HEX_TILE_WINTER );
			}
			else
			{
				farmTiles.setTile( column, row, HEX_TILE_FIELD );
			}
			
			var buildingGraphic:Image = new Image( buildingImage, clipRect );
			
			var point:Point = farmTiles.GetPositionAtTile( row, column );
			mapObjects.push( world.addGraphic( buildingGraphic, -point.y - (BASE_HEIGHT >> 1), point.x + (BASE_WIDTH >> 1) - (buildingGraphic.width >> 1), point.y + BASE_HEIGHT - buildingGraphic.height ) );
		}
		
		/**
		 * Add a graphic for a farm field
		 * @param	field	Field to add
		 */
		public function AddField( row:int, column:int, fieldGraphic:Stamp ):void
		{
			farmTiles.setTile( column, row, HEX_TILE_FIELD );
			var point:Point = farmTiles.GetPositionAtTile( row, column );
			mapObjects.push( world.addGraphic( fieldGraphic, -point.y, point.x, point.y ) );
		}
		
		/**
		 * Set a tile to a river, choosing the appropriate hex tile to match
		 * adjacent river tiles
		 * @param	row		Map position to place the river
		 * @param	column	Map position to place the river
		 */
		public function AddRiver( row:int, column:int ):void
		{
			var upperAdjacency:int = FindAdjacentRiver( row, column, true );
			var lowerAdjacency:int = FindAdjacentRiver( row, column, false );
			
			farmTiles.setTile( column, row, HEX_TILE_RIVER + RIVER_ADJACENCY_TABLE[upperAdjacency][lowerAdjacency] + (GameWorld.sim.season == Sim.SEASON_WINTER ? HEX_TILE_WINTER : 0) );
		}
		
		/**
		 * Set a tile to a bridge, choosing the appropriate hex tile to match
		 * adjacent river tiles
		 * @param	row		Map position to place the bridge
		 * @param	column	Map position to place the bridge
		 */
		public function AddBridge( row:int, column:int ):void
		{
			var upperAdjacency:int = FindAdjacentRiver( row, column, true );
			
			farmTiles.setTile( column, row, HEX_TILE_BRIDGE + upperAdjacency + (GameWorld.sim.season == Sim.SEASON_WINTER ? HEX_TILE_WINTER : 0) );
		}
		
		/**
		 * Find any river tiles adjacent either above or below the selected tile
		 * @param	row		Tile to search around
		 * @param	column	Tile to search around
		 * @param	upper	True to search three tiles above, false for below
		 * @return	Adjacent tile found: 0 - left, 1 - middle, 2 - right
		 */
		private function FindAdjacentRiver( row:int, column:int, upper:Boolean ):int
		{
			var lowColumn:int = column % 2;
			
			for (var i:int = -1; i <= 1; ++i) 
			{
				var rowStep:int;
				if( upper )
					rowStep = (lowColumn == 0 || i == 0 ? -1 : 0);
				else
					rowStep = (lowColumn == 1 || i == 0 ? 1 : 0);
				
				if ( row + rowStep >= 0 && row + rowStep < Country.TILE_MAP_HEIGHT && column + i >= 0 && column + i < Country.TILE_MAP_WIDTH )
				{
					if (	country.mapTiles[(row + rowStep) * Country.TILE_MAP_WIDTH + column + i] == Country.TILE_RIVER ||
							country.mapTiles[(row + rowStep) * Country.TILE_MAP_WIDTH + column + i] == Country.TILE_BRIDGE )
					{
						return i + 1;
					}
				}
			}
			
			return 1;
		}
		
		/**
		 * Remove all map objects
		 */
		public function RemoveMapObjects():void
		{
			world.removeList( mapObjects );
		}
		
		/**
		 * Gets the line separating two adjacent hexes and returns it in the
		 * parameters p1 and p2
		 * @param	position	Map position of adjacent hexes
		 * @param	destPos		Map position of adjacent hexes
		 * @param	p1			Border line between them - output parameter
		 * @param	p2			Border line between them - output parameter
		 */
		public function GetAdjacentBorder( position:MapPosition, destPos:MapPosition, p1:Point, p2:Point ):void
		{
			if ( position.column == destPos.column )
			{
				if ( position.row > destPos.row )
				{
					// Below
					p1.x = BASE_WIDTH - (BASE_WIDTH >> 2);
					p1.y = BASE_HEIGHT;
					p2.x = (BASE_WIDTH >> 2);
					p2.y = BASE_HEIGHT;
				}
				else
				{
					// Above
					p1.x = (BASE_WIDTH >> 2);
					p1.y = 0;
					p2.x = BASE_WIDTH - (BASE_WIDTH >> 2);
					p2.y = 0;
				}
			}
			else
			{
				var lowColumn:int = Math.abs( destPos.column % 2 );
				if ( position.row < destPos.row + lowColumn )
				{
					if ( position.column < destPos.column )
					{
						// Upper left
						p1.x = 0;
						p1.y = (BASE_HEIGHT >> 1);
						p2.x = (BASE_WIDTH >> 2);
						p2.y = 0;
					}
					else
					{
						// Upper right
						p1.x = BASE_WIDTH - (BASE_WIDTH >> 2);
						p1.y = 0;
						p2.x = BASE_WIDTH;
						p2.y = (BASE_HEIGHT >> 1);
					}
				}
				else
				{
					if ( position.column < destPos.column )
					{
						// Lower left
						p1.x = (BASE_WIDTH >> 2);
						p1.y = BASE_HEIGHT;
						p2.x = 0;
						p2.y = (BASE_HEIGHT >> 1);
					}
					else
					{
						// Lower right
						p1.x = BASE_WIDTH;
						p1.y = (BASE_HEIGHT >> 1);
						p2.x = BASE_WIDTH - (BASE_WIDTH >> 2);
						p2.y = BASE_HEIGHT;
					}
				}
			}
			
			var tileLocation:Point = farmTiles.GetPositionAtTile( destPos.row, destPos.column );
			p1.x += tileLocation.x;
			p1.y += tileLocation.y;
			p2.x += tileLocation.x;
			p2.y += tileLocation.y;
		}
	}
}