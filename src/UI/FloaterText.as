package UI 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.MultiVarTween;
	import net.flashpunk.tweens.misc.VarTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.FP;
	
	/**
	 * Object to show a piece of text then float up and fade away
	 * @author Switchbreak
	 */
	public class FloaterText extends Entity 
	{
		// Constants
		private static const ASCEND_AMOUNT:Number = 40;
		private static const ASCEND_TIME:Number = 1.0;
		
		// Graphics
		private var floater:Text;
		
		/**
		 * Creates and initializes the FloaterText entity
		 * 
		 * @param	setX
		 * Sets the starting position of the entity
		 * @param	setY
		 * Sets the starting position of the entity
		 * @param	text
		 * Text to display
		 */
		public function FloaterText( setX:int, setY:int, text:String )
		{
			// Create text object
			floater = new Text( text );
			floater.centerOrigin();
			
			// Initialize the entity
			layer = GameWorld.HUD_LAYER;
			super( setX, setY, floater );
			
			// Set up motion
			var floatFade:MultiVarTween = new MultiVarTween( RemoveFloater, ONESHOT );
			floatFade.tween( floater, { y: -ASCEND_AMOUNT, alpha: 0 }, ASCEND_TIME );
			addTween( floatFade, true );
		}
		
		/**
		 * Function to remove the entity from the world after it has finished
		 * floating away and fading out
		 */
		public function RemoveFloater():void
		{
			FP.world.remove( this );
		}
		
	}

}