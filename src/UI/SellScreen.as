package UI
{
	import net.flashpunk.graphics.Stamp;
	import Types.*;
	import UI.Elements.*;
	import mx.resources.ResourceManager
	import mx.utils.StringUtil;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	/**
	 * Entity to display Buy dialog box on screen
	 * @author Switchbreak
	 */
	public class SellScreen extends Entity 
	{
		// Embeds
		[Embed(source = "../../img/UI/StoreScreen.png")] private static const STORE_SCREEN:Class;
		
		// Constants
		private static const WIDTH:int					= 207;
		private static const SPACING:int				= 2;
		private static const LINE_SPACING:int			= -2;
		private static const BACK_COLOR:uint			= 0;
		private static const BUTTON_WIDTH:int			= 60;
		private static const BUTTON_HEIGHT:int			= 25;
		private static const ITEM_COLOR:uint			= 0xFFFFFF;
		private static const ITEM_IN_CART_COLOR:uint	= 0x00FF00;
		private static const PANEL_X:int				= 215;
		private static const PANEL_Y:int				= 12;
		private static const LIST_HEIGHT:int			= 278;
		
		private var inCart:Vector.<Boolean>;
		private var cart:Vector.<int>;
		private var listY:int;
		
		private var sellButton:Button;
		private var cancelButton:Button;
		private var items:Vector.<LineItem>;
		private var cartDisplay:Vector.<LineItem>;
		private var priceDisplay:Text;
		private var taxDisplay:Text;
		
		/**
		 * Create and display the Buy screen
		 */
		public function SellScreen( okCallback:Function = null, cancelCallback:Function = null ) 
		{
			inCart	= new Vector.<Boolean>();
			cart	= new Vector.<int>();
			
			layer			= GameWorld.HUD_LAYER;
			graphic			= new Graphiclist();
			graphic.scrollX	= 0;
			graphic.scrollY	= 0;
			
			var rowY:int	= PANEL_Y;
			listY			= rowY;
			
			// Add graphics for list of items available to sell
			var itemHeader:Text = new Text( ResourceManager.getInstance().getString( "resources", "HEADER_STORAGE" ), PANEL_X, rowY );
			rowY += itemHeader.height + SPACING;
			items = new Vector.<LineItem>();
			for ( var i:int = 0; i < GameWorld.sim.farm.silo.length; i++ )
			{
				// Get product from silo
				var product:Product = GameWorld.sim.farm.silo[i];
				var plantType:String = ResourceManager.getInstance().getString( "resources", "LIST_NONE" );
				var price:Number = 0;
				
				// Initialize line item
				var lineItem:LineItem = new LineItem();
				lineItem.amount = new Text( "", PANEL_X, rowY );
				lineItem.amount.align = "right";
				lineItem.amount.width = WIDTH - (SPACING << 1);
				
				// If the silo slot isn't empty, get product info to display
				if ( product != null )
				{
					// Get plant name by product plant type
					plantType = GameWorld.sim.crops[product.plantType].cropName;
					
					// Get the price, applying markup for selling seeds
					if( product.storageTime < Product.STORAGE_TIME_ROTTEN )
						price = GameWorld.sim.GetPrice( product.type, product.plantType, -1, Product.TYPE_SEED );
					
					lineItem.amount.text = product.quantity + " / $" + price.toFixed( 2 );
				}
				
				lineItem.itemName = new Text( plantType, PANEL_X, rowY );
				
				// Add line item to display
				items.push( lineItem );
				rowY += lineItem.itemName.height + LINE_SPACING;
			}
			
			// Add cart graphics
			var cartHeader:Text = new Text( ResourceManager.getInstance().getString( "resources", "HEADER_CART" ), PANEL_X + WIDTH + SPACING, listY );
			listY += cartHeader.height + SPACING;
			cartDisplay = new Vector.<LineItem>();
			
			rowY = LIST_HEIGHT;
			
			// Add cart tax graphic
			taxDisplay = new Text( " ", PANEL_X + WIDTH + SPACING, rowY );
			taxDisplay.align = "right";
			taxDisplay.width = WIDTH - (SPACING << 1);
			rowY += taxDisplay.height + LINE_SPACING;
			
			// Add cart total graphic
			priceDisplay = new Text( " ", PANEL_X + WIDTH + SPACING, rowY );
			priceDisplay.align = "right";
			priceDisplay.width = WIDTH - (SPACING << 1);
			rowY += priceDisplay.height + (SPACING << 1);
			
			UpdateCartTotals();
			
			// Add buttons
			sellButton = new Button( ResourceManager.getInstance().getString( "resources", "OK_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, okCallback );
			sellButton.x = PANEL_X + (WIDTH << 1) - SPACING - BUTTON_WIDTH;
			sellButton.y = rowY;
			
			cancelButton = new Button( ResourceManager.getInstance().getString( "resources", "CANCEL_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, cancelCallback );
			cancelButton.x = sellButton.x - (SPACING << 1) - BUTTON_WIDTH;
			cancelButton.y = rowY;
			
			rowY += BUTTON_HEIGHT + SPACING;
			
			// Add all graphics to the dialog box
			addGraphic( new Stamp( STORE_SCREEN ) );
			addGraphic( itemHeader );
			addGraphic( cartHeader );
			for each( var item:LineItem in items )
			{
				addGraphic( item.itemName );
				addGraphic( item.amount );
			}
			addGraphic( taxDisplay );
			addGraphic( priceDisplay );
			addGraphic( sellButton );
			addGraphic( cancelButton );
			
			addGraphic( new SpeechBox( "Selling something?" ) );
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Detect if an item was clicked
			if ( Input.mousePressed )
			{
				for ( var i:int = 0; i < items.length; i++ )
				{
					if ( Input.mouseX >= x + items[i].itemName.x && Input.mouseX <= x + items[i].itemName.x + WIDTH
						&& Input.mouseY >= y + items[i].itemName.y && Input.mouseY <= y + items[i].itemName.y + items[i].itemName.height )
					{
						AddToCart( i );
					}
				}
				
				for ( i = 0; i < cartDisplay.length; i++ )
				{
					if ( Input.mouseX >= x + cartDisplay[i].itemName.x && Input.mouseX <= x + cartDisplay[i].itemName.x + WIDTH
						&& Input.mouseY >= y + cartDisplay[i].itemName.y && Input.mouseY <= y + cartDisplay[i].itemName.y + cartDisplay[i].itemName.height )
					{
						RemoveFromCart( i );
					}
				}
				
				// Detect if the OK button has been pressed
				if ( !sellButton.buttonSelected && 
					Input.mouseX >= x + sellButton.x && Input.mouseX <= x + sellButton.x + BUTTON_WIDTH &&
					Input.mouseY >= y + sellButton.y && Input.mouseY <= y + sellButton.y + BUTTON_HEIGHT )
				{
					sellButton.buttonSelected = true;
				}
				
				// Detect if the OK button has been pressed
				if ( !cancelButton.buttonSelected && 
					Input.mouseX >= x + cancelButton.x && Input.mouseX <= x + cancelButton.x + BUTTON_WIDTH &&
					Input.mouseY >= y + cancelButton.y && Input.mouseY <= y + cancelButton.y + BUTTON_HEIGHT )
				{
					cancelButton.buttonSelected = true;
				}
			}
				
			if ( Input.mouseUp )
			{
				if ( sellButton.buttonSelected )
				{
					sellButton.buttonSelected = false;
					
					if ( sellButton.callback != null &&
						Input.mouseX >= x + sellButton.x && Input.mouseX <= x + sellButton.x + BUTTON_WIDTH &&
						Input.mouseY >= y + sellButton.y && Input.mouseY <= y + sellButton.y + BUTTON_HEIGHT )
					{
						Sell();
					}
				}
				if ( cancelButton.buttonSelected )
				{
					cancelButton.buttonSelected = false;
					
					if ( cancelButton.callback != null &&
						Input.mouseX >= x + cancelButton.x && Input.mouseX <= x + cancelButton.x + BUTTON_WIDTH &&
						Input.mouseY >= y + cancelButton.y && Input.mouseY <= y + cancelButton.y + BUTTON_HEIGHT )
					{
						FP.world.remove( this );
						
						cancelButton.callback();
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Function to add an item from the Buy menu to the cart
		 * 
		 * @param	itemIndex
		 * Index of item on Buy menu
		 */
		private function AddToCart( itemIndex:int ):void
		{
			// Check if slot is empty before adding to cart
			if ( GameWorld.sim.farm.silo[itemIndex] == null )
				return;
			
			// Color code item in items list
			items[itemIndex].itemName.color	= ITEM_IN_CART_COLOR;
			items[itemIndex].amount.color	= ITEM_IN_CART_COLOR;
			
			// Position cursor at the bottom of the cart list to add new item
			var rowY:int = listY;
			for ( var i:int = 0; i < cart.length; i++ )
			{
				rowY += cartDisplay[i].itemName.height + LINE_SPACING;
				if ( cart[i] == itemIndex )
					return;
			}
			
			// Add item to cart
			cart.push( itemIndex );
			
			// Create new display object for item in cart
			var lineItem:LineItem	= new LineItem();
			lineItem.itemName		= new Text( GameWorld.sim.crops[GameWorld.sim.farm.silo[itemIndex].plantType].cropName, PANEL_X + WIDTH + SPACING, rowY );
			lineItem.amount			= new Text( items[itemIndex].amount.text, PANEL_X + WIDTH + SPACING, rowY );
			lineItem.amount.align	= "right";
			lineItem.amount.width	= WIDTH - (SPACING << 1);
			cartDisplay.push( lineItem );
			
			addGraphic( lineItem.itemName );
			addGraphic( lineItem.amount );
			
			UpdateCartTotals();
		}
		
		/**
		 * Remove an item from the cart
		 * 
		 * @param	itemIndex
		 * Index of the item in the cart
		 */
		private function RemoveFromCart( cartIndex:int ):void
		{
			items[cart[cartIndex]].itemName.color	= ITEM_COLOR;
			items[cart[cartIndex]].amount.color		= ITEM_COLOR;
			
			(graphic as Graphiclist).remove( cartDisplay[cartIndex].itemName );
			(graphic as Graphiclist).remove( cartDisplay[cartIndex].amount );
			
			var shift:int = cartDisplay[cartIndex].itemName.height + LINE_SPACING;
			for ( var i:int = cartIndex + 1; i < cartDisplay.length; i++ )
			{
				cartDisplay[i].itemName.y	-= shift;
				cartDisplay[i].amount.y		-= shift;
			}
			
			cart.splice( cartIndex, 1 );
			cartDisplay.splice( cartIndex, 1 );
			
			UpdateCartTotals();
		}
		
		/**
		 * Updates the sale prices of all the items in the cart and totals them
		 */
		private function UpdateCartTotals():void
		{
			var totalPrice:Number = 0;
			for ( var i:int = 0; i < cart.length; i++ )
			{
				if ( GameWorld.sim.farm.silo[cart[i]].storageTime < Product.STORAGE_TIME_ROTTEN )
				{
					totalPrice += GameWorld.sim.GetPrice( GameWorld.sim.farm.silo[cart[i]].type, GameWorld.sim.farm.silo[cart[i]].plantType, -1, Product.TYPE_SEED ) * GameWorld.sim.farm.silo[cart[i]].quantity;
				}
			}
			var totalTax:Number = totalPrice * GameWorld.sim.country.tax;
			totalPrice -= totalTax;
			
			taxDisplay.text = StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "TAX_DISPLAY" ), totalTax.toFixed( 2 ) );
			priceDisplay.text = "$" + totalPrice.toFixed( 2 );
		}
		
		/**
		 * Sell all items in the cart, if they are not over our availability limits
		 */
		private function Sell():void
		{
			GameWorld.sim.SellProducts( cart );
			
			FP.world.remove( this );
			sellButton.callback();
		}
		
	}
}

import net.flashpunk.graphics.Text;

/**
 * Object containing graphics for buy screen line items
 */
class LineItem
{
	public var itemName:Text;
	public var amount:Text;
}