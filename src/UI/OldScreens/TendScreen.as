package UI.OldScreens
{
	import Types.*;
	import UI.Elements.*;
	import mx.resources.ResourceManager
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	/**
	 * Entity to display Plant dialog box on screen
	 * 
	 * @author Switchbreak
	 */
	public class TendScreen extends Entity 
	{
		
		private static const WIDTH:int = 300;
		private static const SPACING:int = 10;
		private static const BACK_COLOR:uint = 0;
		private static const BUTTON_WIDTH:int = 60;
		private static const BUTTON_HEIGHT:int = 25;
		
		private var okButton:Button;
		private var fields:Vector.<Text>;
		
		/**
		 * Create and display the Tend screen
		 */
		public function TendScreen( callback:Function = null )
		{
			var rowY:int = SPACING;
			var fieldHeader:Text = new Text( ResourceManager.getInstance().getString( "resources", "HEADER_FIELDS" ), SPACING, rowY );
			rowY += fieldHeader.height + SPACING;
			
			fields = new Vector.<Text>();
			for ( var i:int = 0; i < Farm.FIELD_COUNT; i++ )
			{
				var fieldName:String = ResourceManager.getInstance().getString( "resources", "LIST_EMPTY" );
				
				if ( GameWorld.sim.farm.fields[i] != null )
				{
					fieldName = GameWorld.sim.crops[GameWorld.sim.farm.fields[i].plantType];
					
					switch( GameWorld.sim.farm.fields[i].age )
					{
						case Field.AGE_SEED:
							fieldName += " - " + ResourceManager.getInstance().getString( "resources", "AGE_SEED" );
							break;
						case Field.AGE_SPROUT:
							fieldName += " - " + ResourceManager.getInstance().getString( "resources", "AGE_SPROUT" );
							break;
						case Field.AGE_RIPE:
							fieldName += " - " + ResourceManager.getInstance().getString( "resources", "AGE_RIPE" );
							break;
						default:
							fieldName += " - " + ResourceManager.getInstance().getString( "resources", "AGE_WITHERED" );
					}
					
					if ( GameWorld.sim.farm.fields[i].tended )
						fieldName += " - " + ResourceManager.getInstance().getString( "resources", "FIELD_TENDED" );
				}
				
				var lineItem:Text = new Text( (i + 1) + ". " + fieldName, SPACING, rowY );
				
				fields.push( lineItem );
				rowY += lineItem.height + SPACING;
			}
			
			rowY += SPACING;
			
			okButton = new Button( ResourceManager.getInstance().getString( "resources", "OK_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, callback );
			okButton.x = WIDTH - SPACING - BUTTON_WIDTH;
			okButton.y = rowY;
			
			rowY += BUTTON_HEIGHT + SPACING;
			
			addGraphic( Image.createRect( WIDTH, rowY, BACK_COLOR ) );
			addGraphic( fieldHeader );
			
			for each( var item:Text in fields )
				addGraphic( item );
			
			addGraphic( okButton );
			
			x = FP.halfWidth - (WIDTH >> 1);
			y = FP.halfHeight - (rowY >> 1);
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Detect if an item was clicked
			if ( Input.mousePressed )
			{
				// Detect if the OK button has been pressed
				if ( !okButton.buttonSelected && 
					Input.mouseX >= x + okButton.x && Input.mouseX <= x + okButton.x + BUTTON_WIDTH &&
					Input.mouseY >= y + okButton.y && Input.mouseY <= y + okButton.y + BUTTON_HEIGHT )
				{
					okButton.buttonSelected = true;
				}
				
				for ( var i:int = 0; i < fields.length; i++ )
				{
					if ( Input.mouseX >= x + fields[i].x && Input.mouseX <= x + fields[i].x + fields[i].width &&
						Input.mouseY >= y + fields[i].y && Input.mouseY <= y + fields[i].y + fields[i].height )
					{
						TendField( i );
					}
				}
			}
				
			if ( Input.mouseUp )
			{
				if ( okButton.buttonSelected )
				{
					okButton.buttonSelected = false;
					
					if ( okButton.callback != null &&
						Input.mouseX >= x + okButton.x && Input.mouseX <= x + okButton.x + BUTTON_WIDTH &&
						Input.mouseY >= y + okButton.y && Input.mouseY <= y + okButton.y + BUTTON_HEIGHT )
					{
						FP.world.remove( this );
						
						okButton.callback();
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Tend to crops planted in field
		 * 
		 * @param	fieldIndex
		 * Index of field to be tended
		 */
		public function TendField( fieldIndex:int ):void
		{
			// If the field was successfully tended, update list item
			if( GameWorld.sim.TendField( fieldIndex ) )
				fields[fieldIndex].text += " - " + ResourceManager.getInstance().getString( "resources", "FIELD_TENDED" );
		}
		
	}
}