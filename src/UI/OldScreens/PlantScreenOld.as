package UI.OldScreens
{
	import Types.*;
	import UI.Elements.*;
	import mx.resources.ResourceManager
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	/**
	 * Entity to display Plant dialog box on screen
	 * 
	 * @author Switchbreak
	 */
	public class PlantScreen extends Entity 
	{
		
		private static const WIDTH:int = 200;
		private static const SPACING:int = 10;
		private static const BACK_COLOR:uint = 0;
		private static const BUTTON_WIDTH:int = 60;
		private static const BUTTON_HEIGHT:int = 25;
		private static const FRONT_COLOR:uint = 0xFFFFFF;
		private static const SELECTED_COLOR:uint = 0x00FF00;
		
		private var selectedItem:int = -1;
		private var seedIndex:Vector.<int>;
		
		private var okButton:Button;
		private var items:Vector.<Text>;
		private var fields:Vector.<Text>;
		
		/**
		 * Create and display the Plant screen
		 */
		public function PlantScreen( callback:Function = null )
		{
			var rowY:int = SPACING;
			var fieldY:int = rowY;
			
			// Add field list graphics
			var itemHeader:Text = new Text( ResourceManager.getInstance().getString( "resources", "HEADER_SEEDS" ), SPACING, rowY );
			rowY += itemHeader.height + SPACING;
			
			items = new Vector.<Text>();
			seedIndex = new Vector.<int>();
			var index:int = 1;
			for ( var i:int = 0; i < Farm.SILO_COUNT * Farm.SILO_SIZE; i++ )
			{
				if ( GameWorld.sim.farm.silo[i] != null && GameWorld.sim.farm.silo[i].type == Product.TYPE_SEED && GameWorld.sim.farm.silo[i].storageTime < Product.STORAGE_TIME_ROTTEN )
				{
					var lineItem:Text = new Text( index + ". " + GameWorld.sim.crops[GameWorld.sim.farm.silo[i].plantType], SPACING, rowY );
					
					items.push( lineItem );
					seedIndex.push( i );
					index++;
					rowY += lineItem.height + SPACING;
				}
			}
			
			// Add field list graphics
			var fieldHeader:Text = new Text( ResourceManager.getInstance().getString( "resources", "HEADER_FIELDS" ), WIDTH + SPACING, fieldY );
			fieldY += fieldHeader.height + SPACING;
			
			fields = new Vector.<Text>();
			for ( i = 0; i < Farm.FIELD_COUNT; i++ )
			{
				// Initialize field graphic
				var fieldName:String = ResourceManager.getInstance().getString( "resources", "LIST_EMPTY" );;
				
				// Show field plant type and age
				if ( GameWorld.sim.farm.fields[i] != null )
				{
					fieldName = GameWorld.sim.crops[GameWorld.sim.farm.fields[i].plantType];
					
					switch( GameWorld.sim.farm.fields[i].age )
					{
						case Field.AGE_SEED:
							fieldName += " - " + ResourceManager.getInstance().getString( "resources", "AGE_SEED" );
							break;
						case Field.AGE_SPROUT:
							fieldName += " - " + ResourceManager.getInstance().getString( "resources", "AGE_SPROUT" );
							break;
						case Field.AGE_RIPE:
							fieldName += " - " + ResourceManager.getInstance().getString( "resources", "AGE_RIPE" );
							break;
						default:
							fieldName += " - " + ResourceManager.getInstance().getString( "resources", "AGE_WITHERED" );
					}
				}
				
				// Add field name graphic
				lineItem = new Text( (i + 1) + ". " + fieldName, WIDTH + SPACING, fieldY );
				fields.push( lineItem );
				
				fieldY += lineItem.height + SPACING;
			}
			
			// Move position marker to bottom of whichever list is longer
			if ( rowY < fieldY )
				rowY = fieldY;
			rowY += SPACING;
			
			// Add OK button
			okButton = new Button( ResourceManager.getInstance().getString( "resources", "OK_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, callback );
			okButton.x = (WIDTH << 1) - SPACING - BUTTON_WIDTH;
			okButton.y = rowY;
			
			rowY += BUTTON_HEIGHT + SPACING;
			
			// Add all graphics to the dialog box
			addGraphic( Image.createRect( (WIDTH << 1), rowY, BACK_COLOR ) );
			addGraphic( itemHeader );
			addGraphic( fieldHeader );
			for each( var item:Text in items )
				addGraphic( item );
			for each( item in fields )
				addGraphic( item );
			addGraphic( okButton );
			
			// Center the dialog box on screen
			x = FP.halfWidth - WIDTH;
			y = FP.halfHeight - (rowY >> 1);
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Detect if an item was clicked
			if ( Input.mousePressed )
			{
				// Detect if the OK button has been pressed
				if ( !okButton.buttonSelected && 
					Input.mouseX >= x + okButton.x && Input.mouseX <= x + okButton.x + BUTTON_WIDTH &&
					Input.mouseY >= y + okButton.y && Input.mouseY <= y + okButton.y + BUTTON_HEIGHT )
				{
					okButton.buttonSelected = true;
				}
				
				if ( selectedItem >= 0 )
				{
					items[selectedItem].color = FRONT_COLOR;
					
					for ( var i:int = 0; i < fields.length; i++ )
					{
						if( Input.mouseX >= x + fields[i].x && Input.mouseX <= x + fields[i].x + fields[i].width &&
							Input.mouseY >= y + fields[i].y && Input.mouseY <= y + fields[i].y + fields[i].height )
						{
							PlantField( selectedItem, i );
						}
					}
					
					selectedItem = -1;
				}
				
				for ( i = 0; i < items.length; i++ )
				{
					if( Input.mouseX >= x + items[i].x && Input.mouseX <= x + items[i].x + items[i].width &&
						Input.mouseY >= y + items[i].y && Input.mouseY <= y + items[i].y + items[i].height )
					{
						selectedItem = i;
						items[i].color = SELECTED_COLOR;
					}
				}
			}
				
			if ( Input.mouseUp )
			{
				if ( okButton.buttonSelected )
				{
					okButton.buttonSelected = false;
					
					if ( okButton.callback != null &&
						Input.mouseX >= x + okButton.x && Input.mouseX <= x + okButton.x + BUTTON_WIDTH &&
						Input.mouseY >= y + okButton.y && Input.mouseY <= y + okButton.y + BUTTON_HEIGHT )
					{
						FP.world.remove( this );
						
						okButton.callback();
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Plant crop from storage in field
		 * 
		 * @param	itemIndex
		 * Index of crop from storage to plant
		 * @param	fieldIndex
		 * Index of field to be planted
		 */
		public function PlantField( itemIndex:int, fieldIndex:int ):void
		{
			// Plant the field
			if ( GameWorld.sim.PlantField( seedIndex[itemIndex], fieldIndex ) )
			{
				// Update the field list item
				fields[fieldIndex].text = GameWorld.sim.crops[GameWorld.sim.farm.fields[fieldIndex].plantType] + " - " + ResourceManager.getInstance().getString( "resources", "AGE_SEED" );
				
				// Remove the seed from the list of seeds to plant
				(graphic as Graphiclist).remove( items[itemIndex] );
				
				var shift:int = items[itemIndex].height + SPACING;
				for ( var i:int = itemIndex + 1; i < items.length; i++ )
				{
					items[i].text = i + ". " + GameWorld.sim.crops[GameWorld.sim.farm.silo[seedIndex[i]].plantType];
					items[i].y -= shift;
				}
				
				items.splice( itemIndex, 1 );
				seedIndex.splice( itemIndex, 1 );
			}
		}
		
	}
}