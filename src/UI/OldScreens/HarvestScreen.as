package UI.OldScreens
{
	import Types.*;
	import UI.Elements.*;
	import mx.resources.ResourceManager
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	/**
	 * Entity to display Plant dialog box on screen
	 * 
	 * @author Switchbreak
	 */
	public class HarvestScreen extends Entity 
	{
		
		private static const WIDTH:int = 200;
		private static const SPACING:int = 10;
		private static const BACK_COLOR:uint = 0;
		private static const BUTTON_WIDTH:int = 60;
		private static const BUTTON_HEIGHT:int = 25;
		
		private var okButton:Button;
		private var fields:Vector.<Text>;
		
		/**
		 * Create and display the Harvest screen
		 */
		public function HarvestScreen( callback:Function = null )
		{
			var rowY:int = SPACING;
			
			// Add header text graphic
			var fieldHeader:Text = new Text( ResourceManager.getInstance().getString( "resources", "HEADER_FIELDS" ), SPACING, rowY );
			rowY += fieldHeader.height + SPACING;
			
			// Show field list graphics
			fields = new Vector.<Text>();
			for ( var i:int = 0; i < Farm.FIELD_COUNT; i++ )
			{
				// Initialize field name graphic
				var fieldName:String = ResourceManager.getInstance().getString( "resources", "LIST_EMPTY" );
				
				// Show field plant type and age
				if ( GameWorld.sim.farm.fields[i] != null )
				{
					fieldName = GameWorld.sim.crops[GameWorld.sim.farm.fields[i].plantType];
					
					switch( GameWorld.sim.farm.fields[i].age )
					{
						case Field.AGE_SEED:
							fieldName += " - " + ResourceManager.getInstance().getString( "resources", "AGE_SEED" );
							break;
						case Field.AGE_SPROUT:
							fieldName += " - " + ResourceManager.getInstance().getString( "resources", "AGE_SPROUT" );
							break;
						case Field.AGE_RIPE:
							fieldName += " - " + ResourceManager.getInstance().getString( "resources", "AGE_RIPE" );
							break;
						default:
							fieldName += " - " + ResourceManager.getInstance().getString( "resources", "AGE_WITHERED" );
					}
				}
				
				// Add field name graphic
				var lineItem:Text = new Text( (i + 1) + ". " + fieldName, SPACING, rowY );
				fields.push( lineItem );
				
				rowY += lineItem.height + SPACING;
			}
			
			rowY += SPACING;
			
			// Add OK button
			okButton = new Button( ResourceManager.getInstance().getString( "resources", "OK_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, callback );
			okButton.x = WIDTH - SPACING - BUTTON_WIDTH;
			okButton.y = rowY;
			
			rowY += BUTTON_HEIGHT + SPACING;
			
			// Add all graphics to the dialog box
			addGraphic( Image.createRect( WIDTH, rowY, BACK_COLOR ) );
			addGraphic( fieldHeader );
			for each( var item:Text in fields )
				addGraphic( item );
			addGraphic( okButton );
			
			// Center dialog box on screen
			x = FP.halfWidth - (WIDTH >> 1);
			y = FP.halfHeight - (rowY >> 1);
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Detect if an item was clicked
			if ( Input.mousePressed )
			{
				// Detect if the OK button has been pressed
				if ( !okButton.buttonSelected && 
					Input.mouseX >= x + okButton.x && Input.mouseX <= x + okButton.x + BUTTON_WIDTH &&
					Input.mouseY >= y + okButton.y && Input.mouseY <= y + okButton.y + BUTTON_HEIGHT )
				{
					okButton.buttonSelected = true;
				}
				
				if ( GameWorld.sim.farm.storageSpace > 0 )
				{
					for ( var i:int = 0; i < fields.length; i++ )
					{
						if ( GameWorld.sim.farm.fields[i] != null &&
							GameWorld.sim.farm.fields[i].age == Field.AGE_RIPE &&
							Input.mouseX >= x + fields[i].x && Input.mouseX <= x + fields[i].x + fields[i].width &&
							Input.mouseY >= y + fields[i].y && Input.mouseY <= y + fields[i].y + fields[i].height )
						{
							PickField( i );
						}
					}
				}
			}
				
			if ( Input.mouseUp )
			{
				if ( okButton.buttonSelected )
				{
					okButton.buttonSelected = false;
					
					if ( okButton.callback != null &&
						Input.mouseX >= x + okButton.x && Input.mouseX <= x + okButton.x + BUTTON_WIDTH &&
						Input.mouseY >= y + okButton.y && Input.mouseY <= y + okButton.y + BUTTON_HEIGHT )
					{
						FP.world.remove( this );
						
						okButton.callback();
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Harvest crops planted in field
		 * 
		 * @param	fieldIndex
		 * Index of field to be harvested
		 */
		public function PickField( fieldIndex:int ):void
		{
			// Harvest the field, if successful then blank out the list item
			if( GameWorld.sim.HarvestField( fieldIndex ) )
				fields[fieldIndex].text = (fieldIndex + 1) + ". " + ResourceManager.getInstance().getString( "resources", "LIST_EMPTY" );
		}
		
	}
}