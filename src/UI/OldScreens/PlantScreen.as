package UI 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Draw;
	import net.flashpunk.utils.Input;
	import mx.resources.ResourceManager;
	import Types.Product;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class PlantScreen extends Entity 
	{
		// Constants
		private static const RADIUS:Number = 75;
		private static const HEADER_COLOR:uint = 0xFF0000;
		private static const ITEM_COLOR:uint = 0;
		
		// Private variables
		private var maxCircleSize:int;
		private var fieldIndex:int;
		private var seedIndices:Vector.<int>;
		private var eventPlant:Function;
		private var eventCancel:Function;
		
		// Graphics
		private var seedButtons:Vector.<Text>;
		
		/**
		 * Initializes the planting radial menu
		 * 
		 * @param	setFieldIndex
		 * Index of the field to plant
		 * @param	setX
		 * Centerpoint of the radial menu
		 * @param	setY
		 * Centerpoint of the radial menu
		 * @param	setEventPlant
		 * Event to call when a seed is chosen
		 * @param	setEventCancel
		 * Event to call when the dialog box is canceled
		 */
		public function PlantScreen( setFieldIndex:int, setX:int, setY:int, setEventPlant:Function, setEventCancel:Function ) 
		{
			// Set object values
			fieldIndex = setFieldIndex;
			x = setX;
			y = setY;
			eventPlant = setEventPlant;
			eventCancel = setEventCancel;
			maxCircleSize = 0;
			
			// Initialize seed lists
			seedIndices = new Vector.<int>();
			seedButtons = new Vector.<Text>();
		}
		
		/**
		 * When the menu is added to the world, check that it has items to click
		 * on, otherwise immediately cancel it
		 */
		override public function added():void 
		{
			super.added();
			
			var seedTypes:Vector.<int> = new Vector.<int>();
			
			// Search through silo to find all valid seeds
			for ( var i:int = 0; i < GameWorld.sim.farm.silo.length; i++ )
			{
				// Check if the current silo slot contains seeds that have not
				// rotted yet
				if ( GameWorld.sim.farm.silo[i] != null
					&& GameWorld.sim.farm.silo[i].type == Product.TYPE_SEED
					&& GameWorld.sim.farm.silo[i].storageTime < Product.STORAGE_TIME_ROTTEN )
				{
					// Only add the first seed of each type found
					if ( seedTypes.indexOf( GameWorld.sim.farm.silo[i].plantType ) == -1 )
					{
						// Add seeds to list
						seedIndices.push( i );
						seedTypes.push( GameWorld.sim.farm.silo[i].plantType );
						
						// Create seed display object
						var seedButton:Text = new Text( GameWorld.sim.crops[GameWorld.sim.farm.silo[i].plantType].cropName.replace( " ", "\n" ) );
						seedButton.align = "center";
						seedButton.centerOrigin();
						seedButton.color = ITEM_COLOR;
						
						// Set circle radius to half the width of the widest
						// text box
						var circleSize:int = (seedButton.width >> 1) * (seedButton.width >> 1) + (seedButton.height >> 1) * (seedButton.height >> 1)
						if ( circleSize > maxCircleSize )
							maxCircleSize = circleSize;
						
						// Add text box to list
						seedButtons.push( seedButton );
					}
				}
			}
			
			// If no seeds are available, cancel the plant menu
			if ( seedIndices.length == 0 )
			{
				Cancel();
			}
			else
			{
				// Place seed buttons radially around the centerpoint
				var buttonAngle:Number = 0;
				var step:Number = 360 / seedButtons.length;
				maxCircleSize = Math.sqrt( maxCircleSize );
				for each( seedButton in seedButtons )
				{
					FP.angleXY( seedButton, buttonAngle, RADIUS );
					
					var block:Image = Image.createCircle( maxCircleSize );
					block.centerOrigin();
					block.x = seedButton.x;
					block.y = seedButton.y;
					
					addGraphic( block );
					addGraphic( seedButton );
					
					buttonAngle += step;
				}
				
				// Show menu title in center
				var title:Text = new Text( ResourceManager.getInstance().getString( "resources", "HEADER_PLANT" ) );
				title.centerOrigin();
				title.color = HEADER_COLOR;
				
				block = Image.createCircle( title.width >> 1 );
				block.centerOrigin();
				
				addGraphic( block );
				addGraphic( title );
			}
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Catch mouse clicks
			if ( Input.mousePressed )
			{
				// Check each button on the radial menu to see if it was clicked
				var buttonClicked:Boolean = false;
				for ( var i:int = 0; i < seedButtons.length; i++ )
				{
					// Get the distance from the mouse click to the center
					var distanceSq:Number = (Input.mouseX - x - seedButtons[i].x) * (Input.mouseX - x - seedButtons[i].x) + (Input.mouseY - y - seedButtons[i].y) * (Input.mouseY - y - seedButtons[i].y);
					if ( distanceSq <= maxCircleSize * maxCircleSize )
					{
						Input.mousePressed = false;
						
						PlantSeed( i );
						buttonClicked = true;
					}
				}
				
				// If user clicked off the radial menu, cancel out of it
				if ( !buttonClicked )
				{
					// Clear input.mousePressed to avoid it cascading to other
					// objects - cancel click should just cancel the menu
					Input.mousePressed = false;
					
					Cancel();
				}
			}
			
			super.update();
		}
		
		/**
		 * Draw lines between center and menu items
		 */
		override public function render():void 
		{
			Draw.resetTarget();
			for each( var seedButton:Text in seedButtons )
			{
				Draw.line( x, y, x + seedButton.x, y + seedButton.y );
			}
			
			super.render();
		}
		
		/**
		 * Removes the menu from the world and calls the cancel event
		 */
		private function Cancel():void
		{
			if ( eventCancel != null )
				eventCancel();
			
			FP.world.remove( this );
		}
		
		/**
		 * Plants the selected seeds in the field
		 * 
		 * @param	seedIndex
		 * Index of the seeds selected
		 */
		private function PlantSeed( seedIndex:int ):void
		{
			if( eventPlant != null )
				eventPlant( fieldIndex, seedIndices[seedIndex] );
			
			FP.world.remove( this );
		}
	}

}