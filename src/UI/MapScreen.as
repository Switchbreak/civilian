package UI
{
	import flash.display.BitmapData;
	import flash.display.InteractiveObject;
	import flash.display.Stage;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import net.flashpunk.Entity;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.Tween;
	import net.flashpunk.Tweener;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.tweens.misc.MultiVarTween;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.utils.Draw;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.utils.Input;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Image;
	import Types.*;
	import UI.Elements.*;
	import Minigames.Blockade.Blockade;
	
	/**
	 * Entity to display a map dialog box on screen
	 * 
	 * @author Switchbreak
	 */
	public class MapScreen extends Entity 
	{
		
		// Embeds
		[Embed (source = '../../img/Arrow.png')]			private static const ARROW_SPRITE:Class;
		[Embed (source = '../../img/MapBackground640.png')]	private static const MAP_BACKGROUND:Class;
		[Embed (source = '../../img/IconLocation.png')]		private static const ICON_LOCATION:Class;
		[Embed (source = '../../img/IconFarm.png')]			private static const ICON_FARM:Class;
		[Embed (source = '../../img/IconTax.png')]			private static const ICON_TAX:Class;
		[Embed (source = '../../img/Farmer.png')]			private static const FARMER:Class;
		[Embed (source = '../../img/RedBarn.png')]			private static const FARM:Class;
		[Embed (source = '../../font/Fondamento-Italic.ttf', embedAsCFF = 'false', fontFamily = 'Map Font')] private static const MAP_FONT:Class;
		
		// Private constants
		private static const SPACING:int						= 10;
		private static const BUTTON_WIDTH:int					= 60;
		private static const BUTTON_HEIGHT:int					= 50;
		private static const COUNTRY_COLOR:uint					= 0;
		private static const SELECTED_COLOR:uint				= 0x003300;
		private static const HIGHLIGHT_COLOR:uint				= 0x333300;
		private static const LIST_COLOR:uint					= 0xFFFFFF;
		private static const LIST_OCCUPIED_COLOR:uint			= 0xFF0000;
		private static const TICK_SPACING:int					= 5;
		private static const TICK_WIDTH:int						= 2;
		private static const TICK_HEIGHT:int					= 5;
		private static const BAR_SIZE:int						= 75;
		private static const BAR_BGCOLOR:uint					= 0x222222;
		private static const LIST_SPACING:int					= 4;
		private static const LIST_TEXT_OFFSET:int				= 2;
		private static const TOWN_LIST_X:int					= 80;
		private static const ICON_X:int							= FP.halfWidth + 110;
		private static const ICON_SPACING:int					= 7;
		private static const ICON_FONT_SIZE:int					= 12;
		private static const ICON_DISABLED_ALPHA:Number			= 0.2;
		private static const SMALL_FLAG_SCALE:Number			= 0.33;
		private static const MAP_MARKER_SPACING:int				= 25;
		private static const FARMER_ICON_Y:int					= 25;
		private static const SELECT_BORDER_WIDTH:int			= 2;
		private static const SELECT_BORDER_COLOR:uint			= 0xFFFFFF;
		private static const SELECT_MOVE_DURATION:Number		= 0.1;
		private static const FARMER_Y_OFFSET:Number				= 16;
		private static const BATTLE_CINEMATIC_DURATION:Number	= 1.0;
		private static const CROP_BAR_WIDTH:int					= 100;
		private static const CROP_BAR_HEIGHT:int				= 10;
		private static const CROP_BAR_SPACING:int				= 3;
		private static const CROP_BAR_COLOR:uint				= 0x888888;
		private static const TOOLBAR_FLAG_X:int					= 310;
		private static const PRICE_ANIMATE_DURATION:Number		= 0.5;
		private static const FARMER_WIDTH:int					= 28;
		private static const FARMER_HEIGHT:int					= 64;
		
		// Variables
		private var farmButton:ActionButton;
		private var nextButton:ActionButton;
		private var map:Image;
		private var townNames:Vector.<Text>;
		private var townFlags:Vector.<Image>;
		private var cropBars:Vector.<Image>;
		private var cropNames:Vector.<Text>;
		private var oldPrices:Vector.<int>;
		private var displayPrices:Vector.<int>;
		private var countryName:Text;
		private var countryFlag:Stamp;
		private var iconLocation:Image;
		private var textLocation:Text;
		private var iconFarm:Image;
		private var textFarm:Text;
		private var iconTax:Image;
		private var textTax:Text;
		private var selectBorder:Image;
		private var selectFlag:Image;
		private var farmer:Image;
		private var mover:LinearMotion;
		private var priceAnimate:NumTween;
		private var selectedCountry:Country;
		private var mapEnabled:Boolean;
		private var farmEvent:Function;
		private var nextEvent:Function;
		
		/**
		 * Create and display the Map screen
		 */
		public function MapScreen( battleCinematic:Boolean, farmCallback:Function, nextCallback:Function ) 
		{
			selectedCountry	= GameWorld.sim.country;
			farmEvent		= farmCallback;
			nextEvent		= nextCallback;
			selectFlag		= null;
			mapEnabled		= true;
			layer			= GameWorld.HUD_LAYER;
			graphic			= new Graphiclist();
			graphic.scrollX	= 0;
			graphic.scrollY	= 0;
			
			mover = new LinearMotion( MoverSet, Tween.PERSIST );
			addTween( mover );
			
			priceAnimate = new NumTween( PriceSet, Tween.PERSIST );
			addTween( priceAnimate );
			
			// Add the map and paper texture background
			addGraphic( new Stamp( MAP_BACKGROUND ) );
			addGraphic( GetMapBitmap() );
			
			InitGameIcons();
			
			InitToolbar();
			UpdateToolbar();
			
			// Show the non-interactive battle cinematic if the flag is set
			if ( battleCinematic )
			{
				mapEnabled = false;
				farmButton.visible = false;
				nextButton.visible = false;
				addTween( new Alarm( BATTLE_CINEMATIC_DURATION, EventCloseMap, ONESHOT ), true);
			}
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Handle mouse clicks
			if ( Input.mousePressed )
			{
				// Detect if a country was clicked
				if ( mapEnabled
					&& Input.mouseX >= x && Input.mouseX <= x + map.width
					&& Input.mouseY >= y && Input.mouseY <= y + map.height )
				{
					// Check mouse click against country mask to determine what
					// country was clicked on
					var countryIndex:int = GameWorld.sim.map.mapMask.getPixel( Input.mouseX - x, Input.mouseY - y );
					if ( countryIndex > 0 )
					{
						selectedCountry = GameWorld.sim.countries[countryIndex - 1];
						UpdateToolbar();
					}
				}
			}
			
			if ( mover.active )
				MoverSet();
			if ( priceAnimate.active )
				PriceSet();
			
			super.update();
		}
		
		/**
		 * Update the toolbar with information on the currently selected country
		 */
		private function UpdateToolbar():void
		{
			UpdateCropPriceBars();
			
			UpdateTownList();
			
			// Show selected country on the map
			if ( selectFlag != null )
				(graphic as Graphiclist).remove( selectFlag );
			selectFlag = new Image( selectedCountry.occupiedBy.flag );
			selectFlag.scale = SMALL_FLAG_SCALE;
			selectFlag.smooth = true;
			addGraphic( selectFlag );
			selectFlag.x = selectedCountry.x - (selectFlag.scaledWidth >> 1);
			selectFlag.y = selectedCountry.y - selectFlag.scaledHeight;
			
			mover.setMotion( selectBorder.x, selectBorder.y, selectFlag.x - SELECT_BORDER_WIDTH, selectFlag.y - SELECT_BORDER_WIDTH, SELECT_MOVE_DURATION );
			
			// Set country name and flag to currently selected country
			countryName.text = selectedCountry.name;
			countryFlag.source = FP.getBitmap( selectedCountry.occupiedBy.flag );
			
			UpdateFarmButton();
			
			// Set or hide farm icon
			if ( selectedCountry.farm != null )
			{
				iconFarm.alpha = 1.0;
				textFarm.text = ResourceManager.getInstance().getString( "resources", "ICON_FARM" );
			}
			else
			{
				iconFarm.alpha = ICON_DISABLED_ALPHA;
				textFarm.text = "";
			}
			
			textTax.text = StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "ICON_TAX" ), selectedCountry.tax.toFixed( 2 ) );
		}
		
		/**
		 * Update the list of town names and flags on the toolbar
		 */
		private function UpdateTownList():void 
		{
			// Remove current town list from toolbar
			for each( var townName:Text in townNames )
			{
				(graphic as Graphiclist).remove( townName );
			}
			townNames.length = 0;
			
			// Remove current town flags from toolbar
			for each( var townFlag:Image in townFlags )
			{
				(graphic as Graphiclist).remove( townFlag );
			}
			townFlags.length = 0;
			
			// Create new list of towns
			var listY:int = FP.height - BAR_SIZE + ICON_SPACING;
			for each( var town:Town in selectedCountry.towns )
			{
				// Create the graphics for the flag and the town name
				var townColor:uint;
				if ( town.occupiedBy != null )
				{
					townColor = LIST_OCCUPIED_COLOR;
					townFlag = new Image( town.occupiedBy.invader.flag );
				}
				else
				{
					townColor = LIST_COLOR;
					townFlag = new Image( town.country.occupiedBy.flag );
				}
				townFlag.smooth = true;
				townFlag.x = TOWN_LIST_X + CROP_BAR_WIDTH;
				townFlag.y = listY;
				townFlag.scale = SMALL_FLAG_SCALE;
				townName = new Text( town.name, townFlag.x + townFlag.scaledWidth + ICON_SPACING, listY - LIST_TEXT_OFFSET );
				townName.color = townColor;
				townName.size = 14;
				listY += townFlag.scaledHeight + ICON_SPACING;
				
				// Add graphics to the toolbar
				addGraphic( townFlag );
				addGraphic( townName );
				
				// Add graphics to town graphic lists
				townFlags.push( townFlag );
				townNames.push( townName );
			}
		}
		
		/**
		 * Update the prices for crops displayed
		 */
		private function UpdateCropPriceBars():void 
		{
			for (var i:int = 0; i < cropBars.length; i++) 
			{
				var displayPrice:int = selectedCountry.cropPrices[i] * CROP_BAR_WIDTH + (CROP_BAR_WIDTH >> 1);
				if ( displayPrice < 1 )
					displayPrice = 1;
				if ( displayPrice > CROP_BAR_WIDTH )
					displayPrice = CROP_BAR_WIDTH;
				
				oldPrices[i] = displayPrices[i];
				displayPrices[i] = displayPrice;
			}
			priceAnimate.tween( 0, 1, PRICE_ANIMATE_DURATION, Ease.bounceOut );
		}
		
		/**
		 * Show farm/travel button as appropriate to the selected country
		 */
		private function UpdateFarmButton():void 
		{
			// If the selected country is not the one the farmer is currently
			// located on, give the option to travel there
			if ( selectedCountry == GameWorld.sim.country )
			{
				iconLocation.alpha = 1.0;
				textLocation.text = ResourceManager.getInstance().getString( "resources", "ICON_LOCATION" );
				farmButton.text = "Farm";
				farmButton.visible = true;
			}
			else
			{
				iconLocation.alpha = ICON_DISABLED_ALPHA;
				textLocation.text = "";
				farmButton.visible = false;
				for each( var country:Country in GameWorld.sim.country.adjacent )
				{
					if ( selectedCountry == country )
					{
						farmButton.text = "Travel";
						farmButton.visible = true;
						break;
					}
				}
			}
		}
		
		/**
		 * Travel to the selected destination country
		 */
		private function MoveCountry():void
		{
			// If either the current country or the destination
			// country are at war, warn the player of the dangers of
			// travel
			if ( GameWorld.sim.country.wars.length > 0 && GameWorld.sim.country.occupiedBy != selectedCountry.occupiedBy )
			{
				var moveDialog:DialogBox = new DialogBox( ResourceManager.getInstance().getString( "resources", "DIALOG_BORDER_CLOSED" ) );
				moveDialog.AddButton( ResourceManager.getInstance().getString( "resources", "OK_BUTTON" ), Travel );
				moveDialog.AddButton( ResourceManager.getInstance().getString( "resources", "CANCEL_BUTTON" ), CancelDialog );
				FP.world.add( moveDialog );
				mapEnabled = false;
			}
			else if ( selectedCountry.wars.length > 0 && GameWorld.sim.country.occupiedBy != selectedCountry.occupiedBy )
			{
				moveDialog = new DialogBox( ResourceManager.getInstance().getString( "resources", "DIALOG_DEST_BORDER_CLOSED" ) );
				moveDialog.AddButton( ResourceManager.getInstance().getString( "resources", "OK_BUTTON" ), Travel );
				moveDialog.AddButton( ResourceManager.getInstance().getString( "resources", "CANCEL_BUTTON" ), CancelDialog );
				FP.world.add( moveDialog );
				mapEnabled = false;
			}
			else
			{
				Travel();
			}
		}
		
		/**
		 * Finish the travel if a dialogue button was popped up
		 */
		private function Travel():void 
		{
			// Reenable map dialog if it was disabled for the confirmation box
			mapEnabled = true;
			
			// Move the country
			GameWorld.sim.MoveCountry( selectedCountry, EventBlockade );
			
			// Set farmer marker position
			farmer.x = GameWorld.sim.country.x + MAP_MARKER_SPACING;
			farmer.y = GameWorld.sim.country.y - FARMER_Y_OFFSET;
			
			farmButton.visible = true;
			farmButton.text = "Farm";
		}
		
		/**
		 * Farm/Travel button was clicked
		 */
		private function EventFarmButton():void
		{
			if( selectedCountry == GameWorld.sim.country )
				EventCloseMap();
			else
				MoveCountry();
		}
		
		/**
		 * Farm/Travel button was clicked
		 */
		private function EventNextButton():void
		{
			FP.world.remove( this );
			nextEvent();
		}
		
		/**
		 * Close the map and return to the farm
		 */
		public function EventCloseMap():void
		{
			FP.world.remove( this );
			farmEvent();
		}
		
		/**
		 * This function is called to inform the player that they have hit a
		 * blockade
		 */
		public function EventBlockade( message:String ):void
		{
			var blockadeDialog:DialogBox = new DialogBox( message );
			blockadeDialog.AddButton( ResourceManager.getInstance().getString( "resources", "RUN_BUTTON" ), EventBlockadeRun );
			if( GameWorld.sim.farmer.money >= Sim.BRIBE_PRICE )
				blockadeDialog.AddButton( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "BRIBE_BUTTON" ), Sim.BRIBE_PRICE ), EventBribe );
			FP.world.add( blockadeDialog );
			
			mapEnabled = false;
		}
		
		/**
		 * This function is called to invoke the blockade run minigame
		 */
		public function EventBlockadeRun():void
		{
			FP.world = new Blockade( GameWorld.sim, CancelDialog );
		}
		
		/**
		 * This function is called to invoke the blockade run minigame
		 */
		public function EventBribe():void
		{
			GameWorld.sim.Bribe();
			CancelDialog();
		}
		
		/**
		 * Callback for dialog box cancel buttons, reenables the map screen
		 * when cancel is pressed and the dialog is removed
		 */
		private function CancelDialog():void
		{
			mapEnabled = true;
		}
		
		/**
		 * Move farm selection border to mover tween location
		 */
		private function MoverSet():void
		{
			selectBorder.x = mover.x;
			selectBorder.y = mover.y;
		}
		
		/**
		 * Move price bars to price tween location
		 */
		private function PriceSet():void
		{
			for (var i:int = 0; i < cropBars.length; i++) 
			{
				cropBars[i].scaleX = oldPrices[i] + (displayPrices[i] - oldPrices[i]) * priceAnimate.value;
			}
		}
		
		/**
		 * Show arrows to indicate wars between countries
		 */
		private function InitWarArrows():void 
		{
			for each( var war:War in GameWorld.sim.activeWars )
			{
				// Create arrow to indicate countries at war
				var arrow:Image = new Image( ARROW_SPRITE );
				
				// Arrow should be centered between the countries, pointing at the country being invaded
				arrow.centerOrigin();
				arrow.x = war.invader.borderCenter[war.invader.adjacent.indexOf( war.invaded )].x;
				arrow.y = war.invader.borderCenter[war.invader.adjacent.indexOf( war.invaded )].y;
				arrow.smooth = true;
				arrow.angle = FP.angle( war.invader.x, war.invader.y, war.invaded.x, war.invaded.y );
				
				Draw.graphic( arrow );
				
				// Show war status, one tick for every captured town
				var tickX:int = arrow.x - ((war.invaded.towns.length * TICK_SPACING) >> 1);
				for ( var i:int = 0; i < war.townsTaken; i++ )
				{
					var tick:Image = Image.createRect( TICK_WIDTH, TICK_HEIGHT, COUNTRY_COLOR );
					tick.x = tickX;
					tick.y = arrow.y + SPACING;
					Draw.graphic( tick );
					
					tickX += TICK_SPACING;
				}
			}
		}
		
		/**
		 * Show country names and markers on the map
		 */
		private function InitCountryLabels( mapBitmap:BitmapData ):void 
		{
			var countryIndex:int = 1;
			for each( var country:Country in GameWorld.sim.countries )
			{
				// Set the country name color: highlight the selected country in
				// one color, highlight countries at war in another
				var countryColor:uint = COUNTRY_COLOR;
				if ( country == GameWorld.sim.country )
					countryColor = SELECTED_COLOR
				else if ( country.wars.length > 0 )
					countryColor = HIGHLIGHT_COLOR;
				
				// Highlight warring countries in red
				if ( country.wars.length > 0 )
				{
					HighlightCountry( countryIndex, mapBitmap );
				}
				
				// Show occupying country flag at the centroid position
				var flag:Image = new Image( country.occupiedBy.flag );
				flag.smooth = true;
				flag.scale = SMALL_FLAG_SCALE;
				flag.x = country.x - (flag.scaledWidth >> 1);
				flag.y = country.y - flag.scaledHeight;
				Draw.graphic( flag );
				
				// Show country name
				var countryText:Text = new Text( country.name );
				countryText.font = "Map Font";
				countryText.color = countryColor;
				countryText.x = country.x - (countryText.width >> 1);
				countryText.y = country.y;
				Draw.graphic( countryText );
				
				// Show farm marker if country has a farm
				if ( country.farm != null )
				{
					var farm:Stamp = new Stamp( FARM );
					farm.x = country.x - MAP_MARKER_SPACING - farm.width;
					farm.y = country.y - FARMER_Y_OFFSET;
					addGraphic( farm );
				}
				
				// Show country status, one tick for every non-occupied town
				if ( country.defending )
				{
					var tickX:int = country.x - ((country.towns.length * TICK_SPACING) >> 1);
					for each( var town:Town in country.towns )
					{
						if ( town.occupiedBy == null )
						{
							var tick:Image = Image.createRect( TICK_WIDTH, TICK_HEIGHT, countryColor );
							tick.x = tickX;
							tick.y = countryText.y + countryText.height;
							Draw.graphic( tick );
							
							tickX += TICK_SPACING;
						}
					}
				}
				
				countryIndex++;
			}
			
			// Add border around selected flag
			selectBorder = Image.createRect( flag.scaledWidth + (SELECT_BORDER_WIDTH << 1) + 1, flag.scaledHeight + (SELECT_BORDER_WIDTH << 1) + 1, SELECT_BORDER_COLOR );
			selectBorder.x = selectedCountry.x - (flag.scaledWidth >> 1) - SELECT_BORDER_WIDTH;
			selectBorder.y = selectedCountry.y - flag.scaledHeight - SELECT_BORDER_WIDTH;
			addGraphic( selectBorder );
		}
		
		/**
		 * Highlight a selected country in red on the map
		 * @param	countryIndex	Country to highlight
		 * @param	mapBitmap		Map bitmap
		 */
		private function HighlightCountry( countryIndex:int, mapBitmap:BitmapData ):void 
		{
			var highlight:BitmapData = new BitmapData( mapBitmap.width, mapBitmap.height, true, 0x00000000 );
			highlight.threshold( GameWorld.sim.map.mapMask, GameWorld.sim.map.mapMask.rect, new Point( 0, 0 ), "==", 0xFF000000 + countryIndex, 0x55FF0000 );
			mapBitmap.copyPixels( highlight, highlight.rect, new Point( 0, 0 ), null, null, true );
		}
		
		/**
		 * Build the map bitmap with country labels and arrows baked in
		 * @return	Map image
		 */
		private function GetMapBitmap():Image 
		{
			var mapBitmap:BitmapData = GameWorld.sim.map.mapBitmap.clone();
			Draw.setTarget( mapBitmap );
			
			InitWarArrows();
			InitCountryLabels( mapBitmap );
			
			map = new Image( mapBitmap );
			map.alpha = 0.5;
			
			return map;
		}
		
		/**
		 * Add game icons to the map
		 */
		private function InitGameIcons():void 
		{
			// Add farmer to map
			farmer = new Image( FARMER, new Rectangle( 0, 0, FARMER_WIDTH, FARMER_HEIGHT ) );
			farmer.x = GameWorld.sim.country.x + MAP_MARKER_SPACING;
			farmer.y = GameWorld.sim.country.y - FARMER_ICON_Y;
			addGraphic( farmer );
		}
		
		/**
		 * Add the toolbar to the map screen
		 */
		private function InitToolbar():void 
		{
			// Add the toolbar background
			var toolbar:Image = Image.createRect( FP.width, BAR_SIZE, BAR_BGCOLOR );
			toolbar.y = FP.height - toolbar.height;
			addGraphic( toolbar );
			
			// Add Farm/Travel button
			farmButton = new ActionButton( ResourceManager.getInstance().getString( "resources", "FARM_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, EventFarmButton );
			farmButton.x = SPACING;
			farmButton.y = FP.height - (BAR_SIZE >> 1) - (BUTTON_HEIGHT >> 1);
			addGraphic( farmButton );
			
			// Add Next button
			nextButton = new ActionButton( ResourceManager.getInstance().getString( "resources", "NEXT_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, EventNextButton );
			nextButton.x = FP.width - SPACING - BUTTON_WIDTH;
			nextButton.y = FP.height - (BAR_SIZE >> 1) - (BUTTON_HEIGHT >> 1);
			addGraphic( nextButton );
			
			// Add selected country name and flag
			countryName = new Text( "", TOOLBAR_FLAG_X, FP.height - BAR_SIZE );
			countryName.width = 96;
			countryName.align = "center";
			addGraphic( countryName );
			
			countryFlag = new Stamp( selectedCountry.occupiedBy.flag );
			countryFlag.x = TOOLBAR_FLAG_X;
			countryFlag.y = countryName.y + countryName.height;
			addGraphic( countryFlag );
			
			// Add icons
			iconLocation = new Image( ICON_LOCATION );
			iconLocation.x = ICON_X;
			iconLocation.y = FP.height - BAR_SIZE + ICON_SPACING;
			textLocation = new Text( "", ICON_X + iconLocation.width + ICON_SPACING, iconLocation.y - LIST_TEXT_OFFSET );
			textLocation.size = ICON_FONT_SIZE;
			addGraphic( iconLocation );
			addGraphic( textLocation );
			
			iconFarm = new Image( ICON_FARM );
			iconFarm.x = ICON_X;
			iconFarm.y = iconLocation.y + iconLocation.height + ICON_SPACING;
			textFarm = new Text( "", ICON_X + iconFarm.width + ICON_SPACING, iconFarm.y - LIST_TEXT_OFFSET );
			textFarm.size = ICON_FONT_SIZE;
			addGraphic( iconFarm );
			addGraphic( textFarm );
			
			iconTax = new Image( ICON_TAX );
			iconTax.x = ICON_X;
			iconTax.y = iconFarm.y + iconFarm.height + ICON_SPACING
			textTax = new Text( "", ICON_X + iconTax.width + ICON_SPACING, iconTax.y - LIST_TEXT_OFFSET );
			textTax.size = ICON_FONT_SIZE;
			addGraphic( iconTax );
			addGraphic( textTax );
			
			// Initialize toolbar graphics
			townNames		= new Vector.<Text>();
			townFlags		= new Vector.<Image>();
			cropBars		= new Vector.<Image>();
			cropNames		= new Vector.<Text>();
			
			var listY:int = FP.height - BAR_SIZE + ICON_SPACING - 3;
			for each( var crop:Crop in GameWorld.sim.crops )
			{
				var cropBar:Image = Image.createRect( 1, CROP_BAR_HEIGHT, CROP_BAR_COLOR );
				cropBar.x = TOWN_LIST_X;
				cropBar.y = listY + CROP_BAR_SPACING;
				addGraphic( cropBar );
				cropBars.push( cropBar );
				
				var cropName:Text = new Text( crop.cropName, TOWN_LIST_X, listY );
				cropName.size = 8;
				addGraphic( cropName );
				cropNames.push( cropName );
				
				listY += CROP_BAR_HEIGHT + CROP_BAR_SPACING;
			}
			
			oldPrices		= new Vector.<int>( cropBars.length );
			displayPrices	= new Vector.<int>( cropBars.length );
		}
	}

}