package UI
{
	import UI.Elements.*;
	import mx.resources.ResourceManager;
	import net.flashpunk.FP;
	import net.flashpunk.Entity;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.graphics.Graphiclist;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class NameEntry extends Entity 
	{
		// Constants
		private static const BOX_MIN_WIDTH:int = 200;		// Width of entry box
		private static const BOX_BACK:uint = 0;				// Background color of the entry box
		private static const PADDING:int = 5;				// Size of UI element padding
		private static const SPACING:int = 10;				// Size of UI element vertical spacing
		private static const BLINK_RATE:Number = 0.25;		// Rate at which the cursor blinks, in blinks/sec
		private static const CURSOR_SPACING:int = -2;		// Spacing between text entry and cursor
		private static const ENTRY_LENGTH:int = 18;			// Maximum length allowed for name
		private static const BUTTON_WIDTH:int = 50;			// Size and color of OK button
		private static const BUTTON_HEIGHT:int = 25;
		
		// Private variables
		private var enteredNames:Vector.<String>;
		private var selectedIndex:int = 0;
		private var blinkTimer:Number = 0.0;
		private var buttonSelected:Boolean = false;
		private var callback:Function = null;
		private var boxWidth:int;
		private var boxHeight:int;
		
		// Graphics
		private var textEntries:Vector.<Text>;
		private var cursor:Image;
		private var button:Button;
		
		/**
		 * Create a text entry box for the player to enter a character name
		 * 
		 * @param	BoxTitle
		 * Title text to display above entry
		 * @param	setCallback
		 * Function to call when text entry is completed
		 */
		public function NameEntry( BoxTitle:String, setCallback:Function = null, names:Vector.<String> = null, nameCount:int = 1 )
		{
			layer = GameWorld.HUD_LAYER;
			graphic = new Graphiclist();
			graphic.scrollX = 0;
			graphic.scrollY = 0;
			
			// Check names argument
			if ( names == null || names.length != nameCount )
				names = new Vector.<String>( nameCount );
			
			boxHeight = 0;
			boxWidth = BOX_MIN_WIDTH;
			
			// Add box title
			var titleText:Text = new Text( BoxTitle );
			titleText.align = "center";
			if ( titleText.textWidth > boxWidth )
				boxWidth = titleText.textWidth + PADDING * 2;
			titleText.width = boxWidth;
			boxHeight += titleText.height;
			
			// Add text entry boxes
			textEntries = new Vector.<Text>();
			enteredNames = new Vector.<String>( nameCount );
			var cursorWidth:int;
			for ( var i:int = 0; i < nameCount; i++ )
			{
				textEntries.push( new Text( " ", PADDING, boxHeight + SPACING ) );
				boxHeight += SPACING + textEntries[i].height;
				cursorWidth = textEntries[i].textWidth;
				
				enteredNames[i] = names[i];
				
				if ( nameCount > 1 )
					textEntries[i].text = (i + 1) + ". " + names[i];
				else if( names[i] != null )
					textEntries[i].text = names[i];
			}
			
			// Add cursor
			cursor = Image.createRect( cursorWidth, textEntries[selectedIndex].textHeight );
			cursor.x = textEntries[selectedIndex].x;
			cursor.y = textEntries[selectedIndex].y;
			
			// Add OK button
			button = new Button( ResourceManager.getInstance().getString( "resources", "OK_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, FinishEntry );
			button.x = (boxWidth >> 1) - (BUTTON_WIDTH >> 1);
			button.y = boxHeight + SPACING;
			boxHeight += SPACING + BUTTON_HEIGHT + PADDING;
			
			// Set entry box position on screen
			x = FP.halfWidth - (boxWidth >> 1);
			y = FP.halfHeight - (boxHeight >> 1);
			
			// Add box background
			addGraphic( Image.createRect( boxWidth, boxHeight, BOX_BACK ) );
			
			// Add all graphics
			addGraphic( titleText );
			for each (var textEntry:Text in textEntries ) 
			{
				addGraphic( textEntry );
			}
			addGraphic( cursor );
			addGraphic( button );
			
			callback = setCallback;
		}
		
		/**
		 * Called when the entry box is added to the world
		 */
		override public function added():void 
		{
			super.added();
			
			// Clear input buffer before accepting text
			Input.clear();
			if( enteredNames[0] != null )
				Input.keyString = enteredNames[0];
			else
				Input.keyString = "";
		}
		
		/**
		 * Update the entity every frame, used for the blinking cursor
		 */
		override public function update():void 
		{
			// Count time until next blink toggle
			blinkTimer += FP.elapsed;
			if ( blinkTimer >= BLINK_RATE )
			{
				blinkTimer = 0.0;
				cursor.visible = !cursor.visible;
			}
			
			// Display entered text and reposition cursor as necessary
			if ( Input.keyString.length > ENTRY_LENGTH )
				Input.keyString = Input.keyString.substr( 0, ENTRY_LENGTH );
			enteredNames[selectedIndex] = Input.keyString;
			textEntries[selectedIndex].text = enteredNames[selectedIndex];
			if ( textEntries.length > 1 )
				textEntries[selectedIndex].text = (selectedIndex + 1) + ". " + textEntries[selectedIndex].text;
			cursor.x = textEntries[selectedIndex].x + textEntries[selectedIndex].textWidth + CURSOR_SPACING;
			cursor.y = textEntries[selectedIndex].y;
			
			// Detect if the user pressed enter
			if ( Input.pressed( Key.ENTER ) || Input.pressed( Key.NUMPAD_ENTER ) )
			{
				SetEntry( selectedIndex + 1 );
			}
			
			// Detect if the OK button has been pressed
			if ( !button.buttonSelected && Input.mousePressed &&
				Input.mouseX >= x + button.x && Input.mouseX <= x + button.x + BUTTON_WIDTH &&
				Input.mouseY >= y + button.y && Input.mouseY <= y + button.y + BUTTON_HEIGHT )
			{
				button.buttonSelected = true;
			}
			else if ( button.buttonSelected && Input.mouseUp )
			{
				button.buttonSelected = false;
				
				if( Input.mouseX >= x + button.x && Input.mouseX <= x + button.x + BUTTON_WIDTH &&
					Input.mouseY >= y + button.y && Input.mouseY <= y + button.y + BUTTON_HEIGHT )
				{
					button.callback();
				}
			}
			
			// Detect if a text box has been clicked
			for ( var i:int = 0; i < textEntries.length; i++ )
			{
				if ( Input.mousePressed &&
					Input.mouseX >= x + textEntries[i].x && Input.mouseX <= x + boxWidth - PADDING &&
					Input.mouseY >= y + textEntries[i].y && Input.mouseY <= y + textEntries[i].y + textEntries[i].height )
				{
					SetEntry( i );
				}
			}
			
			super.update();
		}
		
		/**
		 * Move cursor to next text entry box
		 */
		private function SetEntry( setIndex:int ):void
		{
			selectedIndex = setIndex;
			
			// If the cursor is on the last text entry box, close the box
			if ( selectedIndex > textEntries.length - 1 )
			{
				--selectedIndex;
				FinishEntry();
			}
			else
			{
				Input.clear();
				if( enteredNames[selectedIndex] != null )
					Input.keyString = enteredNames[selectedIndex];
				else
					Input.keyString = "";
			}
		}
		
		/**
		 * Close the text entry box
		 */
		private function FinishEntry():void
		{
			// Call the callback function with the entered names if it is set
			if ( callback != null )
			{
				// If multiple names were entered, pass the array - otherwise, pass the name as a string
				if( enteredNames.length > 1 )
					callback( enteredNames );
				else
					callback( enteredNames[selectedIndex] );
			}
			
			// Remove the entry box
			FP.world.remove( this );
		}
		
	}

}