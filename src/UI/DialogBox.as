package UI 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	import net.flashpunk.FP;
	import UI.Elements.*;
	
	/**
	 * Entity to display a dialog box onscreen
	 * 
	 * @author Switchbreak
	 */
	public class DialogBox extends Entity 
	{
		
		private static const SPACING:int = 10;
		private static const BACK_COLOR:uint = 0;
		private static const BUTTON_WIDTH:int = 60;
		private static const BUTTON_HEIGHT:int = 25;
		
		private var buttonX:int, buttonY:int;
		private var buttons:Vector.<Button>;
		
		/**
		 * Create the dialog box
		 */
		public function DialogBox( dialogText:String ) 
		{
			layer = GameWorld.HUD_LAYER;
			graphic = new Graphiclist();
			graphic.scrollX = 0;
			graphic.scrollY = 0;
			
			var textBox:Text = new Text( dialogText, SPACING, SPACING );
			textBox.align = "center";
			
			buttonX = textBox.width + SPACING;
			buttonY = textBox.height + (SPACING << 1);
			
			var backWidth:int = buttonX + SPACING;
			var backHeight:int = buttonY + BUTTON_HEIGHT + SPACING;
			
			x = FP.halfWidth - (backWidth >> 1);
			y = FP.halfHeight - (backHeight >> 1);
			
			addGraphic( Image.createRect( backWidth, backHeight, BACK_COLOR ) );
			addGraphic( textBox );
			
			buttons = new Vector.<Button>();
		}
		
		/**
		 * Add a button to the dialog box
		 * 
		 * @param	buttonText
		 * Caption of the button
		 * @param	callback
		 * Callback to trigger when the button is clicked
		 */
		public function AddButton( buttonText:String, callback:Function ):void
		{
			var newButton:Button = new Button( buttonText, BUTTON_WIDTH, BUTTON_HEIGHT, callback );
			
			buttonX -= newButton.width;
			newButton.x = buttonX;
			newButton.y = buttonY;
			
			buttonX -= SPACING;
			
			buttons.push( newButton );
			addGraphic( newButton );
		}
		
		/**
		 * Process input
		 */
		override public function update():void 
		{
			// Detect clicks on all of the buttons
			for each( var button:Button in buttons )
			{
				if ( !button.buttonSelected && Input.mousePressed &&
					Input.mouseX >= x + button.x && Input.mouseX <= x + button.x + button.width &&
					Input.mouseY >= y + button.y && Input.mouseY <= y + button.y + button.height )
				{
					button.buttonSelected = true;
				}
				else if ( button.buttonSelected && Input.mouseUp )
				{
					button.buttonSelected = false;
					
					if ( Input.mouseX >= x + button.x && Input.mouseX <= x + button.x + button.width &&
						Input.mouseY >= y + button.y && Input.mouseY <= y + button.y + button.height )
					{
						FP.world.remove( this );
						
						if( button.callback != null )
							button.callback();
					}
				}
			}
			
			super.update();
		}
		
	}

}