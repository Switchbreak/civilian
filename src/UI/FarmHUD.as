package UI
{
	import UI.Elements.FieldTile
	import Types.*;
	import UI.Elements.*;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	import net.flashpunk.FP;
	import net.flashpunk.Entity;
	import net.flashpunk.utils.Input;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Spritemap;
	import Weather.Snowstorm;
	
	/**
	 * Entity containing HUD for the home farm screen
	 * 
	 * @author Switchbreak
	 */
	public class FarmHUD extends Entity 
	{
		// Embeds
		[Embed (source = '../../img/IconAttack.png')]	private static const ICON_ATTACK:Class;
		[Embed (source = '../../img/IconDefend.png')]	private static const ICON_DEFEND:Class;
		[Embed (source = '../../img/IconTax.png')]		private static const ICON_TAX:Class;
		[Embed (source = '../../font/OpenSans-Bold.ttf', embedAsCFF = 'false', fontFamily = 'HUD Font')] private static const HUD_FONT:Class;
		
		// Constants
		private static const LINE_SPACING:int				= 0;
		private static const BAR_SIZE:int					= 75;
		private static const BAR_BGCOLOR:uint				= 0x222222;
		private static const BUTTON_WIDTH:int				= 50;
		private static const BUTTON_HEIGHT:int				= 50;
		private static const BUTTON_SPACING:int				= 10;
		private static const TEXT_WIDTH:int					= 100;
		private static const TEXT_COLOR:uint				= 0xFFFFFF;
		private static const TEXT_OFFSET:int				= 3;
		private static const ICON_SPACING:int				= 7;
		private static const ICON_FONT_SIZE:int				= 14;
		private static const ICON_DISABLED_ALPHA:Number		= 0.2;
		
		// Public variables
		public var eventSilo:Function		= null;
		public var eventNext:Function		= null;
		public var eventMap:Function		= null;
		public var eventPlant:Function		= null;
		public var eventTend:Function		= null;
		public var eventHarvest:Function	= null;
		public var eventClear:Function		= null;
		public var eventBuy:Function		= null;
		public var eventSell:Function		= null;
		public var enabled:Boolean			= true;
		
		// Private variables
		private var actionButtonX:int = FP.width - (BUTTON_WIDTH << 1) - (BUTTON_SPACING << 1);
		private var sim:Sim;
		
		// UI element values
		private var farmerNameString:String;
		private var spouseNameString:String;
		private var childNamesString:Vector.<String>;
		private var countryString:String;
		private var seasonInt:int;
		private var moneyNumber:Number;
		
		// Graphics
		private var farmerName:Text;
		private var spouseName:Text;
		private var childNames:Vector.<Text>;
		private var season:Text;
		private var money:Text;
		private var actionButtons:Vector.<Button>;
		private var farmButtons:Vector.<Button>;
		private var newsTicker:NewsTicker;
		private var countryName:Text;
		private var countryFlag:Stamp;
		private var iconInvading:Image;
		private var textInvading:Text;
		private var iconDefending:Image;
		private var textDefending:Text;
		private var iconTax:Image;
		private var textTax:Text;
		private var snowstorm:Snowstorm;
		
		//
		// Public functions
		//
		
		public function FarmHUD( setSim:Sim = null )
		{
			sim = setSim;
			if ( sim == null )
				sim = GameWorld.sim;
		}
		
		/**
		 * Create a home Farm screen HUD object
		 */
		override public function added():void 
		{
			// Get current display values from simulation
			farmerNameString = sim.farmer.name;
			spouseNameString = sim.farmer.spouse;
			childNamesString = sim.farmer.children;
			seasonInt = sim.season;
			moneyNumber = sim.farmer.money;
			
			//
			// Farmer info HUD section
			//
			
			layer = GameWorld.HUD_LAYER;
			graphic = new Graphiclist();
			graphic.scrollX = 0;
			graphic.scrollY = 0;
			
			var lineY:int = 0;
			
			farmerName = addGraphic( new Text( (farmerNameString == null ? " " : farmerNameString) ) ) as Text;
			farmerName.color = TEXT_COLOR;
			farmerName.font = "HUD Font";
			farmerName.border = true;
			farmerName.x = TEXT_OFFSET;
			lineY += farmerName.height + LINE_SPACING;
			
			spouseName = addGraphic( new Text( (spouseNameString == null ? " " : spouseNameString), 0, lineY ) ) as Text;
			spouseName.color = TEXT_COLOR;
			spouseName.font = "HUD Font";
			spouseName.border = true;
			spouseName.x = TEXT_OFFSET;
			lineY += spouseName.height + LINE_SPACING;
			
			childNames = new Vector.<Text>;
			for ( var i:int = 1; i <= childNamesString.length; i++ )
			{
				var childName:Text = new Text( i + ": " + childNamesString[i - 1], 0, lineY );
				childName.color = TEXT_COLOR;
				childName.font = "HUD Font"
				childName.border = true;
				childName.x = TEXT_OFFSET;
				addGraphic( childName );
				childNames.push( childName );
				lineY += childName.height + LINE_SPACING;
			}
			
			//
			// Button bar HUD section
			//
			
			var bar:Image = Image.createRect( FP.width, BAR_SIZE, BAR_BGCOLOR );
			bar.x = 0;
			bar.y = FP.height - BAR_SIZE;
			addGraphic( bar );
			
			actionButtons = new Vector.<Button>();
			farmButtons = new Vector.<Button>();
			
			var nextButton:Button = new Button( ResourceManager.getInstance().getString( "resources", "NEXT_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, eventNext );
			nextButton.x = FP.width - BUTTON_WIDTH - BUTTON_SPACING;
			nextButton.y = FP.height - (BAR_SIZE >> 1) - (BUTTON_HEIGHT >> 1);
			actionButtons.push( addGraphic( nextButton ) as Button );
			
			var mapButton:Button = new Button( ResourceManager.getInstance().getString( "resources", "MAP_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, eventMap );
			mapButton.x = BUTTON_SPACING;
			mapButton.y = FP.height - (BAR_SIZE >> 1) - (BUTTON_HEIGHT >> 1);
			actionButtons.push( addGraphic( mapButton ) as Button );
			
			AddButton( ResourceManager.getInstance().getString( "resources", "SELL_BUTTON" ), eventSell );
			AddButton( ResourceManager.getInstance().getString( "resources", "BUY_BUTTON" ), eventBuy );
			
			newsTicker = new NewsTicker( 0, FP.height - BAR_SIZE, FP.width, sim.newsStories );
			addGraphic( newsTicker );
			
			//
			// Game state info section
			//
			
			lineY = 0;
			
			season = new Text( "" );
			season.color = TEXT_COLOR;
			season.font = "HUD Font";
			season.border = true;
			season.width = TEXT_WIDTH;
			season.align = "right";
			season.x = FP.width - TEXT_WIDTH - TEXT_OFFSET;
			season.y = lineY;
			UpdateSeason();
			addGraphic( season );
			lineY += season.height + LINE_SPACING;
			
			money = new Text( "$" + moneyNumber.toFixed( 2 ) );
			money.color = TEXT_COLOR;
			money.font = "HUD Font";
			money.border = true;
			money.width = TEXT_WIDTH;
			money.align = "right";
			money.x = FP.width - TEXT_WIDTH - TEXT_OFFSET;
			money.y = lineY;
			addGraphic( money );
			
			//
			// Toolbar country status
			//
			
			// Country name
			countryName = new Text( sim.country.name );
			countryName.x = BUTTON_WIDTH + (BUTTON_SPACING << 2);
			countryName.y = FP.height - BAR_SIZE - TEXT_OFFSET;
			
			// Country flag
			countryFlag = new Stamp( sim.country.occupiedBy.flag );
			countryFlag.x = countryName.x;
			countryFlag.y = countryName.y + countryName.height;
			
			// Center country name
			countryName.width = countryFlag.width;
			countryName.align = "center";
			
			addGraphic( countryName );
			addGraphic( countryFlag );
			
			// Add status icons
			iconInvading = new Image( ICON_ATTACK );
			iconInvading.x = countryFlag.x + countryFlag.width + (BUTTON_SPACING << 1);
			iconInvading.y = FP.height - BAR_SIZE + ICON_SPACING;
			textInvading = new Text( "", iconInvading.x + iconInvading.width + ICON_SPACING, iconInvading.y - TEXT_OFFSET );
			textInvading.size = ICON_FONT_SIZE;
			addGraphic( iconInvading );
			addGraphic( textInvading );
			
			iconDefending = new Image( ICON_DEFEND );
			iconDefending.x = countryFlag.x + countryFlag.width + (BUTTON_SPACING << 1);
			iconDefending.y = iconInvading.y + iconInvading.height + ICON_SPACING;
			textDefending = new Text( "", iconDefending.x + iconDefending.width + ICON_SPACING, iconDefending.y - TEXT_OFFSET );
			textDefending.size = ICON_FONT_SIZE;
			addGraphic( iconDefending );
			addGraphic( textDefending );
			
			iconTax = new Image( ICON_TAX );
			iconTax.x = countryFlag.x + countryFlag.width + (BUTTON_SPACING << 1);
			iconTax.y = iconDefending.y + iconDefending.height + ICON_SPACING
			textTax = new Text( "", iconTax.x + iconTax.width + ICON_SPACING, iconTax.y - TEXT_OFFSET );
			textTax.size = ICON_FONT_SIZE;
			addGraphic( iconTax );
			addGraphic( textTax );
			
			super.added();
		}
		
		/**
		 * Process input for HUD
		 */
		override public function update():void 
		{
			if ( enabled )
			{
				// Capture mouse press event
				if ( Input.mousePressed )
				{
					// Check if mouse was pressed over any of the action buttons
					for each( var button:Button in actionButtons )
					{
						if ( !button.buttonSelected &&
							Input.mouseX >= x + button.x && Input.mouseX <= x + button.x + BUTTON_WIDTH &&
							Input.mouseY >= y + button.y && Input.mouseY <= y + button.y + BUTTON_HEIGHT )
						{
							button.buttonSelected = true;
						}
					}
					
					// Check for farm controls if the farm exists
					if ( sim.farm != null )
					{
						// Check if mouse was pressed over a farm button
						for each( button in farmButtons )
						{
							if ( !button.buttonSelected &&
								Input.mouseX >= x + button.x && Input.mouseX <= x + button.x + BUTTON_WIDTH &&
								Input.mouseY >= y + button.y && Input.mouseY <= y + button.y + BUTTON_HEIGHT )
							{
								button.buttonSelected = true;
							}
						}
					}
				}
				
				// Capture mouse release event
				if ( Input.mouseReleased )
				{
					// Check if mouse was released over a selected action button
					for each( button in actionButtons )
					{
						if ( button.buttonSelected )
						{
							button.buttonSelected = false;
							
							if ( button.callback != null &&
								Input.mouseX >= x + button.x && Input.mouseX <= x + button.x + BUTTON_WIDTH &&
								Input.mouseY >= y + button.y && Input.mouseY <= y + button.y + BUTTON_HEIGHT )
							{
								button.callback();
							}
						}
					}
					
					// Check for farm controls if the farm exists
					if ( sim.farm != null )
					{
						// Check if mouse was released over a farm button
						for each( button in farmButtons )
						{
							if ( button.buttonSelected && Input.mouseUp )
							{
								button.buttonSelected = false;
								
								if ( button.callback != null &&
									Input.mouseX >= x + button.x && Input.mouseX <= x + button.x + BUTTON_WIDTH &&
									Input.mouseY >= y + button.y && Input.mouseY <= y + button.y + BUTTON_HEIGHT )
								{
									button.callback();
								}
							}
						}
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Function to update the HUD when farmer data changes
		 */
		public function UpdateHUD():void
		{
			// Get current display values from simulation
			farmerNameString = sim.farmer.name;
			spouseNameString = sim.farmer.spouse;
			childNamesString = sim.farmer.children;
			countryString = sim.country.name;
			seasonInt = sim.season;
			moneyNumber = sim.farmer.money;
			
			// Set the weather based on the season
			if ( seasonInt == Sim.SEASON_WINTER && snowstorm == null )
			{
				snowstorm = FP.world.add( new Snowstorm() ) as Snowstorm;
			}
			else if ( seasonInt != Sim.SEASON_WINTER && snowstorm != null )
			{
				FP.world.remove( snowstorm );
				snowstorm = null;
			}
			
			// Set farmer and spouse name text
			farmerName.text = (farmerNameString == null ? " " : farmerNameString);
			spouseName.text = (spouseNameString == null ? " " : spouseNameString);
			
			// Clear out child text boxes
			for each( var childName:Text in childNames )
			{
				(graphic as Graphiclist).remove( childName );
			}
			childNames.length = 0;
			
			// Read child text boxes
			var lineY:int = farmerName.height + spouseName.height + (LINE_SPACING << 1);
			for ( var i:int = 1; i <= childNamesString.length; i++ )
			{
				childName = new Text( i + ": " + childNamesString[i - 1], 0, lineY );
				childName.color = TEXT_COLOR;
				childName.border = true;
				childName.font = "HUD Font";
				childName.x = TEXT_OFFSET;
				addGraphic( childName );
				childNames.push( childName );
				lineY += childName.height + LINE_SPACING;
			}
			
			money.text = "$" + moneyNumber.toFixed( 2 );
			
			// Update toolbar country status
			countryName.text = countryString;
			(graphic as Graphiclist).remove( countryFlag );
			countryFlag = new Stamp( sim.country.occupiedBy.flag );
			countryFlag.x = countryName.x;
			countryFlag.y = countryName.y + countryName.height;
			addGraphic( countryFlag );
			
			// Set status icons
			if ( sim.country.wars.length > 0 )
			{
				var attacking:Vector.<String> = new Vector.<String>();
				var defending:Vector.<String> = new Vector.<String>();
				
				for each( var war:War in sim.country.wars )
				{
					if ( war.invader == sim.country )
						attacking.push( war.invaded.name );
					if ( war.invaded == sim.country )
						defending.push( war.invader.name );
				}
				
				if ( attacking.length > 0 )
				{
					iconInvading.alpha = 1.0;
					textInvading.text = StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "ICON_INVADING" ), attacking.join( ", " ) );
				}
				else
				{
					iconInvading.alpha = ICON_DISABLED_ALPHA;
					textInvading.text = "";
				}
				
				if ( defending.length > 0 )
				{
					iconDefending.alpha = 1.0;
					textDefending.text = StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "ICON_DEFENDING" ), defending.join( ", " ) );
				}
				else
				{
					iconDefending.alpha = ICON_DISABLED_ALPHA;
					textDefending.text = "";
				}
			}
			else
			{
				iconInvading.alpha = ICON_DISABLED_ALPHA;
				textInvading.text = "";
				iconDefending.alpha = ICON_DISABLED_ALPHA;
				textDefending.text = "";
			}
			
			textTax.text = StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "ICON_TAX" ), sim.country.tax.toFixed( 2 ) );
			
			UpdateSeason();
		}
		
		/**
		 * Function to reset the news ticker
		 */
		public function ResetTicker():void 
		{
			newsTicker.ResetTicker();
		}
		
		/**
		 * Sets the currently displayed season
		 * 
		 * @param	season
		 * Season as integer
		 */
		public function UpdateSeason():void
		{
			// Show the season text box
			switch( seasonInt )
			{
				case Sim.SEASON_SPRING:
					season.text = ResourceManager.getInstance().getString( "resources", "SEASON_SPRING" );
					break;
				case Sim.SEASON_SUMMER:
					season.text = ResourceManager.getInstance().getString( "resources", "SEASON_SUMMER" );
					break;
				case Sim.SEASON_FALL:
					season.text = ResourceManager.getInstance().getString( "resources", "SEASON_FALL" );
					break;
				case Sim.SEASON_WINTER:
					season.text = ResourceManager.getInstance().getString( "resources", "SEASON_WINTER" );
					break;
			}
		}
		
		/**
		 * Function to add an action button to the HUD
		 * 
		 * @param	text
		 * Caption for the button
		 * @param	callback
		 * Callback to trigger when the button is clicked
		 */
		private function AddButton( text:String, callback:Function ):void
		{
			var button:Button = new Button( text, BUTTON_WIDTH, BUTTON_HEIGHT, callback );
			button.x = actionButtonX;
			button.y = FP.height - (BAR_SIZE >> 1) - (BUTTON_HEIGHT >> 1);
			farmButtons.push( addGraphic( button ) as Button );
			
			actionButtonX -= BUTTON_WIDTH + BUTTON_SPACING;
		}
	}

}