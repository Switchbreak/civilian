package UI
{
	import Types.*;
	import UI.Elements.*;
	import mx.resources.ResourceManager
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	/**
	 * Entity to display all farm info on one screen
	 * 
	 * @author Switchbreak
	 */
	public class FarmInfoScreen extends Entity 
	{
		// Constants
		private static const COLUMN_WIDTH:int = 240;
		private static const ROW_SPACING:int = 10;
		private static const COLUMN_SPACING:int = 10;
		private static const BACK_COLOR:uint = 0;
		private static const BUTTON_WIDTH:int = 65;
		private static const BUTTON_HEIGHT:int = 25;
		
		// Graphics
		private var items:Vector.<LineItem>;
		private var deleteButton:Button;
		private var reseedButton:Button;
		private var okButton:Button;
		
		/**
		 * Create and display the info screen
		 */
		public function FarmInfoScreen( callback:Function = null ) 
		{
			layer = GameWorld.HUD_LAYER;
			graphic = new Graphiclist();
			graphic.scrollX = 0;
			graphic.scrollY = 0;
			
			// Initialize lists of graphics
			var foreground:Vector.<Graphic> = new Vector.<Graphic>();
			items = new Vector.<LineItem>();
			
			// Show one column for each silo
			var columnX:int = COLUMN_SPACING;
			for ( var silo:int = 0; silo < Farm.SILO_COUNT; silo++ )
			{
				var rowY:int = ROW_SPACING;
				
				// Add header to the top of the silo column
				var header:Text = new Text( ResourceManager.getInstance().getString( "resources", "FARM_INFO_SILO" ) + " " + (silo + 1), columnX, rowY );
				foreground.push( header );
				rowY += header.height + ROW_SPACING;
				
				// Show all storage slots in the silo
				for ( var product:int = 0; product < Farm.SILO_SIZE; product++ )
				{
					// Create line item for storage slot
					var lineItem:LineItem = new LineItem();
					lineItem.itemName = new Text( " ", columnX, rowY );
					lineItem.amount = new Text( "", columnX, rowY );
					lineItem.amount.align = "right";
					lineItem.amount.width = COLUMN_WIDTH;
					
					// Add name to list
					items.push( lineItem );
					
					rowY += lineItem.itemName.height + ROW_SPACING;
				}
				
				columnX += COLUMN_WIDTH + COLUMN_SPACING;
			}
			
			// Show storage items in list
			PopulateList();
			
			rowY += ROW_SPACING;
			
			// Add OK button
			okButton = new Button( ResourceManager.getInstance().getString( "resources", "OK_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT, callback );
			okButton.x = columnX - BUTTON_WIDTH - COLUMN_SPACING;
			okButton.y = rowY;
			
			// Add delete button
			deleteButton = new Button( ResourceManager.getInstance().getString( "resources", "DELETE_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT );
			deleteButton.x = COLUMN_SPACING;
			deleteButton.y = rowY;
			
			// Add reseed button
			reseedButton = new Button( ResourceManager.getInstance().getString( "resources", "RESEED_BUTTON" ), BUTTON_WIDTH, BUTTON_HEIGHT );
			reseedButton.x = (COLUMN_SPACING << 1) + BUTTON_WIDTH;
			reseedButton.y = rowY;
			
			rowY += BUTTON_HEIGHT + ROW_SPACING;
			
			// Show all graphics on dialog box
			addGraphic( Image.createRect( columnX, rowY, BACK_COLOR ) );
			for each( var foreGraphic:Graphic in foreground )
			{
				addGraphic( foreGraphic );
			}
			for each( var item:LineItem in items )
			{
				addGraphic( item.itemName );
				addGraphic( item.amount );
			}
			addGraphic( okButton );
			addGraphic( deleteButton );
			addGraphic( reseedButton );
			
			// Center dialog box on screen
			x = FP.halfWidth - (columnX >> 1);
			y = FP.halfHeight - (rowY >> 1);
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			if ( deleteButton.buttonSelected || reseedButton.buttonSelected )
			{
				if ( Input.mousePressed )
				{
					for ( var i:int = 0; i < items.length; i++ )
					{
						if ( GameWorld.sim.farm.silo[i] != null && 
							Input.mouseX >= x + items[i].itemName.x && Input.mouseX <= x + items[i].itemName.x + COLUMN_WIDTH &&
							Input.mouseY >= y + items[i].itemName.y && Input.mouseY <= y + items[i].itemName.y + items[i].itemName.height )
						{
							if ( reseedButton.buttonSelected )
							{
								ReseedItem( i );
							}
							if ( deleteButton.buttonSelected )
							{
								DeleteItem( i );
							}
						}
					}
					
					deleteButton.buttonSelected = false;
					reseedButton.buttonSelected = false;
				}
			}
			else
			{
				if ( Input.mousePressed )
				{
					// Detect if the OK button has been pressed
					if ( !okButton.buttonSelected &&
						Input.mouseX >= x + okButton.x && Input.mouseX <= x + okButton.x + BUTTON_WIDTH &&
						Input.mouseY >= y + okButton.y && Input.mouseY <= y + okButton.y + BUTTON_HEIGHT )
					{
						okButton.buttonSelected = true;
					}
					
					// Detect if the delete button has been pressed
					if ( !deleteButton.buttonSelected &&
						Input.mouseX >= x + deleteButton.x && Input.mouseX <= x + deleteButton.x + BUTTON_WIDTH &&
						Input.mouseY >= y + deleteButton.y && Input.mouseY <= y + deleteButton.y + BUTTON_HEIGHT )
					{
						deleteButton.buttonSelected = true;
					}
					
					// Detect if the reseed button has been pressed
					if ( !reseedButton.buttonSelected &&
						Input.mouseX >= x + reseedButton.x && Input.mouseX <= x + reseedButton.x + BUTTON_WIDTH &&
						Input.mouseY >= y + reseedButton.y && Input.mouseY <= y + reseedButton.y + BUTTON_HEIGHT )
					{
						reseedButton.buttonSelected = true;
					}
				}
				else if ( Input.mouseUp )
				{
					if ( okButton.buttonSelected )
					{
						okButton.buttonSelected = false;
						
						if ( okButton.callback != null &&
							Input.mouseX >= x + okButton.x && Input.mouseX <= x + okButton.x + BUTTON_WIDTH &&
							Input.mouseY >= y + okButton.y && Input.mouseY <= y + okButton.y + BUTTON_HEIGHT )
						{
							FP.world.remove( this );
							
							okButton.callback();
						}
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Delete an item in storage
		 * 
		 * @param	itemIndex
		 * Index of the item to delete
		 */
		public function DeleteItem( itemIndex:int ):void
		{
			GameWorld.sim.DiscardStorage( itemIndex );
			
			items[itemIndex].amount.text = "";
			items[itemIndex].itemName.text = (itemIndex % Farm.SILO_SIZE + 1) + ". " + ResourceManager.getInstance().getString( "resources", "LIST_NONE" );
		}
		
		/**
		 * Reseed an item in storage
		 * 
		 * @param	itemIndex
		 * Index of the item to reseed
		 */
		public function ReseedItem( itemIndex:int ):void
		{
			// If reseeding succeeds, update the storage display list
			if ( GameWorld.sim.Reseed( itemIndex ) )
			{
				PopulateList();
			}
		}
		
		/**
		 * Fills the display list with the products stored in the silo
		 */
		public function PopulateList():void
		{
			for ( var silo:int = 0; silo < Farm.SILO_COUNT; silo++ )
			{
				for ( var product:int = 0; product < Farm.SILO_SIZE; product++ )
				{
					// Show product stored in slot if any
					var plantType:String = ResourceManager.getInstance().getString( "resources", "LIST_NONE" );
					var amount:String = "";
					if ( GameWorld.sim.farm.silo[silo * Farm.SILO_SIZE + product] != null )
					{
						// Get crop name
						plantType = GameWorld.sim.crops[GameWorld.sim.farm.silo[silo * Farm.SILO_SIZE + product].plantType].cropName;
						
						// Append crop type to the name
						switch( GameWorld.sim.farm.silo[silo * Farm.SILO_SIZE + product].type )
						{
							case Product.TYPE_SEED:
								plantType += " " + ResourceManager.getInstance().getString( "resources", "TYPE_SEED" );
								break;
							case Product.TYPE_CROP:
								plantType += " " + ResourceManager.getInstance().getString( "resources", "TYPE_CROP" );
								break;
						}
						
						// Append stored product age to the name
						if ( GameWorld.sim.farm.silo[silo * Farm.SILO_SIZE + product].storageTime <= Product.STORAGE_TIME_FRESH )
							plantType += " - " + ResourceManager.getInstance().getString( "resources", "AGE_FRESH" );
						else if( GameWorld.sim.farm.silo[silo * Farm.SILO_SIZE + product].storageTime <= Product.STORAGE_TIME_NORMAL )
							plantType += " - " + ResourceManager.getInstance().getString( "resources", "AGE_NORMAL" );
						else if( GameWorld.sim.farm.silo[silo * Farm.SILO_SIZE + product].storageTime <= Product.STORAGE_TIME_OLD )
							plantType += " - " + ResourceManager.getInstance().getString( "resources", "AGE_OLD" );
						else 
							plantType += " - " + ResourceManager.getInstance().getString( "resources", "AGE_ROTTEN" );
						
						// Get quantity
						amount = GameWorld.sim.farm.silo[silo * Farm.SILO_SIZE + product].quantity.toString();
					}
					
					items[silo * Farm.SILO_SIZE + product].itemName.text = (product + 1) + ". " + plantType;
					items[silo * Farm.SILO_SIZE + product].amount.text = amount;
				}
			}
		}
		
	}

}

import net.flashpunk.graphics.Text;

/**
 * Object containing graphics for buy screen line items
 */
class LineItem
{
	public var itemName:Text;
	public var amount:Text;
}