package UI.Elements 
{
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	/**
	 * News Ticker UI element, shows a news story scrolling by
	 * 
	 * @author Switchbreak
	 */
	public class NewsTicker extends Graphiclist 
	{
		// Embeds
		[Embed (source = '../../../font/OpenSans-Bold.ttf', embedAsCFF = 'false', fontFamily = 'Ticker Font')] private static const UI_FONT:Class;
		
		// Constants
		private static const BACK_COLOR:uint = 0;
		private static const BORDER_COLOR:uint = 0xFFFFFF;
		private static const BORDER_THICKNESS:uint = 2;
		private static const TICKER_HEIGHT:int = 26;
		private static const PADDING:uint = 1;
		private static const STORY_TIME:Number = 2.0;
		private static const TEXT_OFFSET:int = 4;
		
		// Private variables
		private var tickerTimer:Number;
		private var currentStory:int;
		
		// Graphics
		private var newsStories:Vector.<String> = null;
		private var newsText:Text;
		
		/**
		 * Create news ticker object
		 * @param	setX
		 * Coordinates of bottom-left of news ticker
		 * @param	setY
		 * Coordinates of bottom-left of news ticker
		 * @param	width
		 * Width of news ticker
		 * @param	setNews
		 * List of news stories to display on ticker
		 */
		public function NewsTicker( setX:int, setY:int, width:int, setNews:Vector.<String> = null )
		{
			// Set graphic values
			relative = false;
			active = true;
			x = setX;
			newsStories = setNews;
			
			// Create news text object
			newsText = new Text( " ", PADDING, BORDER_THICKNESS + PADDING - TEXT_OFFSET );
			newsText.font = "Ticker Font";
			
			// Calculate ticker height based on text object height
			y = setY - TICKER_HEIGHT;
			ResetTicker();
			
			// Create border
			var topBorder:Image = Image.createRect( width, BORDER_THICKNESS, BORDER_COLOR );
			var bottomBorder:Image = Image.createRect( width, BORDER_THICKNESS, BORDER_COLOR );
			bottomBorder.y = TICKER_HEIGHT - BORDER_THICKNESS;
			
			// Add graphics to list
			add( Image.createRect( width, TICKER_HEIGHT, BACK_COLOR ) );
			add( newsText );
			add( topBorder );
			add( bottomBorder );
		}
		
		/**
		 * Sets the current text on the ticker
		 * 
		 * @param	setNews
		 * List of stories to display
		 */
		public function SetNewsText( setNews:Vector.<String> ):void
		{
			newsStories = setNews;
		}
		
		/**
		 * Reset news ticker to start at the first news story
		 */
		public function ResetTicker():void
		{
			// Set initial news story, if any are in the queue
			if ( newsStories != null && newsStories.length > 0 )
			{
				currentStory = 0;
				tickerTimer = 0;
				newsText.text = newsStories[currentStory];
			}
			else
			{
				// Clear current story and reset timer
				currentStory = -1;
				tickerTimer = STORY_TIME;
				newsText.text = "";
			}
		}
		
		/**
		 * Handle ticker stories scrolling
		 */
		override public function update():void 
		{
			// Check if there are news stories in the list to display
			if ( newsStories != null && newsStories.length > 0 )
			{
				// Rotate through the stories on a timer
				tickerTimer += FP.elapsed;
				if ( tickerTimer >= STORY_TIME )
				{
					tickerTimer = 0;
					currentStory = (currentStory + 1) % newsStories.length;
					
					newsText.text = newsStories[currentStory];
				}
			}
			
			super.update();
		}
		
	}

}