package UI.Elements
{
	import net.flashpunk.utils.Input;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	
	/**
	 * Version of the button object which handles its own input
	 * @author Switchbreak
	 */
	public class ActionButton extends Graphiclist 
	{
		// Constants
		private static const BUTTON_BACK:uint		= 0x888888;
		private static const BUTTON_SELECTED:uint	= 0xFFFFFF;
		private static const BUTTON_FRONT:uint		= 0;
		private static const BUTTON_SPACING:int		= 10;
		
		// Public variables
		public var	width:int;
		public var	height:int;
		public var	callback:Function;
		private var	_buttonSelected:Boolean = false;
		
		// Graphics
		private var buttonBack:Image;
		private var buttonText:Text;
		
		/**
		 * Create a button
		 * @param	text			Button caption text
		 * @param	setWidth		Width of button
		 * @param	setHeight		Height of button
		 * @param	setCallback		Function to call when the button is clicked
		 */
		public function ActionButton( text:String, setWidth:int, setHeight:int, setCallback:Function = null )
		{
			active		= true;
			relative	= false;
			width		= setWidth;
			height		= setHeight;
			callback	= setCallback;
			
			buttonText			= new Text( text );
			buttonText.color	= BUTTON_FRONT;
			buttonText.align	= "center";
			buttonText.y		= (height >> 1) - (buttonText.height >> 1);
			
			if ( buttonText.textWidth > width )
				width = buttonText.textWidth + (BUTTON_SPACING << 1);
			buttonText.width = width;
			
			buttonBack = Image.createRect( width, height, BUTTON_BACK );
			
			super( buttonBack, buttonText );
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			if ( visible )
			{
				if ( Input.mousePressed && MouseOver() )
				{
					buttonSelected = true;
				}
				if ( _buttonSelected && Input.mouseUp )
				{
					buttonSelected = false;
					
					if ( MouseOver() )
					{
						callback();
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Check if the mouse is over the button
		 * @return	True if mouse is over button
		 */
		private function MouseOver():Boolean
		{
			return Input.mouseX >= x && Input.mouseX <= x + width && Input.mouseY >= y && Input.mouseY <= y + height;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets or sets the selected stated of the button
		 */
		public function get buttonSelected():Boolean
		{
			return _buttonSelected;
		}
		public function set buttonSelected( setSelected:Boolean ):void
		{
			_buttonSelected = setSelected;
			if ( _buttonSelected )
				buttonBack.color = BUTTON_SELECTED;
			else
				buttonBack.color = BUTTON_BACK;
		}
		
		/**
		 * Gets or sets the button text
		 */
		public function get text():String
		{
			return buttonText.text;
		}
		public function set text( value:String ):void
		{
			buttonText.text = value;
		}
	}

}