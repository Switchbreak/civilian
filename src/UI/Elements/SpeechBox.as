package UI.Elements 
{
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.tweens.misc.Alarm;
	
	/**
	 * Displays a speech box on the bottom of the screen
	 * @author Switchbreak
	 */
	public class SpeechBox extends Graphiclist 
	{
		// Embeds
		[Embed(source = "../../../img/UI/DialogBox.png")] private static const BACKGROUND:Class;
		[Embed(source = '../../../font/OpenSans-Bold.ttf', embedAsCFF = 'false', fontFamily = 'Speech Font')] private static const SPEECH_FONT:Class;
		
		// Constants
		private static var TEXT_OPTIONS:Object		= { font: "Speech Font", size: 20, color: 0xFFFFFF, wordWrap: true, width: 630, height: 89 };
		private static var SHADOW_OPTIONS:Object	= { font: "Speech Font", size: 20, color: 0, wordWrap: true, width: 630, height: 89 };
		private static var TEXT_X:int				= 7;
		private static var TEXT_Y:int				= 2;
		private static var SHADOW_X:int				= 2;
		private static var SHADOW_Y:int				= 3;
		private static var TYPE_RATE:Number			= 0.05;
		
		// Variables
		private var text:Text;
		private var dropShadow:Text;
		private var speech:String;
		private var typeTimer:Number;
		private var letterIndex:int;
		
		/**
		 * Create and initialize the speech box
		 * @param	setSpeech	Speech to display
		 */
		public function SpeechBox( setSpeech:String )
		{
			var background:Stamp = new Stamp( BACKGROUND );
			text = new Text( "", TEXT_X, TEXT_Y, TEXT_OPTIONS );
			dropShadow = new Text( "", TEXT_X + SHADOW_X, TEXT_Y + SHADOW_Y, SHADOW_OPTIONS );
			
			super( background, dropShadow, text );
			
			relative	= false;
			scrollX		= 0;
			scrollY		= 0;
			x			= 0;
			y			= FP.height - background.height;
			
			SetSpeech( setSpeech );
		}
		
		/**
		 * Sets the speech and shows the typing animation
		 * @param	setSpeech	Speech to show
		 */
		public function SetSpeech( setSpeech:String ):void 
		{
			speech			= setSpeech;
			typeTimer		= TYPE_RATE;
			letterIndex		= 0;
			text.text		= "";
			dropShadow.text	= "";
			active			= true;
		}
		
		/**
		 * Show typing animation if active
		 */
		override public function update():void 
		{
			typeTimer -= FP.elapsed;
			if ( typeTimer <= 0 )
			{
				typeTimer				= TYPE_RATE;
				var typedSpeech:String	= speech.substring( 0, ++letterIndex );
				text.text				= typedSpeech;
				dropShadow.text			= typedSpeech;
				
				if ( letterIndex >= speech.length )
					active = false;
			}
			
			super.update();
		}
	}
}