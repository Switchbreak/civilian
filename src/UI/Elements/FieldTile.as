package UI.Elements 
{
	import net.flashpunk.Graphic;
	import net.flashpunk.utils.Input;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	
	/**
	 * Field tile UI element for farm HUD
	 * 
	 * @author Switchbreak
	 */
	public class FieldTile extends Graphiclist
	{
		// Embeds
		[Embed (source = '../../../img/TileField.png')]		private static const TILE_FIELD:Class;
		[Embed (source = '../../../img/TileHouse.png')]		private static const TILE_HOUSE:Class;
		[Embed (source = '../../../img/TileBarn.png')]		private static const TILE_BARN:Class;
		[Embed (source = '../../../img/TileSilo.png')]		private static const TILE_SILO:Class;
		[Embed (source = '../../../img/TilePasture.png')]	private static const TILE_PASTURE:Class;
		
		// Constants
		public static const TILE_WIDTH:int = 109;
		public static const TILE_HEIGHT:int = 63;
		
		// Tile types
		public static const TYPE_FIELD:int = 0;
		public static const TYPE_HOUSE:int = 1;
		public static const TYPE_BARN:int = 2;
		public static const TYPE_SILO:int = 3;
		public static const TYPE_PASTURE:int = 4;
		
		// Field frames
		public static const FRAME_EMPTY:int = 0;
		public static const FRAME_SEED:int = 1;
		public static const FRAME_SPROUT:int = 2;
		public static const FRAME_TENDED:int = 3;
		public static const FRAME_RIPE:int = 4;
		public static const FRAME_WITHERED:int = 5;
		
		// Private constants
		private static const GRID_LEFT:int = 152;
		private static const GRID_TOP:int = 182;
		private static const PASTURE_WIDTH:int = 59;
		private static const PASTURE_HEIGHT:int = 59;
		private static const HOUSE_WIDTH:int = 102;
		private static const HOUSE_HEIGHT:int = 61;
		private static const BARN_WIDTH:int = 42;
		private static const BARN_HEIGHT:int = 64;
		
		// Private variables
		private var fieldIndex:int;
		private var clickEvent:Function;
		
		// Public variables
		public var enabled:Boolean;
		public var passLocation:Boolean;
		
		// Graphics
		private var tileImage:Graphic;
		private var hoverText:Text;
		
		//
		// Public methods
		//
		
		/**
		 * Creates and initializes the field tile
		 * 
		 * @param	gridX
		 * Coordinates of the top-left of the tile grid
		 * @param	gridY
		 * Coordinates of the top-left of the tile grid
		 * @param	gridPos
		 * Position on the grid, represented as a number 1-9
		 * @param	type
		 * Type of tile, should be one of the TYPE static constants
		 * @param	setFieldIndex
		 * Index of the field this tile represents
		 */
		public function FieldTile( gridPos:int, type:int, setFieldIndex:int = -1 ) 
		{
			// Set object values
			relative = false;
			UpdatePosition( gridPos );
			fieldIndex = setFieldIndex;
			clickEvent = null;
			passLocation = false;
			enabled = true;
			
			// Create tile image based on tile type
			switch( type )
			{
				case TYPE_FIELD:
					tileImage = new Spritemap( TILE_FIELD, TILE_WIDTH, TILE_HEIGHT );
					break;
				case TYPE_HOUSE:
					tileImage = new Spritemap( TILE_HOUSE, HOUSE_WIDTH, HOUSE_HEIGHT );
					break;
				case TYPE_BARN:
					tileImage = new Spritemap( TILE_BARN, BARN_WIDTH, BARN_HEIGHT );
					break;
				case TYPE_SILO:
					tileImage = new Stamp( TILE_SILO );
					break;
				case TYPE_PASTURE:
					tileImage = new Spritemap( TILE_PASTURE, PASTURE_WIDTH, PASTURE_HEIGHT );
					break;
			}
			
			// Center the sprite on the tile
			tileImage.y = (TILE_HEIGHT >> 1) - ((tileImage as Object).height >> 1);
			tileImage.x = 3;
			if ( type != TYPE_PASTURE )
				tileImage.x += (TILE_WIDTH >> 1) - ((tileImage as Object).width >> 1);
			
			add( tileImage );
		}
		
		/**
		 * Capture mouse input
		 */
		override public function update():void 
		{
			// Only handle input if control is enabled
			if ( enabled )
			{
				// Handle hovers
				if ( hoverText != null )
				{
					if( !hoverText.visible &&
						Input.mouseX >= x && Input.mouseX <= x + TILE_WIDTH &&
						Input.mouseY >= y && Input.mouseY <= y + TILE_HEIGHT )
					{
						hoverText.visible = true;
					}
					else if ( hoverText.visible &&
						!(Input.mouseX >= x && Input.mouseX <= x + TILE_WIDTH &&
						Input.mouseY >= y && Input.mouseY <= y + TILE_HEIGHT) )
					{
						hoverText.visible = false;
					}
				}
				
				// Handle click events
				if ( clickEvent != null )
				{
					// Check if mouse was pressed while over the tile
					if( Input.mousePressed &&
						Input.mouseX >= x && Input.mouseX <= x + TILE_WIDTH &&
						Input.mouseY >= y && Input.mouseY <= y + TILE_HEIGHT )
					{
						// If there is a field index, pass it to the click event
						if ( fieldIndex >= 0 )
						{
							// If passing location to callback, send center of tile
							if ( passLocation )
								clickEvent( fieldIndex, x + (TILE_WIDTH >> 1), y + (TILE_HEIGHT >> 1) );
							else
								clickEvent( fieldIndex );
						}
						else
						{
							clickEvent();
						}
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Function to set the field tiles properties all together
		 * 
		 * @param	setFrame
		 * Frame to display
		 * @param	setHover
		 * Hovertext to display
		 * @param	setCallback
		 * Click event to call
		 * @param	setPassLocation
		 * Whether or not the callback event should be passed the tile location
		 */
		public function UpdateStatus( setFrame:int, setHover:String = null, setCallback:Function = null, setPassLocation:Boolean = false ):void
		{
			frame = setFrame;
			hover = setHover;
			passLocation = setPassLocation;
			callback = setCallback;
		}
		
		/**
		 * Function to update the grid position of the tile
		 * 
		 * @param	gridX
		 * Position of the top-left corner of the grid
		 * @param	gridY
		 * Position of the top-left corner of the grid
		 * @param	gridPos
		 * Grid position of the tile
		 */
		public function UpdatePosition( gridPos:int ):void
		{
			x = GRID_LEFT + ((gridPos - 1) % 3) * TILE_WIDTH;
			y = GRID_TOP + Math.floor((gridPos - 1) / 3) * TILE_HEIGHT;
		}
		
		//
		// Properties
		//
		
		/**
		 * Sets the current frame for field tiles
		 */
		public function set frame( value:int ):void
		{
			(tileImage as Spritemap).frame = value;
		}
		
		/**
		 * Sets the hover text
		 */
		public function set hover( value:String ):void
		{
			// If the string is a null string, delete the hovertext graphic
			if ( value == null )
			{
				if ( hoverText != null )
				{
					remove( hoverText );
					hoverText = null;
				}
			}
			else
			{
				// Create hovertext graphic if it doesn't exist
				if ( hoverText == null )
				{
					// Make sure tile is active to capture mouse events
					active = true;
					
					// Create text object and keep it invisible until hover event
					hoverText = new Text( value );
					hoverText.border = true;
					hoverText.width = TILE_WIDTH;
					hoverText.align = "center";
					hoverText.y = (TILE_HEIGHT >> 1) - (hoverText.height >> 1);
					hoverText.visible = false;
					
					add( hoverText );
				}
				else
				{
					// Hovertext graphic exists, update text
					hoverText.text = value;
				}
			}
			
			active = (clickEvent != null && hoverText != null );
		}
		
		/**
		 * Sets the callback function
		 */
		public function set callback( value:Function ):void
		{
			clickEvent = value;
			
			active = (clickEvent != null && hoverText != null );
		}
	}

}