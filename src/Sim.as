package  
{
	import Types.*;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	import net.flashpunk.FP;
	
	/**
	 * Object that manages all aspects of the ongoing simulation
	 * 
	 * @author Switchbreak
	 */
	public class Sim 
	{
		
		// Embeds
		[Embed(source = '../xml/CountryNames.xml', mimeType = "application/octet-stream")] private static const COUNTRY_XML:Class;
		[Embed(source = '../xml/TownNames.xml', mimeType = "application/octet-stream")] private static const TOWN_XML:Class;
		
		// Season enumeration
		static public const SEASON_SPRING:int	= 0;
		static public const SEASON_SUMMER:int	= 1;
		static public const SEASON_FALL:int		= 2
		static public const SEASON_WINTER:int	= 3;
		
		// Constants
		static public const		FARM_PRICE:Number		= 500;
		static public const		MAP_WIDTH:int			= 640;
		static public const		MAP_HEIGHT:int			= 379;
		static public const		BRIBE_PRICE:Number		= 500;
		static private const	SEASON_COUNT:int		= 4;
		static private const	STARTING_MONEY:Number	= 1000;
		static private const	COUNTRY_COUNT:int		= 6;
		static private const	WAR_START_THRESHOLD:int	= SEASON_COUNT;
		static private const	WAR_RATE:Number			= 0.2;
		static private const	TOWN_CAPTURE_SPEED:int	= 2;
		static private const	TOWN_COUNT:int			= 3;
		static private const	BLOCKADE_RATE:Number	= 0.5;
		static private const	UPKEEP:Number			= 5.0;
		static private const	BASE_SEED_PRICE:Number	= 300;
		static private const	BASE_CROP_PRICE:Number	= 5;
		static private const	MARKUP:Number			= 1.5;
		static private const	BASE_YIELD:Number		= 60;
		static private const	BASE_TEND_YIELD:Number	= 40;
		static private const	YIELD_DEVIATION:Number	= 0.15;
		static private const	RESEED_QUANTITY:int		= 40;
		static private const	BASE_GROWTH_RATE:int	= 1;
		static private const	ESCAPE_RATE:Number		= 0.5;
		static private const	PLANT_FIELD_COUNT:int	= 7;
		
		// Public events
		public var EventFamilyDied:Function	= null;
		public var EventLost:Function		= null;
		public var EventEscape:Function		= null;
		
		// Read-only properties
		private var _crops:Vector.<Crop>;
		private var _countries:Vector.<Country>;
		private var _newsStories:Vector.<String>;
		private var _activeWars:Vector.<War>;
		private var _season:int;
		private var _country:Country;
		private var _farmer:Farmer;
		private var _map:Map;
		
		// Private variables
		private var turns:int = 0;
		
		/**
		 * Initialize simulation object
		 */
		public function Sim() 
		{
			//
			// Initialize all objects in the simulation
			//
			
			// Initialize all of the crop types
			_crops = new Vector.<Crop>();
			_crops.push( new Crop( ResourceManager.getInstance().getString( "resources", "PLANT_TYPE_CORN" ),			BASE_SEED_PRICE, BASE_CROP_PRICE, BASE_YIELD,		BASE_GROWTH_RATE,		false ) );
			_crops.push( new Crop( ResourceManager.getInstance().getString( "resources", "PLANT_TYPE_RICE" ),			BASE_SEED_PRICE, BASE_CROP_PRICE, BASE_YIELD,		BASE_GROWTH_RATE,		false ) );
			_crops.push( new Crop( ResourceManager.getInstance().getString( "resources", "PLANT_TYPE_RADISHES" ),		BASE_SEED_PRICE, BASE_CROP_PRICE, BASE_YIELD >> 1,	BASE_GROWTH_RATE << 1,	false ) );
			_crops.push( new Crop( ResourceManager.getInstance().getString( "resources", "PLANT_TYPE_WINTER_WHEAT" ),	BASE_SEED_PRICE, BASE_CROP_PRICE, BASE_YIELD,		BASE_GROWTH_RATE,		true ) );
			_crops.push( new Crop( ResourceManager.getInstance().getString( "resources", "PLANT_TYPE_WINTER_RYE" ),		BASE_SEED_PRICE, BASE_CROP_PRICE, BASE_YIELD,		BASE_GROWTH_RATE,		true ) );
			
			// Get list of country names from XML file
			var names:XML = new XML( new COUNTRY_XML );
			var countryNames:Vector.<String> = new Vector.<String>();
			for each( var name:XML in names.country )
			{
				countryNames.push( name );
			}
			
			// Get list of town names from XML file
			names = new XML( new TOWN_XML );
			var townNames:Vector.<String> = new Vector.<String>();
			for each( name in names.town )
			{
				townNames.push( name );
			}
			
			// Initialize all of the countries
			_countries = new Vector.<Country>();
			for ( var i:int = 0; i < COUNTRY_COUNT; i++ )
			{
				// Pick random name from list and remove it
				var nameIndex:int = FP.rand( countryNames.length );
				var newCountry:Country = new Country( countryNames[nameIndex], _crops.length );
				countryNames.splice( nameIndex, 1 );
				
				// Generate list of towns in the country
				for ( var j:int = 0; j < TOWN_COUNT; j++ )
				{
					// Pick random town name from list and remove it
					nameIndex = FP.rand( townNames.length );
					newCountry.AddTown( townNames[nameIndex] );
					townNames.splice( nameIndex, 1 );
				}
				
				_countries.push( newCountry );
			}
			
			// Create map
			_map = new Map( _countries, MAP_WIDTH, MAP_HEIGHT );
			
			// Set starting season to Spring
			_season = SEASON_SPRING;
			
			// Create a free farm in the starting country
			_country = _countries[0];
			_country.farm = new Farm();
			
			// Initialize the farmer and give him the starting money
			_farmer = new Farmer();
			_farmer.money = STARTING_MONEY;
			
			_newsStories = new Vector.<String>();						// Initialize news stories list
			_activeWars = new Vector.<War>();							// Initialize active wars list
		}
		
		/**
		 * Set the price modifier for all crop types
		 */
		private function RandomizeCropPrices():void
		{
			for each( var countryIter:Country in _countries )
				countryIter.SetCropPrices();
		}
		
		/**
		 * Move the simulation forward one turn
		 */
		public function AdvanceTurn():void
		{
			turns++;
			
			// Advance the season
			_season = (_season + 1) % SEASON_COUNT;
			
			// Clear news ticker
			_newsStories.length = 0;
			
			AdvanceWars();
			
			for each( var countryIter:Country in _countries )
			{
				GrowCrops( countryIter );
				StartWars( countryIter );
			}
			
			RandomizeCropPrices();
			
			ChargeFamilyUpkeep();
		}
		
		/**
		 * Advances all ongoing wars for the simulation when the turn changes
		 */
		private function AdvanceWars():void
		{
			// Run ongoing wars
			for each( var war:War in _activeWars )
			{
				// Age the war
				war.warLength++;
				
				// If a war in the farmer's home country has gone on for two
				// turns, trigger the escape minigame
				if ( _country.farm != null && war.invaded == _country )
				{
					if( FP.random <= ESCAPE_RATE && EventEscape != null )
						EventEscape();
				}
				
				// Come up with a random outcome for the next battle - three possibilities:
				// 1) Town lost - if the invaded country is 
				// 2) Stalemate
				// 3) Town gained
				var battle:int;
				
				// If the invaded country has lost its defensive forces, then no towns can be lost
				if( war.invaded.defending )
					battle = FP.rand( 4 );
				else
					battle = FP.rand( 3 ) + 1;
				
				if ( battle > 2 )
					battle = 2;
				
				// Advance war depending on the outcome of the battle
				switch( battle )
				{
					case 0:
						// Battle lost, retreat
						var town:Town = war.Retreat();
						
						// Broadcast news of the battle
						if ( war.ended )
							_newsStories.push( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "TICKER_WAR_LOST" ), war.invader.name, war.invaded.name ) );
						else
							_newsStories.push( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "TICKER_RETREAT" ), war.invader.name, town.name, war.invaded.name ) );
						
						break;
					case 1:
						// Stalemate, broadcast to news ticker
						_newsStories.push( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "TICKER_STANDOFF" ), war.invader.name, war.invaded.name ) );
						
						break;
					case 2:
						// Battle won, capture town
						town = war.CaptureTown();
						
						// Broadcast news of the battle
						if ( war.ended )
							_newsStories.push( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "TICKER_WAR_WON" ), war.invader.name, war.invaded.name ) );
						else
							_newsStories.push( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "TICKER_CAPTURE" ), war.invader.name, town.name, war.invaded.name ) );
						
						break;
				}
			}
		}
		
		/**
		 * Start wars at random
		 * 
		 * @param countryIter
		 * Country to start wars for
		 */
		private function StartWars( countryIter:Country ):void 
		{
			// Check that the first year has passed and the country has armies
			// to invade with
			if ( turns >= WAR_START_THRESHOLD && countryIter.defending )
			{
				// Roll to start a war
				if ( FP.random <= WAR_RATE )
				{
					// Choose country to invade at random
					var invaded:int = FP.rand( countryIter.adjacent.length );
					
					// Check that the invader is not invading itself, and that it is not already at war with the invaded country
					var bUnique:Boolean = false;
					var tries:int = 0;
					while ( !bUnique && tries < countryIter.adjacent.length )
					{
						// Check that we are not currently invading or being invaded by the selected country already
						bUnique = true;
						if ( countryIter.adjacent[invaded] == countryIter || countryIter.adjacent[invaded].occupiedBy == countryIter.occupiedBy )
							bUnique = false;
						else
						{
							for each( var warIter:War in countryIter.wars )
							{
								if ( warIter.invader == countryIter.adjacent[invaded] || warIter.invaded == countryIter.adjacent[invaded] )
									bUnique = false;
							}
						}
						
						if ( !bUnique )
						{
							invaded = (invaded + 1) % countryIter.adjacent.length;
							tries++;
						}
					}
					
					// If all criteria were met, invade
					if ( bUnique )
					{
						// Add war to list
						var war:War = new War( countryIter, countryIter.adjacent[invaded], this );
						var town:Town = war.CaptureTown();
						_activeWars.push( war );
						
						// Add invasion stories to the top of the news ticker
						_newsStories.unshift( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "TICKER_INVASION" ), war.invader.name, war.invaded.name, town.name ) );
					}
				}
			}
		}
		
		/**
		 * Grows all planted crops when the turn changes
		 * 
		 * @param countryIter
		 * Country to grow crops in
		 */
		private function GrowCrops( countryIter:Country ):void
		{
			// Check that the selected country has a farm
			if ( countryIter.farm != null )
			{
				// Grow each field on the farm
				for each( var field:Field in countryIter.farm.fields )
				{
					AgeField( field );
					UpdateFieldTile( field );
				}
				
				// Age all products in storage
				for each( var product:Product in countryIter.farm.silo )
				{
					if ( product != null )
					{
						product.storageTime++;
						
						// If the product has rotted (storage time = 1 year),
						// leave it in the storage spot but mark the space as
						// available
						if ( product.storageTime == Product.STORAGE_TIME_ROTTEN )
							countryIter.farm.storageSpace++;
					}
				}
			}
		}
		
		/**
		 * Age a field when the turn changes
		 * @param	field	Field to age
		 */
		private function AgeField( field:Field ):void
		{
			// Grow crops by their growth rate, wither when they
			// get too old
			field.age += _crops[field.plantType].growthRate;
			if ( field.age > Field.AGE_WITHERED )
				field.age = Field.AGE_WITHERED;
			
			// Standard crops will wither if we are either entering
			// or exiting a winter season
			if ( !_crops[field.plantType].winterCrop && (_season == SEASON_WINTER || _season == SEASON_SPRING) )
				field.age = Field.AGE_WITHERED;
			
			// Winter crops will wither if we are either entering or
			// exiting a summer season
			if ( _crops[field.plantType].winterCrop && (_season == SEASON_SUMMER || _season == SEASON_FALL) )
				field.age = Field.AGE_WITHERED;
		}
		
		/**
		 * Update the tile to display the current state of a field
		 * @param	field	Field to update
		 */
		private function UpdateFieldTile( field:Field ):void
		{
			switch( field.age )
			{
				case Field.AGE_SEED:
					_country.mapTiles[field.position.row * Country.TILE_MAP_WIDTH + field.position.column] = Country.TILE_SEED;
					break;
				case Field.AGE_SPROUT:
					if( !field.tended )
						_country.mapTiles[field.position.row * Country.TILE_MAP_WIDTH + field.position.column] = Country.TILE_SPROUT;
					else
						_country.mapTiles[field.position.row * Country.TILE_MAP_WIDTH + field.position.column] = Country.TILE_TENDED;
					break;
				case Field.AGE_RIPE:
					_country.mapTiles[field.position.row * Country.TILE_MAP_WIDTH + field.position.column] = Country.TILE_RIPE;
					break;
				default:
					_country.mapTiles[field.position.row * Country.TILE_MAP_WIDTH + field.position.column] = Country.TILE_WITHERED;
			}
		}
		
		/**
		 * Checks the silo at the current farm for any plantable seeds
		 * 
		 * @return
		 * True if plantable seeds found, false if not
		 */
		public function SeedsAvailable():Boolean
		{
			// Check that the current country has a farm
			if ( _country.farm == null )
				return false;
			
			// Search through silo to find all valid seeds
			for ( var i:int = 0; i < _country.farm.silo.length; i++ )
			{
				// Check if the current silo slot contains seeds that have not
				// rotted yet
				if ( _country.farm.silo[i] != null
					&& _country.farm.silo[i].type == Product.TYPE_SEED
					&& _country.farm.silo[i].storageTime < Product.STORAGE_TIME_ROTTEN )
				{
					return true;
				}
			}
			
			// No seeds found, return false
			return false;
		}
		
		/**
		 * Gets the price of a specified product in the current country
		 * 
		 * @param	productType
		 * Type of product (seed or crops)
		 * @param	plantType
		 * Type of plant
		 * @param markupType
		 * Type of product to apply markup to
		 * @return
		 * Returns the price of the product
		 */
		public function GetPrice( productType:int, plantType:int, markupType:int = -1, markdownType:int = -1 ):Number
		{
			var price:Number;
			
			// Get price by product type
			switch( productType )
			{
				case Product.TYPE_CROP:
					price = _crops[plantType].cropPrice;
					break;
				case Product.TYPE_SEED:
					price = _crops[plantType].seedPrice;
					break;
			}
			
			// Modify base price by country's crop price
			price += price * _country.cropPrices[plantType];
			
			// Apply markup/markdown if needed
			if ( productType == markupType )
				price *= MARKUP;
			if ( productType == markdownType )
				price /= MARKUP;
			
			// Floor at $1
			if ( price < 1 )
				price = 1;
			
			// Multiply by quantity and return
			return price;
		}
		
		/**
		 * Buys specified products from the market and puts them in storage
		 * 
		 * @param	products
		 * List of products to buy
		 * @return
		 * Returns true if products were bought, false if unable
		 */
		public function BuyProducts( products:Vector.<Product> ):Boolean
		{
			var totalPrice:Number = 0;
			var totalStorage:int = 0;
			
			// Get the total price and storage requirements of the products
			for each( var product:Product in products )
			{
				// Get product price and storage, apply markup for buying crops
				totalPrice += GetPrice( product.type, product.plantType, Product.TYPE_CROP ) * product.quantity;
				totalStorage += farm.GetStorageRequired( product );
			}
			
			// Check that we have enough money and storage space to buy products
			if ( totalPrice > farmer.money )
				return false;
			if ( totalStorage > farm.storageSpace )
				return false;
			
			// Loop through all products to buy
			for each( product in products )
			{
				// Attempt to store the product in the silo
				farm.StoreProduct( product );
			}
			
			// Deduct money from farmer's account
			farmer.money -= totalPrice;
			
			return true;
		}
		
		/**
		 * Sell products from storage
		 * 
		 * @param	products
		 * Indices of products in silo to sell
		 */
		public function SellProducts( products:Vector.<int> ):void
		{
			var totalPrice:Number = 0;
			
			// Total the price of all items to sell and remove from silo
			for ( var i:int = 0; i < products.length; i++ )
			{
				if ( _country.farm.silo[products[i]].storageTime < Product.STORAGE_TIME_ROTTEN )
				{
					totalPrice += GetPrice( _country.farm.silo[products[i]].type, _country.farm.silo[products[i]].plantType, -1, Product.TYPE_SEED ) * _country.farm.silo[products[i]].quantity;
				}
				
				DiscardStorage( products[i] );
			}
			
			// Deduct taxes from sale
			totalPrice -= totalPrice * _country.tax;
			
			// Give money from sale to farmer
			farmer.money += totalPrice;
		}
		
		/**
		 * Harvest crop from the specified field and store it in the silo
		 * @param	field	Field to harvest
		 * @return	Returns true if the field was harvested, false if not enough storage
		 */
		public function HarvestField( field:Field ):Boolean
		{
			// Create new product from field crop
			var product:Product = new Product();
			product.type = Product.TYPE_CROP;
			product.plantType = field.plantType;
			product.quantity = field.yield;
			product.storageTime = Product.STORAGE_TIME_FRESH;
			
			// If storing the new product fails, return failure to harvest
			if ( !_country.farm.StoreProduct( product ) )
				return false;
			
			// Harvest successful, set field to empty
			ClearField( field );
			
			return true;
		}
		
		/**
		 * Clears a field of all crops
		 * @param	field	Field to clear
		 */
		public function ClearField( field:Field ):void
		{
			GameWorld.sim.farm.fields.splice( GameWorld.sim.farm.fields.indexOf( field, 0 ), 1 );
			_country.mapTiles[field.position.row * Country.TILE_MAP_WIDTH + field.position.column] = Country.TILE_FARM;
		}
		
		/**
		 * Plants a field with seeds from the storage silo
		 * @param	seedIndex		Index of storage silo slot containing seeds
		 * 							to plant
		 * @param	fieldPosition	Position to plant the field at
		 */
		public function PlantField( seedIndex:int, fieldPosition:MapPosition ):void
		{
			// Check that the storage slot has plantable seeds
			if ( _country.farm.silo[seedIndex] == null )
				return;
			if ( _country.farm.silo[seedIndex].type != Product.TYPE_SEED || _country.farm.silo[seedIndex].storageTime >= Product.STORAGE_TIME_ROTTEN )
				return;
			
			// Add field tiles to all adjacent spaces
			var upper:int = 1;
			if ( fieldPosition.column % 2 == 1 )
				upper = -1;
			
			AddFieldTile( fieldPosition.column,		fieldPosition.row + upper,	seedIndex );
			AddFieldTile( fieldPosition.column - 1,	fieldPosition.row,			seedIndex );
			AddFieldTile( fieldPosition.column + 1,	fieldPosition.row,			seedIndex );
			AddFieldTile( fieldPosition.column,		fieldPosition.row,			seedIndex );
			AddFieldTile( fieldPosition.column,		fieldPosition.row - upper,	seedIndex );
			AddFieldTile( fieldPosition.column - 1,	fieldPosition.row - upper,	seedIndex );
			AddFieldTile( fieldPosition.column + 1,	fieldPosition.row - upper,	seedIndex );
			
			DiscardStorage( seedIndex );
		}
		
		/**
		 * Add individual field tiles for planting
		 * @param	column		Position of the field tile
		 * @param	row			Position of the field tile
		 * @param	seedIndex	Index of the seed being planted in the silo
		 */
		private function AddFieldTile( column:int, row:int, seedIndex:int ):void
		{
			if ( row < 0 || row > Country.TILE_MAP_HEIGHT || column < 0 || column > Country.TILE_MAP_WIDTH )
				return;
			if ( _country.mapTiles[row * Country.TILE_MAP_WIDTH + column] != Country.TILE_FARM )
				return;
			
			// Generate random yield modifier
			var yieldModifier:Number = MathHelper.BoxMuller() * YIELD_DEVIATION;
			
			// Create new field object with crop from storage
			var field:Field = new Field();
			field.plantType = _country.farm.silo[seedIndex].plantType;
			field.age = Field.AGE_SEED;
			field.tended = false;
			field.yield = (_crops[_country.farm.silo[seedIndex].plantType].yield + _crops[_country.farm.silo[seedIndex].plantType].yield * yieldModifier) / PLANT_FIELD_COUNT;
			field.position = new MapPosition( row, column );
			
			// Add field to farm
			_country.mapTiles[row * Country.TILE_MAP_WIDTH + column] = Country.TILE_SEED;
			_country.farm.fields.push( field );
		}
		
		/**
		 * Charge upkeep cost for living family members
		 */
		private function ChargeFamilyUpkeep():void 
		{
			// Add up total upkeep for the farmer, the spouse, and all children
			var totalUpkeep:Number = UPKEEP;
			if ( farmer.spouse != null )
				totalUpkeep += UPKEEP;
			totalUpkeep += UPKEEP * farmer.children.length;
			
			// If there is not enough money to pay it, then characters will
			// starve
			while ( totalUpkeep > farmer.money )
			{
				KillCharacter();
				totalUpkeep -= UPKEEP;
			}
			farmer.money -= totalUpkeep;
		}
		
		/**
		 * Tend a field in the sprout stage
		 * @param	field	Field to tend
		 * @return	Returns true if field was tended, false if it failed
		 */
		public function TendField( field:Field ):Boolean
		{
			if( field == null || field.age != Field.AGE_SPROUT || field.tended )
				return false;
			
			// Generate random yield modifier
			var yieldModifier:Number = MathHelper.BoxMuller() * YIELD_DEVIATION;
			
			// Set tended flag and add new yield to existing yield
			field.tended = true;
			field.yield += (BASE_TEND_YIELD + BASE_TEND_YIELD * yieldModifier) / PLANT_FIELD_COUNT;
			_country.mapTiles[field.position.row * Country.TILE_MAP_WIDTH + field.position.column] = Country.TILE_TENDED;
			
			return true;
		}
		
		/**
		 * Removes an item from the storage silo and discards it
		 * 
		 * @param	itemIndex
		 * Index of item to remove
		 */
		public function DiscardStorage( itemIndex:int ):void
		{
			// If the silo slot is occupied, reclaim storage space
			if( _country.farm.silo[itemIndex] != null && _country.farm.silo[itemIndex].storageTime < Product.STORAGE_TIME_ROTTEN )
				_country.farm.storageSpace++;
			
			// Empty the silo slot
			_country.farm.silo[itemIndex] = null;
		}
		
		/**
		 * Convert crops in storage to seeds
		 * 
		 * @param	itemIndex
		 * Index of crops to reseed
		 * @return
		 * Returns true if item was reseeded, false if reseeding failed
		 */
		public function Reseed( itemIndex:int ):Boolean
		{
			// Check that we have room to store the seeds
			if ( _country.farm.storageSpace == 0 )
				return false;
			
			// Check that the specified storage slot has enough crops to reseed
			if ( _country.farm.silo[itemIndex] == null
				|| _country.farm.silo[itemIndex].type != Product.TYPE_CROP
				|| _country.farm.silo[itemIndex].storageTime >= Product.STORAGE_TIME_ROTTEN
				|| _country.farm.silo[itemIndex].quantity < RESEED_QUANTITY )
			{
				return false;
			}
			
			// Remove the amount needed to produce seeds from storage
			_country.farm.silo[itemIndex].quantity -= RESEED_QUANTITY;
			
			// Create new seed product from crop
			var product:Product = new Product();
			product.plantType = GameWorld.sim.farm.silo[itemIndex].plantType;
			product.quantity = 1;
			product.storageTime = Product.STORAGE_TIME_FRESH;
			product.type = Product.TYPE_SEED;
			
			// If the crop was all used up reseeding, free up the storage slot
			if ( _country.farm.silo[itemIndex].quantity <= 0 )
				DiscardStorage( itemIndex );
			
			// Store seed product in silo
			_country.farm.StoreProduct( product );
			
			return true;
		}
		
		/**
		 * Buy a farm on the current country
		 */
		public function BuyFarm():void
		{
			if ( _farmer.money < FARM_PRICE )
				return;
			
			_country.farm = new Farm();
			_farmer.money -= FARM_PRICE;
		}
		
		/**
		 * Destroy a farm on the current country
		 */
		public function DestroyFarm():void
		{
			_country.farm = null;
		}
		
		/**
		 * Move the farmer to a new country
		 * 
		 * @param	destIndex
		 * Index of the new country in the list of countries
		 */
		public function MoveCountry( destCountry:Country, BlockadeCallback:Function ):void
		{
			var blockade:Boolean = false;
			
			// Check if the border is closed on the country you are leaving, if
			// so, random chance of running into a blockade
			if ( (_country.wars.length > 0 || destCountry.wars.length > 0)
				&& _country.occupiedBy != destCountry.occupiedBy
				&& FP.random > BLOCKADE_RATE )
			{
				BlockadeCallback( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "DIALOG_BLOCKADE_LEAVE" ), _country.name ) );
				blockade = true;
			}
			
			_country = destCountry;
		}
		
		/**
		 * Kills one character in the farmer's family in the order:
		 * 1. Children (last to first)
		 * 2. Spouse
		 * 3. Farmer
		 */
		public function KillCharacter():void
		{
			if ( farmer.children.length > 0 )
			{
				var deceasedName:String = farmer.children.pop();
				if ( EventFamilyDied != null )
					EventFamilyDied( deceasedName );
			}
			else if ( farmer.spouse != null )
			{
				deceasedName = farmer.spouse;
				farmer.spouse = null;
				if ( EventFamilyDied != null )
					EventFamilyDied( deceasedName );
			}
			else
			{
				deceasedName = farmer.name;
				farmer.name = null;
				
				if ( EventFamilyDied != null )
					EventFamilyDied( deceasedName )
				if ( EventLost != null )
					EventLost();
			}
		}
		
		/**
		 * Removes a war from the list of active wars
		 * 
		 * @param	war
		 * War to remove
		 */
		public function RemoveWar( war:War ):void
		{
			_activeWars.splice( _activeWars.indexOf( war ), 1 );
		}
		
		/**
		 * Pay a bribe to avoid a blockade
		 */
		public function Bribe():void
		{
			if ( farmer.money >= BRIBE_PRICE )
				farmer.money -= BRIBE_PRICE;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets list of all crop types
		 */
		public function get crops():Vector.<Crop>
		{
			return _crops;
		}
		
		/**
		 * Gets list of all countries
		 */
		public function get countries():Vector.<Country>
		{
			return _countries;
		}
		
		/**
		 * Gets list of all current news stories
		 */
		public function get newsStories():Vector.<String>
		{
			return _newsStories;
		}
		
		/**
		 * Gets list of all active wars
		 */
		public function get activeWars():Vector.<War>
		{
			return _activeWars;
		}
		
		/**
		 * Gets current season
		 */
		public function get season():int
		{
			return _season;
		}
		
		/**
		 * Gets farm object
		 */
		public function get farm():Farm
		{
			return _country.farm;
		}
		
		/**
		 * Gets country object
		 */
		public function get country():Country
		{
			return _country;
		}
		
		/**
		 * Gets farmer object
		 */
		public function get farmer():Farmer
		{
			return _farmer;
		}
		
		/**
		 * Gets map object
		 */
		public function get map():Map
		{
			return _map;
		}
		
	}

}