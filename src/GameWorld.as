package  
{
	import flash.geom.Point;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.utils.*;
	import net.flashpunk.World;
	import Types.*;
	import UI.*;
	
	/**
	 * Main game world object
	 * 
	 * @author Switchbreak
	 */
	
	[ResourceBundle("resources")]
	public class GameWorld extends World 
	{
		// Embeds
		[Embed(source = '../font/OpenSans-Regular.ttf', embedAsCFF = 'false', fontFamily = 'UI Font')] private static const UI_FONT:Class;
		[Embed(source = '../xml/Names.xml', mimeType = "application/octet-stream")] private static const NAMES_XML:Class;
		
		// Constants
		static private const	STARTING_CHILDREN:int	= 3;
		static public const		CAMERA_LEFT:int			= 16;
		static public const		CAMERA_TOP:int			= 16;
		static public const		CAMERA_RIGHT:int		= 1920;
		static public const		CAMERA_BOTTOM:int		= 1280;
		static public const		HUD_LAYER:int			= int.MIN_VALUE;
		
		// Game states
		private static const STATE_NONE:int			= -1;
		private static const STATE_FARM_SCREEN:int	= 0;
		private static const STATE_DIALOG:int		= 1;
		
		// Statics
		private static var _sim:Sim;
		private static var _gameWorld:GameWorld;
		
		// Private variables
		private var _gameState:int = STATE_NONE;
		
		// Graphics
		private var	dialogQueue:Vector.<Entity>;
		private var	farmHUD:FarmHUD;
		public var	farmMap:FarmMap;
		private var	farmer:FarmerAvatar;
		
		/**
		 * Initialize game world
		 */
		public function GameWorld()
		{
			FP.randomizeSeed();
			_gameWorld = this;
			
			// Set default UI font
			Text.font = "UI Font";
			
			// Initialize simulation
			_sim = new Sim();
			_sim.EventFamilyDied = EventDeath;
			_sim.EventLost = EventLost;
			_sim.EventEscape = EventEscape;
			
			farmMap = new FarmMap( _sim.country );
			add( farmMap );
			
			var farmerStart:Point = farmMap.farmTiles.GetPositionAtTile( _sim.country.startingPoint.row + 1, _sim.country.startingPoint.column );
			
			farmer = new FarmerAvatar( farmerStart.x, farmerStart.y, EventPlant, EventTend, EventHarvest, EventClear );
			add( farmer );
			CameraFollow( farmer.x, farmer.y );
			
			// Initialize main farm screen UI elements
			_gameState = STATE_FARM_SCREEN;
			farmHUD = new FarmHUD();
			farmHUD.eventMap = EventMapDisplay;
			farmHUD.eventSilo = EventFarmDisplay;
			farmHUD.eventNext = EventNextTurn;
			farmHUD.eventBuy = EventBuyDisplay;
			farmHUD.eventSell = EventSellDisplay;
			add( farmHUD );
			
			// Load in big list of available names for random name generation
			var availableNames:Vector.<String> = new Vector.<String>();
			var names:XML = new XML( new NAMES_XML );
			var countryNames:Vector.<String> = new Vector.<String>();
			for each( var name:XML in names.name )
			{
				availableNames.push( name );
			}
			
			// Set farmer and family names
			var nameIndex:int = FP.rand( availableNames.length );
			var farmerName:String = availableNames[nameIndex];
			availableNames.splice( nameIndex, 1 );
			
			nameIndex = FP.rand( availableNames.length );
			var spouseName:String = availableNames[nameIndex];
			availableNames.splice( nameIndex, 1 );
			
			var childNames:Vector.<String> = new Vector.<String>();
			for ( var i:int = 0; i < STARTING_CHILDREN; i++ )
			{
				nameIndex = FP.rand( availableNames.length );
				childNames.push( availableNames[nameIndex] );
				availableNames.splice( nameIndex, 1 );
			}
			
			// Initialize dialog queue
			dialogQueue = new Vector.<Entity>();
			
			// Set the dialogue queue to show Farmer, Spouse and Child name entry boxes then go to the farm screen
			AddDialog( new NameEntry( ResourceManager.getInstance().getString( "resources", "ENTER_FARMER_NAME" ), EventFarmerNameSet, Vector.<String>( [farmerName] ) ) );
			AddDialog( new NameEntry( ResourceManager.getInstance().getString( "resources", "ENTER_SPOUSE_NAME" ), EventSpouseNameSet, Vector.<String>( [spouseName] ) ) );
			AddDialog( new NameEntry( ResourceManager.getInstance().getString( "resources", "ENTER_CHILD_NAMES" ), EventChildNamesSet, childNames, STARTING_CHILDREN ) );
		}
		
		/**
		 * Updates game state once per frame
		 */
		override public function update():void 
		{
			switch( _gameState )
			{
				case STATE_FARM_SCREEN:
					// Super secret cheat code
					if ( Input.check( Key.DIGIT_1 ) )
					{
						sim.farmer.money += 1000;
						farmHUD.UpdateHUD();
					}
					
					CameraFollow( farmer.x, farmer.y );
					var position:MapPosition = farmMap.farmTiles.GetTileAtPosition( farmer.x, farmer.y );
					
					if ( Input.check( Key.DIGIT_2 ) )
						farmMap.farmTiles.setTile( position.column, position.row, 6 );
					if ( Input.check( Key.DIGIT_3 ) )
						farmMap.farmTiles.setTile( position.column, position.row, 7 );
					if ( Input.check( Key.DIGIT_4 ) )
						farmMap.farmTiles.setTile( position.column, position.row, 8 );
					if ( Input.check( Key.DIGIT_5 ) )
						farmMap.farmTiles.setTile( position.column, position.row, 9 );
					
					break;
			}
			
			super.update();
		}
		
		/**
		 * Follow a point with the camera
		 * @param	x	Position to center on
		 * @param	y	Position to center on
		 */
		private function CameraFollow( x:Number, y:Number ):void
		{
			camera.x = Math.round( x - FP.halfWidth );
			camera.y = Math.round( y - FP.halfHeight );
			
			if ( camera.x < CAMERA_LEFT )
				camera.x = CAMERA_LEFT;
			if ( camera.y < CAMERA_TOP )
				camera.y = CAMERA_TOP;
			if ( camera.x + FP.width > CAMERA_RIGHT )
				camera.x = CAMERA_RIGHT - FP.width;
			if ( camera.y + FP.height - 103 > CAMERA_BOTTOM )
				camera.y = CAMERA_BOTTOM - FP.height + 103;
		}
		
		/**
		 * Gets the field under the current world coordinates if any
		 * @param	worldX		World coordinates to check for field at
		 * @param	worldY		World coordinates to check for field at
		 * @return	Field at position, or null if none found
		 */
		public function GetFieldAtLocation( worldX:int, worldY:int ):Field
		{
			var position:MapPosition = farmMap.farmTiles.GetTileAtPosition( worldX, worldY );
			
			for each( var field:Field in _sim.farm.fields )
			{
				if ( field.position.row == position.row && field.position.column == position.column )
					return field;
			}
			
			return null;
		}
		
		/**
		 * Go to the next dialog box in the dialog queue
		 */
		public function NextDialog():void
		{
			if ( dialogQueue.length > 0 )
			{
				if ( _gameState != STATE_DIALOG )
				{
					gameState = STATE_DIALOG;
				}
				
				add( dialogQueue.shift() );
			}
			else
				gameState = STATE_FARM_SCREEN;
		}
		
		/**
		 * Add a dialog box to the queue, and jump to it if there are none in
		 * front.
		 * 
		 * @param	dialog
		 * Dialog box entity to add
		 */
		public function AddDialog( dialog:Entity ):void
		{
			dialogQueue.push( dialog );
			
			if ( _gameState != STATE_DIALOG )
				NextDialog();
		}
		
		//
		// Events
		//
		
		/**
		 * The farmer's name entry box calls this event when the name is done being entered
		 * 
		 * @param	farmerName
		 * Farmer name entered
		 */
		public function EventFarmerNameSet( farmerName:String ):void
		{
			// If no name was entered, pick a name
			_sim.farmer.name = farmerName;
			
			farmHUD.UpdateHUD();
			
			NextDialog();
		}
		
		/**
		 * The spouse name entry box calls this event when the spouse's name is done being entered
		 * 
		 * @param	spouseName
		 * Spouse name entered
		 */
		public function EventSpouseNameSet( spouseName:String ):void
		{
			// If no name was entered, pick a name
			_sim.farmer.spouse = spouseName;
			
			farmHUD.UpdateHUD();
			
			NextDialog();
		}
		
		/**
		 * The child names entry box calls this event when the children's names are done being entered
		 * 
		 * @param	childNames
		 * List of child names entered
		 */
		public function EventChildNamesSet( childNames:Vector.<String> ):void
		{
			// If no name was entered, pick a name
			_sim.farmer.children = childNames;
			
			farmHUD.UpdateHUD();
			
			NextDialog();
		}
		
		/**
		 * The "Farm" button on the HUD calls this function when clicked
		 */
		public function EventFarmDisplay():void
		{
			AddDialog( new FarmInfoScreen( EventFarmClosed ) );
		}
		
		/**
		 * The Farm Info dialog calls this function when it closes
		 */
		public function EventFarmClosed():void
		{
			farmHUD.UpdateHUD();
			NextDialog();
		}
		
		/**
		 * The "Buy" button on the HUD calls this function when clicked
		 */
		public function EventBuyDisplay():void
		{
			AddDialog( new BuyScreen( EventBuy, NextDialog ) );
		}
		
		/**
		 * The Buy screen calls here if any products are bought
		 */
		public function EventBuy():void
		{
			farmHUD.UpdateHUD();
			NextDialog();
		}
		
		/**
		 * Plants a field with a seed selected by the PlantScreen dialog
		 */
		public function EventPlant( x:int, y:int, seedIndex:int ):void
		{
			// Plant the field
			var fieldPosition:MapPosition = farmMap.farmTiles.GetTileAtPosition( x, y );
			sim.PlantField( seedIndex, fieldPosition );
			
			// Finish event
			farmMap.UpdateMap();
			farmHUD.UpdateHUD();
			NextDialog();
		}
		
		/**
		 * The "Tend" button on the HUD calls this function when clicked
		 */
		public function EventTend( x:int, y:int ):void
		{
			// Tend the field
			sim.TendField( GetFieldAtLocation( x, y ) );
			
			// Finish event
			farmMap.UpdateMap();
			farmHUD.UpdateHUD();
			NextDialog();
		}
		
		/**
		 * A field was harvested
		 */
		public function EventHarvest( x:int, y:int ):void
		{
			// Harvest the field
			sim.HarvestField( GetFieldAtLocation( x, y ) );
			
			// Finish event
			farmMap.UpdateMap();
			farmHUD.UpdateHUD();
			NextDialog();
		}
		
		/**
		 * A field was cleared
		 */
		public function EventClear( x:int, y:int ):void
		{
			// Harvest the field
			sim.ClearField( GetFieldAtLocation( x, y ) );
			
			// Finish event
			farmMap.UpdateMap();
			farmHUD.UpdateHUD();
			NextDialog();
		}
		
		/**
		 * The "Sell" button on the HUD calls this function when clicked
		 */
		public function EventSellDisplay():void
		{
			AddDialog( new SellScreen( EventSell, NextDialog ) );
		}
		
		/**
		 * The Sell screen calls here if any products are sold
		 */
		public function EventSell():void
		{
			farmHUD.UpdateHUD();
			NextDialog();
		}
		
		/**
		 * The "Map" button on the HUD calls this function when clicked
		 */
		public function EventMapDisplay():void
		{
			AddDialog( new MapScreen( false, EventMove, EventMapNext ) );
		}
		
		/**
		 * The "Next" button on the map screen calls this function when clicked
		 */
		public function EventMapNext():void
		{
			NextDialog();
			
			EventNextTurn();
			AddDialog( new MapScreen( false, EventMove, EventMapNext ) );
		}
		
		/**
		 * The Map screen calls here if the player moves countries
		 */
		public function EventMove():void
		{
			farmHUD.UpdateHUD();
			farmMap.LoadCountryMap( sim.country );
			
			var farmerStart:Point = farmMap.farmTiles.GetPositionAtTile( _sim.country.startingPoint.row + 1, _sim.country.startingPoint.column );
			farmer.x = farmerStart.x;
			farmer.y = farmerStart.y;
			CameraFollow( farmer.x, farmer.y );
			
			NextDialog();
			
			if ( sim.country.farm == null && sim.farmer.money >= Sim.FARM_PRICE )
			{
				var buyFarmDialog:DialogBox = new DialogBox( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "DIALOG_BUY_FARM" ), Sim.FARM_PRICE.toFixed( 2 ) ) );
				buyFarmDialog.AddButton( ResourceManager.getInstance().getString( "resources", "BUY_BUTTON" ), EventBuyFarm );
				buyFarmDialog.AddButton( ResourceManager.getInstance().getString( "resources", "CANCEL_BUTTON" ), NextDialog );
				AddDialog( buyFarmDialog );
			}
		}
		
		/**
		 * The Buy Farm dialog calls here if the player buys a farm
		 */
		public function EventBuyFarm():void
		{
			sim.BuyFarm();
			farmHUD.UpdateHUD();
			farmMap.UpdateMap();
			
			NextDialog();
		}
		
		/**
		 * The "Next" button on the HUD calls this function when clicked
		 */
		public function EventNextTurn():void
		{
			_sim.AdvanceTurn();
			
			farmMap.UpdateMap();
			farmHUD.UpdateHUD();
			farmHUD.ResetTicker();
			
			// If there are active wars, jump to the map screen
			if ( _sim.activeWars.length > 0 )
				AddDialog( new MapScreen( true, NextDialog, NextDialog ) );
		}
		
		/**
		 * This function is called to inform the player that a family member has
		 * died
		 */
		public function EventDeath( deceasedName:String ):void
		{
			farmHUD.UpdateHUD();
			
			var deathDialog:DialogBox = new DialogBox( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "DIALOG_FAMILY_DIED" ), deceasedName ) );
			deathDialog.AddButton( ResourceManager.getInstance().getString( "resources", "OK_BUTTON" ), NextDialog );
			AddDialog( deathDialog );
		}
		
		/**
		 * This function is called when the player has lost
		 */
		public function EventLost():void
		{
			var lostDialog:DialogBox = new DialogBox( ResourceManager.getInstance().getString( "resources", "DIALOG_LOST" ) );
			AddDialog( lostDialog );
		}
		
		/**
		 * This function is called when the player has won the Escape minigame
		 */
		public function EventEscape():void
		{
			// Go to the escape minigame
			//if( sim.farm != null )
				//FP.world = new Escape( sim, farmHUD.farmer.x, farmHUD.farmer.y, EventEscapeWon );
		}
		
		/**
		 * This function is called when the player has won the Escape minigame
		 */
		public function EventEscapeWon():void
		{
			// Destroy the farm the player is escaping from
			sim.DestroyFarm();
			
			// Clear out any dialog boxes in the queue and go to the map screen
			dialogQueue.length = 0;
			AddDialog( new MapScreen( false, EventMove, EventMapNext ) );
		}
		
		//
		// Properties
		//
		
		/**
		 * Sets the game state. All code that needs to run to initialize a game state should go here.
		 */
		private function set gameState( setGameState:int ):void
		{
			_gameState = setGameState;
			
			switch( _gameState )
			{
				case STATE_FARM_SCREEN:
					farmHUD.enabled = true;
					farmer.enabled = true;
					break;
				case STATE_DIALOG:
					farmHUD.enabled = false;
					farmer.enabled = false;
					break;
			}
		}
		
		/**
		 * Gets the simulation object
		 */
		public static function get sim():Sim
		{
			return _sim;
		}
		
		/**
		 * Gets the gameworld object
		 */
		public static function get gameWorld():GameWorld
		{
			return _gameWorld;
		}
		
	}

}