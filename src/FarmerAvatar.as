package  
{
	import flash.geom.Point;
	import mx.utils.StringUtil;
	import mx.resources.ResourceManager;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.utils.Draw;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import Types.Farm;
	import Types.MapPosition;
	import Types.Product;
	import Types.Country;
	
	/**
	 * Player controllable farmer avatar
	 * @author Switchbreak
	 */
	public class FarmerAvatar extends Entity 
	{
		// Assets
		[Embed(source = "../img/Farmer.png")] private static const FARMER:Class;
		
		// Constants
		static private const MOVEMENT_SPEED:Number	= 100;
		static private const CONTEXT_ACTION_KEY:int	= Key.SPACE;
		static private const EPSILON:Number			= 1;
		static private const COLLISION_TRIES:int	= 1000;
		static private const FARMER_WIDTH:int		= 28;
		static private const FARMER_HEIGHT:int		= 64;
		
		// Context actions
		static public const CONTEXT_ACTION_NONE:int		= 0;
		static public const CONTEXT_ACTION_PLANT:int	= 1;
		static public const CONTEXT_ACTION_TEND:int		= 2;
		static public const CONTEXT_ACTION_HARVEST:int	= 3;
		static public const CONTEXT_ACTION_CLEAR:int	= 4;
		
		// Variables
		private var	farmer:Spritemap;
		private var contextLabel:Text;
		private var eventPlant:Function;
		private var eventTend:Function;
		private var eventHarvest:Function;
		private var eventClear:Function;
		private var seedIndex:int	= 0;
		public var	enabled:Boolean	= false;
		
		/**
		 * Create and initialize the farmer object
		 * @param	setX				Starting position of the farmer
		 * @param	setY				Starting position of the farmer
		 * @param	setEventPlant		Callback on plant event
		 * @param	setEventTend		Callback on tend event
		 * @param	setEventHarvest		Callback on harvest event
		 * @param	setEventClear		Callback on clear event
		 */
		public function FarmerAvatar( setX:int, setY:int, setEventPlant:Function, setEventTend:Function, setEventHarvest:Function, setEventClear:Function ) 
		{
			eventPlant		= setEventPlant;
			eventTend		= setEventTend;
			eventHarvest	= setEventHarvest;
			eventClear		= setEventClear;
			
			farmer = new Spritemap( FARMER, FARMER_WIDTH, FARMER_HEIGHT );
			farmer.originX	= farmer.width >> 1;
			farmer.originY	= farmer.height;
			
			contextLabel = new Text( " " );
			contextLabel.y			= -farmer.height - contextLabel.height;
			contextLabel.visible	= false;
			
			super( setX, setY, new Graphiclist( farmer, contextLabel ) );
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			if ( enabled )
			{
				HandleMovement();
				HandleContextAction();
			}
			
			super.update();
		}
		
		/**
		 * Handle movement input controls
		 */
		private function HandleMovement():void 
		{
			var dest:Point = new Point( x, y );
			
			if ( Input.check( Key.UP ) )
			{
				dest.y -= MOVEMENT_SPEED * FP.elapsed;
			}
			if ( Input.check( Key.DOWN ) )
			{
				dest.y += MOVEMENT_SPEED * FP.elapsed;
				farmer.frame = 0;
			}
			if ( Input.check( Key.LEFT ) )
			{
				dest.x -= MOVEMENT_SPEED * FP.elapsed;
				farmer.frame = 1;
				farmer.flipped = true;
			}
			if ( Input.check( Key.RIGHT ) )
			{
				dest.x += MOVEMENT_SPEED * FP.elapsed;
				farmer.frame = 1;
				farmer.flipped = false;
			}
			
			ResolveAllCollisions( dest );
			layer = -dest.y;
			
			x = dest.x;
			y = dest.y;
		}
		
		/**
		 * Continue resolving collisions until none are left
		 * @param	dest	Destination point
		 */
		private function ResolveAllCollisions( dest:Point ):void
		{
			var sourcePos:MapPosition = (world as GameWorld).farmMap.farmTiles.GetTileAtPosition( x, y );
			
			var count:int = 0;
			while ( CollisionCheck( sourcePos, dest ) )
			{
				if ( ++count > COLLISION_TRIES )
					return;
			}
		}
		
		/**
		 * Check for collisions in movement
		 * @param	dest	Destination coordinates
		 * @return	True if collision found and resolved, false otherwise
		 */
		private function CollisionCheck( sourcePos:MapPosition, dest:Point ):Boolean
		{
			if ( ScreenEdgeCheck( dest ) )
			{
				return true;
			}
			
			var destPos:MapPosition = (world as GameWorld).farmMap.farmTiles.GetTileAtPosition( dest.x, dest.y );
			
			if ( sourcePos.row == destPos.row && sourcePos.column == destPos.column )
				return false;
			
			if ( !IsTileWalkable( destPos ) )
			{
				dest = ResolveCollision( sourcePos, dest, destPos );
				return true;
			}
			
			return false;
		}
		
		/**
		 * Check for collisions against the edge of the screen in movement
		 * @param	dest	Destination coordinates
		 * @return	True if collision found and resolved, false otherwise
		 */
		private function ScreenEdgeCheck( dest:Point ):Boolean
		{
			var collisionFound:Boolean = false;
			
			if ( dest.x < GameWorld.CAMERA_LEFT )
			{
				dest.x = GameWorld.CAMERA_LEFT;
				collisionFound = true;
			}
			if ( dest.y - FARMER_HEIGHT < GameWorld.CAMERA_TOP )
			{
				dest.y = GameWorld.CAMERA_TOP + FARMER_HEIGHT;
				collisionFound = true;
			}
			if ( dest.x > GameWorld.CAMERA_RIGHT )
			{
				dest.x = GameWorld.CAMERA_RIGHT;
				collisionFound = true;
			}
			if ( dest.y > GameWorld.CAMERA_BOTTOM )
			{
				dest.y = GameWorld.CAMERA_BOTTOM;
				collisionFound = true;
			}
			
			return collisionFound;
		}
		
		/**
		 * Check if a given map tile type is walkable
		 * @param	tileType	Type of tile to check
		 * @return	True if walkable
		 */
		private function IsTileWalkable( destPos:MapPosition ):Boolean
		{
			if( destPos.row < 0 || destPos.row >= Country.TILE_MAP_HEIGHT ||
				destPos.column < 0 || destPos.column >= Country.TILE_MAP_WIDTH )
			{
				return false;
			}
			
			var tile:int = GameWorld.sim.country.mapTiles[destPos.row * Country.TILE_MAP_WIDTH + destPos.column];
			if ( Country.WALKABLE_TILES.indexOf( tile ) > -1 )
				return true;
			if ( GameWorld.sim.farm == null && Country.BUILDING_TILES.indexOf( tile ) > -1 )
				return true;
			
			return false;
		}
		
		/**
		 * Resolve a collision with an adjacent hex by finding the boundary line
		 * and projecting the movement along the intersection with it
		 * @param	dest		Destination coordinates
		 * @param	destPos		Destination map position
		 * @return	Resolved destination coordinates
		 */
		private function ResolveCollision( sourcePos:MapPosition, dest:Point, destPos:MapPosition ):Point
		{
			var p1:Point = new Point();
			var p2:Point = new Point();
			
			(world as GameWorld).farmMap.GetAdjacentBorder( sourcePos, destPos, p1, p2 );
			
			// Get the following vectors:
			// - a: vector from line point 1 to line point 2
			// - b: vector from line point 1 to motion end point
			var a:Point = new Point( p2.x - p1.x, p2.y - p1.y );
			var b:Point = new Point( dest.x - p1.x, dest.y - p1.y );
			
			// Project b onto a for new destination point
			var v:Number = (a.x * b.x + a.y * b.y) / (a.x * a.x + a.y * a.y);
			
			// Set new target point on the line at scalar position v
			dest.x = p1.x + a.x * v;
			dest.y = p1.y + a.y * v;
			
			var normal:Point = new Point( -(a.y / a.length) * EPSILON, (a.x / a.length) * EPSILON );
			dest.x -= normal.x;
			dest.y -= normal.y;
			
			return dest;
		}
		
		/**
		 * Perform a context action, based on the tile you're standing on
		 * @param	contextAction	Action for the current tile
		 */
		private function HandleContextAction():void 
		{
			var contextAction:int = GetContextAction( x, y );
			
			contextLabel.visible = false;
			switch( contextAction )
			{
				case CONTEXT_ACTION_PLANT:
					var seed:Product = ChooseSeeds();
					
					if ( seed != null )
					{
						ShowContextLabel( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "CONTEXT_ACTION_PLANT" ), GameWorld.sim.crops[seed.plantType].cropName ) );
						
						if ( Input.pressed( Key.Z ) )
							NextSeed( -1 );
						if ( Input.pressed( Key.X ) )
							NextSeed( 1 );
						if ( Input.pressed( CONTEXT_ACTION_KEY ) )
							eventPlant( x, y, seedIndex );
					}
					break;
				case CONTEXT_ACTION_TEND:
					ShowContextLabel( ResourceManager.getInstance().getString( "resources", "CONTEXT_ACTION_TEND" ) );
					
					if ( Input.pressed( CONTEXT_ACTION_KEY ) )
						eventTend( x, y );
					
					break;
				case CONTEXT_ACTION_HARVEST:
					ShowContextLabel( ResourceManager.getInstance().getString( "resources", "CONTEXT_ACTION_HARVEST" ) );
					
					if ( Input.pressed( CONTEXT_ACTION_KEY ) )
						eventHarvest( x, y );
					
					break;
				case CONTEXT_ACTION_CLEAR:
					ShowContextLabel( ResourceManager.getInstance().getString( "resources", "CONTEXT_ACTION_CLEAR" ) );
					
					if ( Input.pressed( CONTEXT_ACTION_KEY ) )
						eventClear( x, y );
					
					break;
			}
		}
		
		/**
		 * Checks for a field under a given location, returns a context sensitve
		 * game action based on the state of the field
		 * @param	worldX		Position to check
		 * @param	worldY		Position to check
		 * @return	Integer that matches one of the CONTEXT_ACTION constants
		 */
		private function GetContextAction( worldX:int, worldY:int ):int
		{
			if ( GameWorld.sim.farm == null )
				return CONTEXT_ACTION_NONE;
			
			var position:MapPosition = (world as GameWorld).farmMap.farmTiles.GetTileAtPosition( worldX, worldY );
			switch( GameWorld.sim.country.mapTiles[position.row * Country.TILE_MAP_WIDTH + position.column] )
			{
				case Country.TILE_FARM:
					return CONTEXT_ACTION_PLANT;
				case Country.TILE_SEED:
					return CONTEXT_ACTION_NONE;
				case Country.TILE_SPROUT:
					return CONTEXT_ACTION_TEND;
				case Country.TILE_RIPE:
					return CONTEXT_ACTION_HARVEST;
				case Country.TILE_WITHERED:
					return CONTEXT_ACTION_CLEAR;
			}
			
			return CONTEXT_ACTION_NONE;
		}
		
		/**
		 * Check if the current seed selection is valid, if not then pick a
		 * valid one
		 */
		private function ChooseSeeds():Product
		{
			if ( GameWorld.sim.farm.silo[seedIndex] == null || GameWorld.sim.farm.silo[seedIndex].type != Product.TYPE_SEED )
				NextSeed();
			
			if ( GameWorld.sim.farm.silo[seedIndex] != null && GameWorld.sim.farm.silo[seedIndex].type == Product.TYPE_SEED )
				return GameWorld.sim.farm.silo[seedIndex];
			else
				return null;
		}
		
		/**
		 * Selects the next or previous available seed in the silo
		 * @param	direction	Send 1 to select next seed, -1 for previous
		 */
		private function NextSeed( direction:int = 1 ):void
		{
			// Add storage size to step value to make modulus two-way
			direction += Farm.SILO_COUNT * Farm.SILO_SIZE;
			
			var siloIndex:int = (seedIndex + direction) % (Farm.SILO_COUNT * Farm.SILO_SIZE);
			while ( siloIndex != seedIndex )
			{
				if ( GameWorld.sim.farm.silo[siloIndex] != null && GameWorld.sim.farm.silo[siloIndex].type == Product.TYPE_SEED )
				{
					seedIndex = siloIndex;
					return;
				}
				
				siloIndex += direction;
				siloIndex %= (Farm.SILO_COUNT * Farm.SILO_SIZE);
			}
		}
		
		/**
		 * Show the context action label centered above the player's head
		 * @param	contextText		Text to show in the label
		 */
		private function ShowContextLabel( contextText:String ):void
		{
			contextLabel.visible = true;
			contextLabel.text = contextText;
			contextLabel.x = -(contextLabel.textWidth >> 1);
		}
	}
}