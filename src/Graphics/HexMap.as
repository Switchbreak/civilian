package Graphics 
{
	import flash.display.BitmapData;
	import flash.errors.IllegalOperationError;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import net.flashpunk.graphics.Tilemap;
	import Types.MapPosition;
	import net.flashpunk.FP;
	
	/**
	 * Extend FlashPunk tile map graphic for displaying an isometric hex grid
	 * @author Switchbreak
	 */
	public class HexMap extends Tilemap 
	{
		// Variables
		protected var baseWidth:uint;
		protected var baseHeight:uint;
		protected var cacheDirty:Boolean = false;
		
		/**
		 * Create and initialize the hex map
		 * @param	tileset			Bitmap sheet containing tiles
		 * @param	width			Dimensions of the tile map in pixels
		 * @param	height			Dimensions of the tile map in pixels
		 * @param	tileWidth		Dimensions of a tile on the tile sheet
		 * @param	tileHeight		Dimensions of a tile on the tile sheet
		 * @param	setBaseWidth	Dimensions of the hex base of a tile
		 * @param	setBaseHeight	Dimensions of the hex base of a tile
		 */
		public function HexMap(tileset:*, width:uint, height:uint, tileWidth:uint, tileHeight:uint, setBaseWidth:uint = 0, setBaseHeight:uint = 0)
		{
			if( setBaseWidth > 0 )
				baseWidth = setBaseWidth;
			else
				baseWidth = tileWidth;
			if ( setBaseHeight > 0 )
				baseHeight = setBaseHeight;
			else
				baseHeight = tileHeight;
			
			// set some tilemap information
			_rows		= height;
			_columns	= width;
			_width		= _columns * (baseWidth - (baseWidth >> 2)) + (baseWidth >> 2);
			_height		= _rows * baseHeight + (baseHeight >> 1);
			_map		= new BitmapData(_columns, _rows, false, 0);
			_temp		= _map.clone();
			_tile		= new Rectangle(0, 0, tileWidth, tileHeight);
			
			// create the canvas
			super(_width, _height);
			
			// load the tileset graphic
			if (tileset is Class) _set = FP.getBitmap(tileset);
			else if (tileset is BitmapData) _set = tileset;
			if (!_set) throw new Error("Invalid tileset graphic provided.");
			_setColumns = Math.ceil(_set.width / tileWidth);
			_setRows = Math.ceil(_set.height / tileHeight);
			_setCount = _setColumns * _setRows;
		}
		
		/**
		 * Set a tile on the tilesheet
		 * @param	column		Position of tile to set
		 * @param	row			Position of tile to set
		 * @param	index		Tile to set to
		 */
		override public function setTile(column:uint, row:uint, index:uint = 0):void 
		{
			if (usePositions)
			{
				var position:MapPosition = GetTileAtPosition( column, row );
				column = position.column;
				row = position.row;
			}
			
			index %= _setCount;
			column %= _columns;
			row %= _rows;
			_map.setPixel(column, row, index);
			
			cacheDirty = true;
		}
		
		/**
		 * Render the cache
		 */
		override public function updateAll():void 
		{
			fill( new Rectangle( 0, 0, _width, _height ), 0, 0 );
			
			for ( var row:int = 0; row < _rows; ++row )
			{
				for ( var column:int = 0; column < _columns; column += 2 )
				{
					drawTile( column, row );
				}
				for ( column = 1; column < _columns; column += 2 )
				{
					drawTile( column, row );
				}
			}
			
			cacheDirty = false;
		}
		
		/**
		 * Check if the cache needs to be rebuilt before rendering
		 */
		override public function render(target:BitmapData, point:Point, camera:Point):void 
		{
			if ( cacheDirty )
				updateAll();
			
			super.render(target, point, camera);
		}
		
		/**
		 * Because the tiles are isometric and may overlap, clearing tiles is
		 * not supported in a HexMap
		 * @deprecated	Unsupported for hex map
		 */
		[Deprecated]
		override public function clearTile(column:uint, row:uint):void 
		{
			throw new IllegalOperationError( "Clearing tiles is not implemented in a HexMap" );
		}
		
		/**
		 * Draw an individual tile to the cache
		 * @param	column	Position of the tile to draw
		 * @param	row		Position of the tile to draw
		 */
		private function drawTile( column:int, row:int ):void
		{
			var index:uint = _map.getPixel( column, row );
			
			_tile.x = (index % _setColumns) * _tile.width;
			_tile.y = uint(index / _setColumns) * _tile.height;
			_point.x = column * baseWidth * 3 / 4;
			_point.y = row * baseHeight + (column % 2) * (baseHeight >> 1);
			copyPixels(_set, _tile, _point, null, null, true);
		}
		
		/**
		 * Convert a set of world coordinates to the tilemap position of the
		 * tile underneath that world position
		 * @param	worldX	World coordinates
		 * @param	worldY	World coordinates
		 * @return	Tile map position of the tile under the world position
		 */
		public function GetTileAtPosition( worldX:int, worldY:int ):MapPosition
		{
			var position:MapPosition = new MapPosition();
			
			// Get approximate column and row by bounding box
			var boundingBoxWidth:Number = baseWidth - (baseWidth >> 2);
			worldX -= this.x;
			position.column = Math.floor( worldX / boundingBoxWidth );
			
			var lowColumn:int = Math.abs( position.column % 2 );
			worldY -= this.y + tileHeight - baseHeight + lowColumn * (baseHeight >> 1);
			position.row = Math.floor( worldY / baseHeight );
			
			// Get location inside bounding box, check if inside the leftmost
			// 1/4 of the box - the pointy part
			var hexPosX:int = (worldX + boundingBoxWidth) % boundingBoxWidth;
			if ( hexPosX < (baseWidth >> 2) )
			{
				// Point is in the pointy part end of the hexagon, check if it
				// is in the top or bottom half to choose which bounding line to
				// test it against
				var hexPosY:int = (worldY + baseHeight) % baseHeight;
				if ( hexPosY < (baseHeight >> 1) )
				{
					// Check if it is on the left or right side of the bounding
					// line - if on the left, move the approximate position to
					// the upper left tile
					hexPosY = (baseHeight >> 1) - hexPosY;
					if ( hexPosX / (baseWidth >> 2) < hexPosY / (baseHeight >> 1) )
					{
						position.column--;
						position.row -= (1 - lowColumn);
					}
				}
				else
				{
					// Same check against the lower bounding line - if on the
					// left, move the approximate position to the lower left
					// tile
					hexPosY -= (baseHeight >> 1);
					if ( hexPosX / (baseWidth >> 2) < hexPosY / (baseHeight >> 1) )
					{
						position.column--;
						position.row += lowColumn;
					}
				}
			}
			
			return position;
		}
		
		/**
		 * Get the absolute world position of a tile on the hex map
		 * @param	row			Position on the hex map
		 * @param	column		Position on the hex map
		 * @return	Absolute world position of hex
		 */
		public function GetPositionAtTile( row:int, column:int ):Point
		{
			var point:Point = new Point();
			point.x = x + column * (baseWidth - (baseWidth >> 2));
			point.y = y + tileHeight - baseHeight + row * baseHeight + Math.abs(column % 2) * (baseHeight >> 1);
			
			return point;
		}
	}

}