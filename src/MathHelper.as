package  
{
	import net.flashpunk.FP;
	
	/**
	 * Class to provide some helper math functions
	 * @author Switchbreak
	 */
	public final class MathHelper
	{
		
		private static var BoxMullerReady : Boolean;
		private static var BoxMullerCache : Number;
		
		public static function BoxMuller() : Number
		{
			if ( BoxMullerReady )
			{							//  Return a cached result
				BoxMullerReady = false;	//  from a previous call
				return BoxMullerCache;	//  if available.
			}

			var	x : Number,		//  Repeat extracting uniform values
				y : Number,		//  in the range ( -1,1 ) until
				w : Number;		//  0 < w = x*x + y*y < 1
			do
			{
				x = FP.random * 2 - 1;
				y = FP.random * 2 - 1;
				w = x * x + y * y;
			}
			while ( w >= 1 || !w );
			
			w = Math.sqrt ( -2 * Math.log ( w ) / w );
			
			BoxMullerReady = true;
			BoxMullerCache = x * w;	//  Cache one of the outputs
			return y * w;			//  and return the other.
		}
		
	}
}