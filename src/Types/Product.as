package Types
{
	/**
	 * Object containing farm product information, either seeds or crops
	 * @author Switchbreak
	 */
	public class Product 
	{
		
		public static const TYPE_SEED:int = 0;
		public static const TYPE_CROP:int = 1;
		
		public static const STORAGE_TIME_FRESH:int = 1;
		public static const STORAGE_TIME_NORMAL:int = 3;
		public static const STORAGE_TIME_OLD:int = 5;
		public static const STORAGE_TIME_ROTTEN:int = 6;
		
		public var type:int;
		public var plantType:int;
		public var quantity:int;
		public var storageTime:int;
		
	}

}