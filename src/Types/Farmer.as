package Types
{
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class Farmer 
	{
		
		public var name:String;
		public var spouse:String = null;
		public var children:Vector.<String> = new Vector.<String>;
		
		public var money:Number;
		
	}

}