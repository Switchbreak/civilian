package Types 
{
	/**
	 * Object to hold information about a town
	 * 
	 * @author Switchbreak
	 */
	public class Town 
	{
		
		public var name:String;
		public var country:Country;
		public var occupiedBy:War = null;
		
	}

}