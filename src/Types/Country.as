package Types 
{
	import adobe.utils.ProductManager;
	import flash.geom.Point;
	import net.flashpunk.FP;
	
	/**
	 * Object containing country information
	 * 
	 * @author Switchbreak
	 */
	public class Country 
	{
		// Constants
		private static const	CROP_PRICE_DEVIATION:Number = 0.2;
		private static const	BASE_TAX:Number				= 0.15;
		private static const	TAX_DEVIATION:Number		= 0.1;
		private static const	TREE_COUNT:int				= 25;
		public static const		TILE_MAP_WIDTH:int			= 40;
		public static const		TILE_MAP_HEIGHT:int			= 40;
		private static const	FARM_WIDTH:int				= 10;
		private static const	FARM_HEIGHT:int				= 10;
		static private const	RANDOM_PLACEMENT_TRIES:int	= 2000;
		
		// Tiles
		public static const	TILE_GRASS:int		= 0;
		public static const	TILE_TREE:int		= 1;
		public static const	TILE_SEED:int		= 2;
		public static const	TILE_SPROUT:int		= 3;
		public static const	TILE_TENDED:int		= 4;
		public static const	TILE_RIPE:int		= 5;
		public static const	TILE_WITHERED:int	= 6;
		public static const	TILE_RIVER:int		= 7;
		public static const	TILE_BRIDGE:int		= 8;
		public static const TILE_FARM:int		= 9;
		public static const TILE_BARN:int		= 10;
		public static const TILE_SILO:int		= 11;
		public static const TILE_HOUSE:int		= 12;
		
		public static const WALKABLE_TILES:Array	= [TILE_GRASS, TILE_SEED, TILE_SPROUT, TILE_TENDED, TILE_RIPE, TILE_WITHERED, TILE_BRIDGE, TILE_FARM];
		public static const BUILDING_TILES:Array	= [TILE_FARM, TILE_BARN, TILE_SILO, TILE_HOUSE];
		
		// Variables
		public var name:String;
		public var tax:Number;
		public var farm:Farm;
		public var x:int, y:int;
		public var flag:Class;
		public var occupiedBy:Country;
		public var cropPrices:Vector.<Number>;
		public var adjacent:Vector.<Country>;
		public var borderCenter:Vector.<Point>;
		public var wars:Vector.<War>			= new Vector.<War>();
		public var towns:Vector.<Town>			= new Vector.<Town>();
		public var defending:Boolean			= true;
		public var mapTiles:Vector.<int>		= new Vector.<int>(TILE_MAP_WIDTH * TILE_MAP_HEIGHT);
		public var startingPoint:MapPosition;
		
		/**
		 * Create and initialize the country
		 * @param	setName		Name of the new country
		 * @param	cropCount	Number of crops to price
		 */
		public function Country( setName:String, cropCount:int )
		{
			// Set country values
			name		= setName;
			tax			= BASE_TAX + BASE_TAX * MathHelper.BoxMuller() * TAX_DEVIATION;
			occupiedBy	= this;
			cropPrices	= new Vector.<Number>( cropCount );
			
			SetCropPrices();
			
			CreateRiver();
			PlaceFarm();
			PlantTrees();
		}
		
		/**
		 * Set all crop prices according to the normal distribution
		 */
		public function SetCropPrices():void
		{
			for ( var i:int = 0; i < cropPrices.length; ++i )
				cropPrices[i] = MathHelper.BoxMuller() * CROP_PRICE_DEVIATION;
		}
		
		/**
		 * Adds a town to the country
		 * @param	townName	Name of the new town
		 */
		public function AddTown( townName:String ):void
		{
			var town:Town	= new Town();
			town.name		= townName;
			town.country	= this;
			
			towns.push( town );
		}
		
		/**
		 * Randomly place the farm on the tilemap
		 */
		private function PlaceFarm():void 
		{
			var spotValid:Boolean = false;
			var tries:int = RANDOM_PLACEMENT_TRIES;
			
			while ( !spotValid && --tries > 0 )
			{
				var farmRow:int		= FP.rand( TILE_MAP_HEIGHT - (FARM_WIDTH + FARM_HEIGHT) );
				var farmColumn:int	= FP.rand( TILE_MAP_WIDTH - (FARM_WIDTH + FARM_HEIGHT) ) + FARM_HEIGHT;
				
				spotValid = true;
				var mapRowStart:int	= farmRow;
				
				for ( var row:int = 0; row < FARM_HEIGHT; ++row )
				{
					var mapRow:int = mapRowStart;
					
					for ( var mapColumn:int = farmColumn - row; mapColumn < farmColumn - row + FARM_WIDTH; ++mapColumn )
					{
						var lowColumn:int = mapColumn % 2;
						
						mapRow += lowColumn;
						
						if ( mapTiles[mapRow * TILE_MAP_WIDTH + mapColumn] != TILE_GRASS )
							spotValid = false;
					}
					
					mapRowStart += ((farmColumn - row) % 2);
				}
			}
			
			if ( !spotValid )
				throw new Error( "Could not randomly place farm" );
			
			mapRowStart = farmRow;
			for ( row = 0; row < FARM_HEIGHT; ++row )
			{
				mapRow = mapRowStart;
				
				for ( mapColumn = farmColumn - row; mapColumn < farmColumn - row + FARM_WIDTH; ++mapColumn )
				{
					if( row > 0 )
						mapTiles[mapRow * TILE_MAP_WIDTH + mapColumn] = TILE_FARM;
					
					mapRow += mapColumn % 2;
				}
				
				mapRowStart += ((farmColumn - row) % 2);
			}
			
			PlaceBuilding( farmRow, farmColumn, TILE_BARN );
			PlaceBuilding( farmRow, farmColumn, TILE_SILO );
			startingPoint = PlaceBuilding( farmRow, farmColumn, TILE_HOUSE );
		}
		
		/**
		 * Place a building at a random spot on the farm
		 * @param	farmRow			Position of farm on the map
		 * @param	farmColumn		Position of farm on the map
		 * @param	buildingType	Type of building to place
		 * @return	Position of the building that was placed
		 */
		private function PlaceBuilding( farmRow:int, farmColumn:int, buildingType:int ):MapPosition
		{
			var tries:int = RANDOM_PLACEMENT_TRIES;
			
			while ( --tries > 0 )
			{
				var column:int	= FP.rand( FARM_WIDTH );
				
				var mapColumn:int	= farmColumn + column;
				var mapRow:int		= farmRow + Math.floor( (column + (farmColumn % 2)) / 2 );
				
				if ( mapTiles[mapRow * TILE_MAP_WIDTH + mapColumn] == TILE_GRASS )
				{
					mapTiles[mapRow * TILE_MAP_WIDTH + mapColumn] = buildingType;
					return new MapPosition( mapRow, mapColumn );
				}
				else
				{
					var tile:int = mapTiles[mapRow * TILE_MAP_WIDTH + mapColumn];
				}
			}
			
			throw( new Error( "Could not place building randomly" ) );
			return null;
		}
		
		/**
		 * Randomly places trees on tilemap
		 */
		private function PlantTrees():void
		{
			for ( var i:int = 0; i < TREE_COUNT; ++i )
			{
				var location:int = FP.rand( TILE_MAP_WIDTH * TILE_MAP_HEIGHT );
				if ( mapTiles[location] == TILE_GRASS )
					mapTiles[location] = TILE_TREE;
			}
		}
		
		/**
		 * Places a river on the map by starting at the top and traversing
		 * randomly downward
		 */
		private function CreateRiver():void
		{
			var riverRow:int	= 0;
			var riverColumn:int	= 1 + FP.rand( TILE_MAP_WIDTH - 2 );
			var nextColumn:int	= 0;
			var bridgeRow:int	= FP.rand( TILE_MAP_HEIGHT );
			
			while ( riverRow < TILE_MAP_HEIGHT )
			{
				if ( riverRow == bridgeRow )
				{
					mapTiles[ riverRow * TILE_MAP_WIDTH + riverColumn] = TILE_BRIDGE;
					bridgeRow = -1;
				}
				else
				{
					mapTiles[ riverRow * TILE_MAP_WIDTH + riverColumn] = TILE_RIVER;
					
					if( nextColumn == 0 )
						nextColumn = FP.rand( 3 ) - 1;
					else if ( nextColumn > 0 )
						nextColumn = FP.rand( 2 );
					else
						nextColumn = FP.rand( 2 ) - 1;
					
					if ( (nextColumn < 0 && riverColumn <= 1) || (nextColumn > 0 && riverColumn >= TILE_MAP_WIDTH - 2) )
						nextColumn = 0;
				}
				
				var lowColumn:int = riverColumn % 2;
				if ( lowColumn == 1 || nextColumn == 0 )
					++riverRow;
				
				riverColumn += nextColumn;
			}
		}
	}
}