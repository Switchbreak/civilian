package Types
{
	import net.flashpunk.FP;
	
	/**
	 * Farm object contains all relavant data to a farm
	 * @author Switchbreak
	 */
	public class Farm 
	{
		// Constants
		public static const	SILO_COUNT:int			= 2;
		public static const	SILO_SIZE:int			= 5;
		public static const	SILO_CROP_STORAGE:int	= 150;
		public static const	SILO_SEED_STORAGE:int	= 1;
		
		// Attributes
		public var storageSpace:int			= SILO_COUNT * SILO_SIZE;
		public var silo:Vector.<Product>	= new Vector.<Product>(SILO_COUNT * SILO_SIZE);
		public var fields:Vector.<Field>	= new Vector.<Field>();
		
		//
		// Methods
		//
		
		/**
		 * Stores a product in the farm's silos
		 * @param	product		Product to store
		 * @return	True if product was stored, false if there was a problem
		 */
		public function StoreProduct( product:Product ):Boolean
		{
			var availableStorage:int = AvailableStorage( product.type );
			
			// Break the product into storage slots and store them
			while ( product.quantity > 0 && storageSpace > 0 )
			{
				var slotIndex:int = FirstAvailableSlot( product );
				if ( silo[slotIndex] == null )
				{
					silo[slotIndex] = BreakStorageProduct( product, availableStorage );
					--storageSpace;
				}
				else
				{
					silo[slotIndex].quantity += BreakStorageProduct( product, availableStorage - silo[slotIndex].quantity ).quantity;
				}
			}
			
			return true;
		}
		
		/**
		 * Breaks a product into parts if necessary, returns a product small
		 * enough to store in the available space
		 * @param	product		Product to store, quantity will be decreased by
		 * 						the amount stored
		 * @return	Storable product, broken apart to available size if needed
		 */
		private function BreakStorageProduct( product:Product, availableStorage:int ):Product 
		{
			var storeProduct:Product = new Product();
			storeProduct.plantType = product.plantType;
			storeProduct.storageTime = product.storageTime;
			storeProduct.type = product.type;
			if( product.quantity > availableStorage )
				storeProduct.quantity = availableStorage;
			else
				storeProduct.quantity = product.quantity;
			
			product.quantity -= availableStorage;
			
			return storeProduct;
		}
		
		/**
		 * Return the index of the first available storage slot in the silos for
		 * a specified product - searches for non-full slots of the same type
		 * and age of product first, then returns empty slots if needed.
		 * @param	product		Product to find space for
		 * @return	Storage slot index
		 */
		private function FirstAvailableSlot( product:Product ):int
		{
			for (var i:int = 0; i < SILO_COUNT * SILO_SIZE; i++) 
			{
				if (	silo[i] != null && silo[i].storageTime < Product.STORAGE_TIME_ROTTEN &&
						silo[i].type == product.type && silo[i].plantType == product.plantType &&
						silo[i].storageTime == product.storageTime &&
						silo[i].quantity < AvailableStorage( product.type ) )
				{
					return i;
				}
			}
			
			for ( i = 0; i < SILO_COUNT * SILO_SIZE; ++i )
			{
				// If the slot is empty or the contents have rotted, then it
				// is available
				if ( silo[i] == null || silo[i].storageTime >= Product.STORAGE_TIME_ROTTEN )
					return i;
			}
			
			return -1;
		}
		
		/**
		 * Gets the amount of space required to store a product
		 * @param	product		Product to check
		 * @return	Number of storage slots required
		 */
		public function GetStorageRequired( product:Product ):int
		{
			var storageQuantity:int	= product.quantity;
			var slotsRequired:int	= 0;
			var slotSpace:int		= AvailableStorage( product.type );
			
			while ( storageQuantity > 0 )
			{
				var slotIndex:int = FirstAvailableSlot( product );
				if ( silo[slotIndex] == null )
				{
					++slotsRequired;
					storageQuantity -= slotSpace;
				}
				else
				{
					storageQuantity -= (slotSpace - silo[slotIndex].quantity);
				}
			}
			
			return slotsRequired;
		}
		
		//
		// Static methods
		//
		
		/**
		 * Return the available storage for the given product type
		 * @param	productType		Product.type, either TYPE_CROP or TYPE_SEED
		 */
		private static function AvailableStorage( productType:int ):int
		{
			switch( productType )
			{
				case Product.TYPE_CROP:
					return Farm.SILO_CROP_STORAGE;
				case Product.TYPE_SEED:
					return Farm.SILO_SEED_STORAGE;
			}
			
			return 0;
		}
	}
}