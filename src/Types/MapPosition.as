package Types 
{
	/**
	 * Position on the hex map, defined by a column and row
	 * @author Switchbreak
	 */
	public class MapPosition 
	{
		public var column:int;
		public var row:int;
		
		public function MapPosition( setRow:int = 0, setColumn:int = 0 )
		{
			column	= setColumn;
			row		= setRow;
		}
	}

}