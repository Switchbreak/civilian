package Types 
{
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class Field 
	{
		
		public static const AGE_SEED:int		= 1;
		public static const AGE_SPROUT:int		= 2;
		public static const AGE_RIPE:int		= 3;
		public static const AGE_WITHERED:int	= 4;
		
		public var plantType:int;
		public var age:int;
		public var tended:Boolean;
		public var yield:int;
		public var position:MapPosition;
	}

}