package Types 
{
	/**
	 * Object containing crop information
	 * 
	 * @author Switchbreak
	 */
	public class Crop 
	{
		
		public var cropName:String;
		public var seedPrice:Number;
		public var cropPrice:Number;
		public var yield:Number;
		public var growthRate:int;
		public var winterCrop:Boolean;
		
		/**
		 * Create the crop object
		 * 
		 * @param	setCropName
		 * Name of the crop
		 * @param	setPrice
		 * Price of the crop
		 * @param	setGrowthRate
		 * Number of seasons the crop takes to grow
		 */
		public function Crop( setCropName:String, setSeedPrice:Number, setCropPrice:Number, setYield:Number, setGrowthRate:int, setWinterCrop:Boolean )
		{
			cropName = setCropName;
			seedPrice = setSeedPrice;
			cropPrice = setCropPrice;
			yield = setYield;
			growthRate = setGrowthRate;
			winterCrop = setWinterCrop;
		}
		
	}

}