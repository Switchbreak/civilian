package  
{
	import flash.display.*;
	import flash.filters.*;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import net.flashpunk.graphics.Image;
	import Types.Country;
	import Voronoi.*;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Draw;
	
	/**
	 * Object to generate game map
	 * 
	 * @author Switchbreak
	 */
	public class Map 
	{
		// Embeds
		[Embed (source = '../img/Flag1.png')] private static const			FLAG1_SPRITE:Class;
		[Embed (source = '../img/Flag2.png')] private static const			FLAG2_SPRITE:Class;
		[Embed (source = '../img/Flag3.png')] private static const			FLAG3_SPRITE:Class;
		[Embed (source = '../img/Flag4.png')] private static const			FLAG4_SPRITE:Class;
		[Embed (source = '../img/Flag5.png')] private static const			FLAG5_SPRITE:Class;
		[Embed (source = '../img/Flag6.png')] private static const			FLAG6_SPRITE:Class;
		[Embed (source = '../img/MountainsSmall.png')] private static const	MOUNTAIN_SPRITE:Class;
		
		// Constants
		private static const MAP_BORDER:int				= 50;
		private static const MAP_TRY_LIMIT:int			= 100;
		private static const MAP_MAX_TRY_LIMIT:int		= 100000;
		private static const MAP_DIST_SQUARED:int		= 22500;
		private static const MAP_MIN_DIST_SQUARED:int	= 2500;
		private static const MOUNTAIN_DIST_SQUARED:int	= 2500;
		private static const MOUNTAIN_COUNT:int			= 30;
		private static const BACK_COLOR:uint			= 0x0C7A3D;
		private static const BORDER_COLOR:uint			= 0;
		private static const BORDER_THICKNESS:uint		= 2;
		private static const WATER_COLOR:uint			= 0x882DFFF1;
		
		// Read-only properties
		private var _mapBitmap:BitmapData;
		private var _mapMask:BitmapData;
		private var flagImages:Array = [FLAG1_SPRITE, FLAG2_SPRITE, FLAG3_SPRITE, FLAG4_SPRITE, FLAG5_SPRITE, FLAG6_SPRITE];
		
		// Private variables
		private var width:int;
		private var height:int;
		private var voronoiSites:Vector.<Point>;
		
		/**
		 * Initialize map
		 * @param countries		List of countries to place on map
		 * @param setWidth		Dimensions of map image
		 * @param setHeight		Dimensions of map image
		 */
		public function Map( countries:Vector.<Country>, setWidth:int, setHeight:int )
		{
			// Set object values
			width = setWidth;
			height = setHeight;
			
			voronoiSites = new Vector.<Point>();
			
			// Procedurally place all countries on the map following these rules
			for each( var country:Country in countries )
			{
				var newSite:Point = AddRandomSite();
				
				voronoiSites.push( newSite );
			}
			
			// Add an additional random water site
			newSite = AddRandomSite();
			voronoiSites.push( newSite );
			
			newSite = AddRandomSite();
			voronoiSites.push( newSite );
			
			// Create voronoi tesselation
			var voronoi:Tesselator = new Tesselator( voronoiSites, new Rectangle( 0, 0, width, height ) );
			voronoi.Tesselate();
			var lines:Vector.<Edge> = voronoi.edges;
			var cells:Vector.<Cell> = voronoi.cells;
			
			// Set country position and adjacency list
			ProcessCountries( countries, cells );
			
			//
			// Draw the map
			//
			
			// Initialize the map image
			//_mapStamp = new Stamp( MAP_BACKGROUND );
			_mapBitmap = new BitmapData( width, height, true, BACK_COLOR );
			Draw.setTarget( _mapBitmap );
			
			// Draw voronoi borders on map image
			var borders:Vector.<Border> = new Vector.<Border>();
			for each( var line:Edge in lines )
			{
				// Get the indices of the two sites bounded by this line
				var siteIndex:int = voronoiSites.indexOf( line.face.site );
				var twinIndex:int = voronoiSites.indexOf( line.twin.face.site );
				
				// Do not draw boundary between two water cells
				if ( siteIndex < countries.length || twinIndex < countries.length )
				{
					// Create fractal border along voronoi boundary
					var border:Border = new Border();
					border.points = FractalLine( line.origin, line.twin.origin, 5, 50 );
					
					// If both bordered sites are countries, set the border
					// center to the middle point of the fractal line
					if ( siteIndex < countries.length && twinIndex < countries.length )
					{
						// Get midpoint from fractal line
						var midPoint:Point = border.points[border.points.length >> 1];
						
						// Set border center on both countries
						var adjacentIndex:int = countries[siteIndex].adjacent.indexOf( countries[twinIndex] );
						countries[siteIndex].borderCenter[adjacentIndex].x = midPoint.x;
						countries[siteIndex].borderCenter[adjacentIndex].y = midPoint.y;
						
						adjacentIndex = countries[twinIndex].adjacent.indexOf( countries[siteIndex] );
						countries[twinIndex].borderCenter[adjacentIndex].x = midPoint.x;
						countries[twinIndex].borderCenter[adjacentIndex].y = midPoint.y;
					}
					
					// Draw fractal border
					var prevPoint:Point = null;
					for each( var point:Point in border.points )
					{
						if ( prevPoint != null )
						{
							Draw.line( prevPoint.x, prevPoint.y, point.x, point.y, BORDER_COLOR, 1 );
						}
						
						prevPoint = point;
					}
					
					// If border is between land and water, draw gradient
					if ( voronoiSites.indexOf( line.face.site ) >= countries.length )
						border.waterLeft = true;
					else if ( voronoiSites.indexOf( line.twin.face.site ) >= countries.length )
						border.waterRight = true;
					
					borders.push( border );
				}
			}
			
			// Clone the mask bitmap and fill in the cells
			_mapMask = mapBitmap.clone();
			for ( var i:int = 0; i < countries.length; i++ )
			{
				_mapMask.floodFill( countries[i].x, countries[i].y, 0xFF000000 + i + 1 );
			}
			
			// Fill in ocean cells
			var waterBitmap:BitmapData = _mapBitmap.clone();
			
			for ( i = countries.length; i < voronoiSites.length; i++ )
			{
				waterBitmap.floodFill( voronoiSites[i].x, voronoiSites[i].y, 0 );
				_mapBitmap.floodFill( voronoiSites[i].x, voronoiSites[i].y, WATER_COLOR );
			}
			
			// Do inner glow around ocean bitmap for gradient borders
			waterBitmap = _mapBitmap.compare( waterBitmap ) as BitmapData;
			if ( waterBitmap != null )
			{
				var blur:GlowFilter = new GlowFilter( 0, 1, 20, 20, 2, 1, true, false );
				waterBitmap.applyFilter( waterBitmap, waterBitmap.rect, new Point( 0, 0 ), blur );
				_mapBitmap.copyPixels( waterBitmap, waterBitmap.rect, new Point( 0, 0 ), mapBitmap, new Point( 0, 0 ) );
			}
			
			// Place mountains randomly
			var mountainPoints:Vector.<Point> = new Vector.<Point>;
			for ( i = 0; i < MOUNTAIN_COUNT; i++ )
			{
				// Get mountain sprite
				var mountain:BitmapData = FP.getBitmap( MOUNTAIN_SPRITE );
				
				// Position sprite randomly, fall in the water
				var pointValid:Boolean = false;
				var destPoint:Point;
				var tries:int = 0;
				while ( !pointValid && tries < MAP_TRY_LIMIT )
				{
					// Generate random point on map
					destPoint = new Point( FP.rand( width - mountain.width ) + (mountain.width >> 1), FP.rand( height - mountain.height  ) + (mountain.height >> 1) );
					pointValid = true;
					
					//
					// Check center point and corners to make sure no part of
					// the mountain is over water
					//
					
					// Center
					if ( _mapBitmap.getPixel( destPoint.x, destPoint.y ) != 0 )
						pointValid = false;
					// Top Left
					if ( _mapBitmap.getPixel( destPoint.x - (mountain.width >> 1), destPoint.y - (mountain.height >> 1) ) != 0 )
						pointValid = false;
					// Top Right
					if ( _mapBitmap.getPixel( destPoint.x + (mountain.width >> 1), destPoint.y - (mountain.height >> 1) ) != 0 )
						pointValid = false;
					// Bottom left
					if ( _mapBitmap.getPixel( destPoint.x - (mountain.width >> 1), destPoint.y + (mountain.height >> 1) ) != 0 )
						pointValid = false;
					// Bottom right
					if ( _mapBitmap.getPixel( destPoint.x + (mountain.width >> 1), destPoint.y + (mountain.height >> 1) ) != 0 )
						pointValid = false;
					
					// Check that it is a minimum distance away from all country
					// centroids to avoid overlapping with flags/text
					for each( country in countries )
					{
						if ( (destPoint.x - country.x) * (destPoint.x - country.x) + (destPoint.y - country.y) * (destPoint.y - country.y) < MOUNTAIN_DIST_SQUARED )
						{
							pointValid = false;
							break;
						}
					}
					
					// Keep mountains a minimum distance from each other
					for each( var mountainPoint:Point in mountainPoints )
					{
						if ( (destPoint.x - mountainPoint.x) * (destPoint.x - mountainPoint.x) + (destPoint.y - mountainPoint.y) * (destPoint.y - mountainPoint.y) < MOUNTAIN_DIST_SQUARED )
						{
							pointValid = false;
							break;
						}
					}
					
					tries++;
				}
				
				if ( pointValid )
				{
					mountainPoints.push( destPoint );
					destPoint.x -= (mountain.width >> 1);
					destPoint.y -= (mountain.height >> 1);
					_mapBitmap.copyPixels( mountain, mountain.rect, destPoint, null, null, true );
				}
			}
			
			//
			// Draw nicer borders
			//
			
			// Draw country borders highlighted in yellow
			for each( border in borders )
			{
				if ( !border.waterLeft && !border.waterRight )
				{
					prevPoint = null;
					for each( point in border.points )
					{
						if ( prevPoint != null )
						{
							Draw.linePlus( prevPoint.x, prevPoint.y, point.x, point.y, 0xFFFF00, 1, BORDER_THICKNESS << 1 );
						}
						
						prevPoint = point;
					}
				}
			}
			
			// Draw ordinary borders
			for each( border in borders )
			{
				prevPoint = null;
				for each( point in border.points )
				{
					if ( prevPoint != null )
					{
						Draw.linePlus( prevPoint.x, prevPoint.y, point.x, point.y, BORDER_COLOR, 1, BORDER_THICKNESS );
					}
					
					prevPoint = point;
				}
			}
			
			Draw.resetTarget();
		}
		
		/**
		 * Adds a randomly positioned site, placed according to rules
		 * constraining the minimum distance it can be from other sites
		 * @return
		 */
		private function AddRandomSite():Point
		{
			var newSite:Point = new Point();
			var minDistance:Boolean = false;
			var tries:int = 0;
			
			// Loop until an acceptable point is found or the final try
			// threshold is passed
			while ( !minDistance && tries < MAP_MAX_TRY_LIMIT )
			{
				// Place site randomly
				newSite.x = FP.rand( width - (MAP_BORDER << 1) ) + MAP_BORDER;
				newSite.y = FP.rand( height - (MAP_BORDER << 1) ) + MAP_BORDER;
				
				// If points fail to fall outside of large distance
				// threshold after many tries, use the small distance
				// threshold
				var minimum:int = MAP_DIST_SQUARED;
				if ( tries > MAP_TRY_LIMIT )
					minimum = MAP_MIN_DIST_SQUARED;
				
				// Check that the points are the minimum distance away from
				// each other
				minDistance = true;
				for each( var pairSite:Point in voronoiSites )
				{
					var distanceSquared:Number = (newSite.x - pairSite.x) * (newSite.x - pairSite.x) + (newSite.y - pairSite.y) * (newSite.y - pairSite.y);
					if ( distanceSquared < minimum )
					{
						minDistance = false;
						break;
					}
				}
				
				tries++;
			}
			
			return newSite;
		}
		
		/**
		 * Draws a fractal line, subdivided recursively and randomized
		 * 
		 * @param	start
		 * Endpoints of the line
		 * @param	end
		 * Endpoints of the line
		 * @param	subLength
		 * Average length of line subdivision
		 * @param	range
		 * Range of randomization
		 * @param	modifier
		 * Amount to modify the range by with each subdivision
		 * @return
		 * List of points to draw, in order
		 */
		private function FractalLine( start:Point, end:Point, subLength:Number, range:Number, modifier:Number = 0.5 ):Vector.<Point>
		{
			// If the line is less than subLength long, draw it
			var lengthSq:Number = (end.x - start.x) * (end.x - start.x) + (end.y - start.y) * (end.y - start.y);
			if ( lengthSq < range * range )
				range /= 2;
			
			if ( lengthSq <= (subLength * subLength) )
			{
				var points:Vector.<Point> = new Vector.<Point>();
				points.push( start );
				points.push( end );
			}
			else
			{
				// Line is longer than subdivision length, divide it into two
				var midpoint:Point = new Point( (start.x + end.x) / 2, (start.y + end.y) / 2 );
				
				// Randomize the midpoint
				midpoint.x += FP.random * range - (range / 2);
				midpoint.y += FP.random * range - (range / 2);
				
				// Modify the range
				range *= modifier;
				
				// Recurse
				var points1:Vector.<Point> = FractalLine( start, midpoint, subLength, range, modifier );
				var points2:Vector.<Point> = FractalLine( midpoint, end, subLength, range, modifier );
				points = points1.concat( points2 );
			}
			
			return points;
		}
		
		/**
		 * Moves all country sites to the centroid of their containing cell and
		 * populates their adjacency list with neighboring countries
		 * 
		 * @param	countries
		 * List of countries
		 * @param	cells
		 * List of voronoi cells containing the countries (indices must match)
		 */
		private function ProcessCountries( countries:Vector.<Country>, cells:Vector.<Cell> ):void 
		{
			// Move country positions to the approximate centroids of their cell
			for ( var i:int = 0; i < countries.length; i++ )
			{
				// Set country's map properties
				countries[i].x = 0;
				countries[i].y = 0;
				countries[i].adjacent = new Vector.<Country>();
				countries[i].borderCenter = new Vector.<Point>();
				
				// Pick the country's flag randomly
				var flagIndex:int = FP.rand( flagImages.length );
				countries[i].flag = flagImages[flagIndex];
				flagImages.splice( flagIndex, 1 );
				
				// Initialize array of flags for each of the edges clipped by
				// the country cell
				var edgesClipped:Vector.<Boolean> = new Vector.<Boolean>( 4 );
				for each( var clipEdge:Boolean in edgesClipped )
					clipEdge = false;
				
				// Approximate the centroid by adding together all points on the
				// edges of the cell and taking the mean average
				var pointCount:int = 0;
				for each( var line:Edge in cells[i].edges )
				{
					// Set country adjacency list to neighboring cell
					var neighborIndex:int = cells.indexOf( line.twin.face );
					if ( neighborIndex >= 0 && neighborIndex < countries.length )
					{
						countries[i].adjacent.push( countries[neighborIndex] );
						countries[i].borderCenter.push( new Point( (line.origin.x + line.twin.origin.x) / 2, (line.origin.y + line.twin.origin.y) / 2 ) );
					}
					
					// Add the half-edge endpoint to the average
					countries[i].x += line.origin.x;
					countries[i].y += line.origin.y;
					pointCount++;
					
					// If the half-edge is clipped, flag the edge
					if ( line.clipEdge > -1 )
						edgesClipped[line.clipEdge] = true;
					
					// If the half-edge twin is clipped, at the twin endpoint
					if ( line.twin.clipEdge > -1 )
					{
						edgesClipped[line.twin.clipEdge] = true;
						
						countries[i].x += line.twin.origin.x;
						countries[i].y += line.twin.origin.y;
						pointCount++;
					}
				}
				
				// For each of the four corners, check if both walls are flagged
				// and add them to the average if so
				if ( edgesClipped[0] && edgesClipped[2] )
				{
					// Top left corner
					pointCount++;
				}
				if ( edgesClipped[1] && edgesClipped[2] )
				{
					// Top right corner
					countries[i].x += width;
					pointCount++;
				}
				if ( edgesClipped[0] && edgesClipped[3] )
				{
					// Bottom left corner
					countries[i].y += height;
					pointCount++;
				}
				if ( edgesClipped[1] && edgesClipped[3] )
				{
					// Bottom right corner
					countries[i].x += width;
					countries[i].y += height;
					pointCount++;
				}
				
				// Points are added twice because we need to include the half-
				// edge twins to make sure the clipping points are included
				countries[i].x /= pointCount;
				countries[i].y /= pointCount;
			}
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets the generated map image
		 */
		public function get mapBitmap():BitmapData
		{
			return _mapBitmap;
		}
		
		/**
		 * Gets teh map mask, with clickable areas color-coded
		 */
		public function get mapMask():BitmapData
		{
			return _mapMask;
		}
	}

}

internal class Border
{
	public var points:Vector.<flash.geom.Point>;
	public var waterLeft:Boolean;
	public var waterRight:Boolean;
}