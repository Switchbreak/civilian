package Weather 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.tweens.misc.Alarm;
	
	/**
	 * Snow weather effect
	 * @author Switchbreak
	 */
	public class Snowstorm extends Entity 
	{
		// Constants
		private var SNOWFLAKE_RATE:Number = 0.1;
		
		// Graphics
		private var snowflakes:Graphiclist;
		private var recycled:Vector.<Snowflake>;
		
		/**
		 * Create and initialize the snow element
		 */
		public function Snowstorm() 
		{
			layer		= -30000;
			recycled	= new Vector.<Snowflake>();
			
			// Initialize graphic
			snowflakes			= new Graphiclist();
			snowflakes.scrollX	= 0;
			snowflakes.scrollY	= 0;
			super( 0, 0, snowflakes );
			
			// Spawn snowflakes on a timer
			addTween( new Alarm( SNOWFLAKE_RATE, SpawnSnowflake, LOOPING ), true );
		}
		
		/**
		 * Remove a snowflake and places it in queue to be reused
		 * @param	snowflake	Reference to the snowflake to remove
		 */
		public function RecycleSnowflake( snowflake:Snowflake ):void
		{
			recycled.push( snowflake );
		}
		
		/**
		 * Create a new snowflake at the top of the screen
		 */
		private function SpawnSnowflake():void
		{
			if ( recycled.length > 0 )
			{
				recycled.shift().Init();
			}
			else
			{
				snowflakes.add( new Snowflake( this ) );
			}
		}
	}
}