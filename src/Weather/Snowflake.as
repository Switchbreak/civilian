package Weather 
{
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.FP;
	
	/**
	 * Snowflake graphic
	 * 
	 * @author Switchbreak
	 */
	public class Snowflake extends Stamp 
	{
		// Embeds
		[Embed (source = '../../img/Snowflake.png')]	private static const SNOWFLAKE:Class;
		
		// Constants
		private var SPEED_RANGE:Number	= 10;
		private var FALL_SPEED_Y:Number	= 50;
		private var FALL_SPEED_X:Number = 0;
		private var MIN_Y:int			= 160;
		private var MAX_Y:int			= 1380;
		
		// Variables
		private var parent:Snowstorm;
		private var fallSpeedY:Number;
		private var fallSpeedX:Number;
		private var landY:int;
		public var	dead:Boolean;
		private var	snowX:Number, snowY:Number;
		
		/**
		 * Create and initialize the snowflake
		 */
		public function Snowflake( snow:Snowstorm ) 
		{
			// Set object values
			relative	= false;
			parent		= snow;
			
			// Create and position the snowflake graphic
			super( SNOWFLAKE );
			
			Init();
		}
		
		/**
		 * Initialize the snowflake
		 */
		public function Init():void
		{
			dead	= false;
			active	= true;
			visible	= true;
			
			// Randomize the snowflake motion
			fallSpeedX = FALL_SPEED_X + (FP.random * (SPEED_RANGE << 1)) - SPEED_RANGE;
			fallSpeedY = FALL_SPEED_Y + (FP.random * (SPEED_RANGE << 1)) - SPEED_RANGE;
			
			// Randomize the spot where it will land
			landY = FP.rand( MAX_Y - MIN_Y ) + MIN_Y;
			
			snowX = FP.rand( FP.width - width );
			snowY = landY - FP.height;
		}
		
		/**
		 * Remove the snowflake when it lands
		 */
		public function Die():void
		{
			dead	= true;
			active	= false;
			visible	= false;
			
			parent.RecycleSnowflake( this );
		}
		
		/**
		 * Handle snowflake movement
		 */
		override public function update():void 
		{
			// Motion
			snowX += fallSpeedX * FP.elapsed;
			snowY += fallSpeedY * FP.elapsed;
			
			x = FP.width - ((snowX + FP.camera.x) % FP.width);
			y = snowY - FP.camera.y;
			
			// If it has fallen to the preselected point, remove it
			if ( y >= landY )
				Die();
			
			super.update();
		}
		
	}

}