package Voronoi 
{
	import flash.geom.Point;
	
	/**
	 * Voronoi cell
	 * 
	 * @author Switchbreak
	 */
	
	public class Cell 
	{
		
		public var edges:Vector.<Edge>;
		public var site:Point;
		public var border:Boolean = false;
		
	}

}