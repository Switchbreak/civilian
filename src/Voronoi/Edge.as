package Voronoi 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	/**
	 * DCEL data structure for storing edges
	 * 
	 * @author Switchbreak
	 */
	public class Edge 
	{
		
		public var origin:Point;
		public var face:Cell;
		public var twin:Edge;
		public var clipEdge:int = -1;
		
		/**
		 * Clip the line to the edges of the viewport
		 * 
		 * @param	viewport
		 * Rectangle defining the viewport
		 */
		public function LiangBarsky( viewport:Rectangle ):Boolean
		{
			// Declare working variables
			var u1:Number = 0.0;
			var u2:Number = 1.0;
			var xDelta:Number = twin.origin.x - origin.x;
			var yDelta:Number = twin.origin.y - origin.y;
			var p:Number, q:Number, u:Number;
			
			// Loop through 4 edges of viewport
			for (var edge:int = 0; edge < 4; edge++)
			{
				switch( edge )
				{
					case 0:
						// Left
						p = -xDelta;
						q = origin.x - viewport.left;
						break;
					case 1:
						// Right
						p = xDelta;
						q = viewport.right - origin.x;
						break;
					case 2:
						// Top
						p = -yDelta;
						q = origin.y - viewport.top;
						break;
					case 3:
						// Bottom
						p = yDelta;
						q = viewport.bottom - origin.y;
						break;
				}
				
				// Check for parallel lines outside of the viewport
				if (p == 0 && q < 0)
					return false;
				
				// If p < 0, line is outside->inside, if p > 0, line is inside->outside
				if (p < 0)
				{
					// Get intersection point
					u = q / p;
					
					// Clip line to maximum intersection point found
					if ( u > u1 )
					{
						u1 = u;
						clipEdge = edge;
					}
				}
				else if (p > 0)
				{
					// Get intersection point
					u = q / p;
					
					// Clip line to minimum intersection point found
					if ( u < u2 )
					{
						u2 = u;
						twin.clipEdge = edge;
					}
				}
				
				// If u1 > u2, line falls outside of clipping window
				if( u1 > u2 )
					return false;
			}
			
			// Line is inside viewport, clip if necessary and return true
			if ( u2 < 1.0 )
			{
				twin.origin.x = origin.x + u2 * xDelta;
				twin.origin.y = origin.y + u2 * yDelta;
			}
			if ( u1 > 0.0 )
			{
				origin.x += u1 * xDelta;
				origin.y += u1 * yDelta;
			}
			
			return true;
		}
		
	}

}