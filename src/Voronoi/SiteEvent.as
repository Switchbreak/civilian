package Voronoi 
{
	/**
	 * Site events for the event queue for Fortune's sweep-line algorithm
	 * 
	 * @author Switchbreak
	 */
	internal class SiteEvent extends FortuneEvent 
	{
		
		public var site:Cell;
		
		/**
		 * Create site event from a cell
		 * 
		 * @param	cell
		 * Cell containing position of the site
		 */
		public function SiteEvent( cell:Cell )
		{
			site = cell;
			point = cell.site;
		}
		
	}

}