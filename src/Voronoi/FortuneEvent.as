package Voronoi 
{
	import flash.geom.Point;
	
	/**
	 * Events for the event queue for Fortune's sweep-line algorithm
	 * 
	 * @author Switchbreak
	 */
	internal class FortuneEvent 
	{
		
		public var point:Point;
		
	}

}