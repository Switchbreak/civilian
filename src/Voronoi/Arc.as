package Voronoi 
{
	/**
	 * Arc definition for the beach-line in Fortune's sweep-line algorithm
	 * 
	 * @author Switchbreak
	 */
	internal class Arc 
	{
		
		public var site:Cell;
		public var circleEvent:CircleEvent;
		public var leftEdge:Edge;
		public var rightEdge:Edge;
		
		public var next:Arc;
		public var prev:Arc;
		
	}

}