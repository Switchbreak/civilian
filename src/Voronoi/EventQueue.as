package Voronoi 
{
	/**
	 * Event queue for Fortune's line-sweep algorithm
	 * 
	 * @author Switchbreak
	 */
	internal class EventQueue 
	{
		
		private static const EPSILON:Number = 1e-4;
		
		private var eventQueue:Vector.<FortuneEvent>;
		
		/**
		 * Initialize the event queue
		 */
		public function EventQueue()
		{
			eventQueue = new Vector.<FortuneEvent>();
		}
		
		/**
		 * Insert an event into the queue, sorting it by y
		 * 
		 * @param	event
		 * Event to insert
		 */
		public function Insert( event:FortuneEvent ):void
		{
			for ( var i:int = 0; i < eventQueue.length; i++ )
			{
				if ( event is SiteEvent && Math.abs( eventQueue[i].point.y - event.point.y ) < 5 && Math.abs( eventQueue[i].point.x - event.point.x ) < 5 )
					return;
				if ( eventQueue[i].point.y == event.point.y && eventQueue[i].point.x > event.point.x )
					break;
				if ( eventQueue[i].point.y > event.point.y )
					break;
			}
			
			eventQueue.splice( i, 0, event );
		}
		
		/**
		 * Removes the event at the front of the queue and returns it
		 * 
		 * @return
		 * Event with the lowest y-value
		 */
		public function Pull():FortuneEvent
		{
			return eventQueue.shift();
		}
		
		/**
		 * Remove an event from the queue
		 * 
		 * @param	event
		 * Event to remove
		 */
		public function RemoveEvent( event:FortuneEvent ):void
		{
			eventQueue.splice( eventQueue.indexOf( event ), 1 );
		}
		
	}

}