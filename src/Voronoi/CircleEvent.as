package Voronoi 
{
	import flash.geom.Point;
	/**
	 * Circle events for the event queue for Fortune's sweep-line algorithm
	 * 
	 * @author Switchbreak
	 */
	internal class CircleEvent extends FortuneEvent 
	{
		
		public var arc:Arc;
		public var center:Point;
		
	}

}