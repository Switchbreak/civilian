package Voronoi 
{
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	/**
	 * ...
	 * @author Switchbreak
	 */
	public class Tesselator 
	{
		
		private static const EPSILON:Number = 1e-4;
		
		private var _edges:Vector.<Edge>;
		private var _cells:Vector.<Cell>;
		
		private var root:Arc;
		private var eventQueue:EventQueue;
		private var viewport:Rectangle;
		
		/**
		 * Initialize tesselator object with a list of sites
		 * 
		 * @param	sites
		 * List of voronoi sites
		 * @param	setViewport
		 * Rectangle describing the edge of the viewport
		 */
		public function Tesselator( sites:Vector.<Point>, setViewport:Rectangle ) 
		{
			viewport = setViewport;
			
			_edges = new Vector.<Edge>();
			_cells = new Vector.<Cell>( sites.length );
			
			for ( var i:int = 0; i < sites.length; i++ )
			{
				_cells[i] = new Cell();
				_cells[i].site = sites[i];
				_cells[i].edges = new Vector.<Edge>();
			}
		}
		
		/**
		 * Run Fortune's sweep-line algorithm to generate voronoi diagram
		 */
		public function Tesselate():void
		{
			// Create event queue
			eventQueue = new EventQueue();
			
			// Fill event queue with list of sites
			for each( var cell:Cell in _cells )
			{
				eventQueue.Insert( new SiteEvent( cell ) );
			}
			
			// Loop through all events in event queue
			var event:FortuneEvent = eventQueue.Pull();
			while ( event != null )
			{
				if( event is SiteEvent )
					HandleSiteEvent( event as SiteEvent );
				else if ( event is CircleEvent )
					HandleCircleEvent( event as CircleEvent );
				
				event = eventQueue.Pull();
			}
			
			// Clip dangling edges against the bottom of the viewport
			// Prime the loop
			var iterator:Arc = root;
			
			// Loop through arcs left to right until you pass the x-coordinate
			while ( iterator != null )
			{
				iterator = iterator.next;
				
				if ( iterator != null && iterator.leftEdge )
				{
					// Find the intersection on the left side of the arc
					var endPoint:Point = new Point( 0, 1600 );
					endPoint.x = ArcIntersectionX( iterator.prev, iterator, endPoint.y );
					endPoint.y = GetIntersectY( iterator.prev, endPoint );
					
					iterator.leftEdge.origin = endPoint;
				}
			}
			
			// Clip edges to viewport
			ClipEdges();
		}
		
		/**
		 * Process a site event in the sweep-line
		 * 
		 * @param	event
		 * Site event to handle
		 */
		private function HandleSiteEvent( event:SiteEvent ):void
		{
			// If this is the first parabola being added to the list, set the root
			if ( root == null )
			{
				root = new Arc();
				root.circleEvent = null;
				root.site = event.site;
				root.next = null;
				root.prev = null;
				
				return;
			}
			
			// Find the arc on the beachline that is over this site's x-coordinate
			var intersection:Arc = GetArcAtPoint( event.point );
			// Remove the circle event on the arc if it exists
			if ( intersection.circleEvent != null )
			{
				eventQueue.RemoveEvent( intersection.circleEvent );
				intersection.circleEvent = null;
			}
			
			// Add new arc
			var newArc:Arc = new Arc();
			newArc.site = event.site;
			
			if ( intersection.next == null && intersection.site.site.y == event.point.y )
			{
				// Insert new arc and duplicated intersection arc into the beach line
				newArc.prev = intersection;
				intersection.next = newArc;
				
				// Add edges
				var leftEdge:Edge = new Edge();
				var rightEdge:Edge = new Edge();
				
				leftEdge.origin = new Point( (intersection.site.site.x + event.point.x) / 2, 0 );
				leftEdge.face = intersection.site;
				leftEdge.twin = rightEdge;
				leftEdge.face.edges.push( leftEdge );
				
				rightEdge.origin = new Point( (intersection.site.site.x + event.point.x) / 2, event.point.y );
				rightEdge.face = event.site;
				rightEdge.twin = leftEdge;
				edges.push( rightEdge );
				rightEdge.face.edges.push( rightEdge );
				
				// Associate edges with arc intersections
				intersection.rightEdge = rightEdge;
				newArc.leftEdge = rightEdge;
				
				// Check for new potential circle events
				AddPotentialCircleEvents( intersection, event.point.y );
			}
			else if ( intersection.prev && Math.abs( ArcIntersectionX( intersection.prev, intersection, event.point.y ) - event.point.x ) < EPSILON )
			{
				// Remove circle event from previous arc
				if ( intersection.prev.circleEvent != null )
				{
					eventQueue.RemoveEvent( intersection.circleEvent );
					intersection.circleEvent = null;
				}
				
				// Insert new arc and duplicated intersection arc into the beach line
				intersection.prev.next = newArc;
				newArc.prev = intersection.prev;
				newArc.next = intersection;
				intersection.prev = newArc;
				
				var edgeY:Number = GetIntersectY( intersection, event.point );
				intersection.rightEdge.origin.x = event.point.x;
				intersection.rightEdge.origin.y = edgeY;
				
				// Add 2 new edges (4 half-edges)
				leftEdge = new Edge();
				rightEdge = new Edge();
				
				leftEdge.origin = new Point( event.point.x, edgeY );
				leftEdge.face = event.site;
				leftEdge.twin = new Edge();
				leftEdge.twin.twin = leftEdge;
				leftEdge.twin.face = newArc.prev.site;
				leftEdge.twin.origin = new Point( event.point.x, edgeY );
				leftEdge.twin.face.edges.push( leftEdge.twin );
				edges.push( leftEdge );
				leftEdge.face.edges.push( leftEdge );
				
				rightEdge.origin = new Point( event.point.x, edgeY );
				rightEdge.face = intersection.site;
				rightEdge.twin = new Edge();
				rightEdge.twin.twin = rightEdge;
				rightEdge.twin.face = event.site;
				rightEdge.twin.origin = new Point( event.point.x, edgeY );
				rightEdge.twin.face.edges.push( rightEdge.twin );
				edges.push( rightEdge );
				rightEdge.face.edges.push( rightEdge );
				
				// Associate edges with arc intersections
				newArc.leftEdge = leftEdge;
				newArc.rightEdge = rightEdge;
				newArc.prev.rightEdge = leftEdge;
				newArc.next.leftEdge = rightEdge;
				
				// Check for new potential circle events
				AddPotentialCircleEvents( newArc.prev, event.point.y );
				AddPotentialCircleEvents( newArc, event.point.y );
				AddPotentialCircleEvents( newArc.next, event.point.y );
			}
			else
			{
				// Duplicate intersected arc
				var intersectRight:Arc = new Arc();
				intersectRight.circleEvent = null;
				intersectRight.site = intersection.site;
				
				// Insert new arc and duplicated intersection arc into the beach line
				newArc.prev = intersection;
				newArc.next = intersectRight;
				intersectRight.prev = newArc;
				intersectRight.next = intersection.next;
				
				if ( intersection.next )
					intersection.next.prev = intersectRight;
				intersection.next = newArc;
				
				// Add edges
				edgeY = GetIntersectY( intersection, event.point );
				leftEdge = new Edge();
				rightEdge = new Edge();
				
				leftEdge.origin = new Point( event.point.x, edgeY );
				leftEdge.face = event.site;
				leftEdge.twin = rightEdge;
				leftEdge.face.edges.push( leftEdge );
				rightEdge.origin = new Point( event.point.x, edgeY );
				rightEdge.face = intersection.site;
				rightEdge.twin = leftEdge;
				edges.push( rightEdge );
				rightEdge.face.edges.push( rightEdge );
				
				// Associate edges with arc intersections
				newArc.leftEdge = leftEdge;
				newArc.rightEdge = rightEdge;
				intersectRight.leftEdge = rightEdge;
				intersectRight.rightEdge = intersection.rightEdge;
				intersection.rightEdge = leftEdge;
				
				// Check for new potential circle events
				AddPotentialCircleEvents( intersection, event.point.y );
				AddPotentialCircleEvents( intersectRight, event.point.y );
			}
		}
		
		/**
		 * Process a circle event in the sweep-line
		 * 
		 * @param	event
		 * Circle event to handle
		 */
		private function HandleCircleEvent( event:CircleEvent ):void
		{
			// Get arcs around circle event
			var arc:Arc = event.arc;
			var leftArc:Arc = arc.prev;
			var rightArc:Arc = arc.next;
			
			// Remove circle events from the left and right arcs if they exist
			if ( leftArc.circleEvent != null )
			{
				eventQueue.RemoveEvent( leftArc.circleEvent );
				leftArc.circleEvent = null;
			}
			if ( rightArc.circleEvent != null )
			{
				eventQueue.RemoveEvent( rightArc.circleEvent );
				rightArc.circleEvent = null;
			}
			
			// Remove arc from the beach line
			leftArc.next = rightArc;
			rightArc.prev = leftArc;
			
			// Finish the edges
			arc.leftEdge.origin.x = event.center.x;
			arc.leftEdge.origin.y = event.center.y;
			arc.rightEdge.origin.x = event.center.x;
			arc.rightEdge.origin.y = event.center.y;
			
			// Add edges
			var leftEdge:Edge = new Edge();
			var rightEdge:Edge = new Edge();
			
			leftEdge.origin = new Point( event.center.x, event.center.y  );
			leftEdge.face = leftArc.site;
			leftEdge.twin = rightEdge;
			leftEdge.face.edges.push( leftEdge );
			rightEdge.origin = new Point( event.center.x, event.center.y );
			rightEdge.face = rightArc.site;
			rightEdge.twin = leftEdge;
			edges.push( rightEdge );
			rightEdge.face.edges.push( rightEdge );
			
			leftArc.rightEdge = rightEdge;
			rightArc.leftEdge = rightEdge;
			
			// Check for new potential circle events
			AddPotentialCircleEvents( leftArc, event.point.y );
			AddPotentialCircleEvents( rightArc, event.point.y );
		}
		
		/**
		 * Check three arcs to the left and right of 
		 * @param	arc
		 */
		private function AddPotentialCircleEvents( arc:Arc, sweepY:Number ):void
		{
			var leftArc:Arc = arc.prev;
			var rightArc:Arc = arc.next;
			
			// Check that we have three existing arcs with unique sites to generate a circle event
			if ( leftArc == null || rightArc == null || leftArc.site == rightArc.site )
				return;
			
			// Check for a circumcircle described by the three arc sites
			var center:Point = GetCircumcircle( leftArc.site.site, arc.site.site, rightArc.site.site );
			if ( center == null )
				return;
			
			// Get the radius of the circle, and the calculate the position of the bottom tangent
			var radius:Number = Point.distance( arc.site.site, center );
			if (sweepY - (center.y + radius) > EPSILON ) //( center.y + radius < sweepY )
				return;
			
			// Add new circle event to the queue
			var circleEvent:CircleEvent = new CircleEvent();
			circleEvent.arc = arc;
			circleEvent.point = new Point( center.x, center.y + radius );
			circleEvent.center = center;
			arc.circleEvent = circleEvent;
			eventQueue.Insert( circleEvent );
		}
		
		/**
		 * Gets the circumcircle described by three points
		 * 
		 * @param	point1
		 * Points describing the circumcircle
		 * @param	point2
		 * Points describing the circumcircle
		 * @param	point3
		 * Points describing the circumcircle
		 * @return
		 * Either the center of the circumcircle or null if none is found
		 */
		private function GetCircumcircle( a:Point, b:Point, c:Point ):Point
		{
			// Check that bc is a "right turn" from ab.
			if ( (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y) < 0)
				return null;
			
			var d:Number = 2 * (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y));
			
			// Check if points are collinear
			if ( d == 0 )
				return null;
			
			var centerPoint:Point = new Point();
			
			centerPoint.x = ((a.y * a.y + a.x * a.x) * (b.y - c.y) + (b.y * b.y + b.x * b.x) * (c.y - a.y) + (c.y * c.y + c.x * c.x) * (a.y - b.y)) / d;
			centerPoint.y = ((a.y * a.y + a.x * a.x) * (c.x - b.x) + (b.y * b.y + b.x * b.x) * (a.x - c.x) + (c.y * c.y + c.x * c.x) * (b.x - a.x)) / d;
			
			return centerPoint;
		}
		
		/**
		 * Returns the arc on the beach line above a given x-coordinate
		 * 
		 * @param	x
		 * X-coordinate to raycast upwards
		 * @return
		 * Arc that intersects with the raycast
		 */
		private function GetArcAtPoint( point:Point ):Arc
		{
			// Prime the loop
			var arcMinX:Number;
			var iterator:Arc = root;
			
			// Loop through arcs left to right until you pass the x-coordinate
			while( iterator != null )
			{
				// If there is another arc to the right go to it
				if ( iterator.next )
				{
					iterator = iterator.next;
					
					// Find the intersection on the left side of the arc
					arcMinX = ArcIntersectionX( iterator.prev, iterator, point.y );
				}
				else
				{
					// This is the rightmost iterator and the ray is to the right of its left edge, return it
					return iterator;
				}
				
				// If the left edge of the current arc has passed the point, the previous arc is the one under it
				if ( arcMinX - point.x > EPSILON ) // arcMinX > point.x )
					return iterator.prev;
			}
			
			return iterator;
		}
		
		/**
		 * Returns the x-coordinate of the intersection of two arcs
		 * 
		 * @param	leftArc
		 * Leftmost arc
		 * @param	rightArc
		 * Rightmost arc
		 * @return
		 * X-coordinate of intersection
		 */
		private function ArcIntersectionX( leftArc:Arc, rightArc:Arc, sweepLineY:Number ):Number
		{
			var leftSite:Point = leftArc.site.site;
			var rightSite:Point = rightArc.site.site;
			
			// Check for parabola in degenerate case where focus is on directrix
			if ( rightSite.y == sweepLineY )
				return rightSite.x;
			if ( leftSite.y == sweepLineY )
				return leftSite.x;
			
			// If noth parabolas have same distance to directrix, break point is midway
			if ( rightSite.y == leftSite.y )
				return (rightSite.x + leftSite.x) / 2;
			
			var dp:Number = 2 * (sweepLineY - leftSite.y);
			var a1:Number = 1 / dp;
			var b1:Number = -2 * leftSite.x / dp;
			var c1:Number = sweepLineY + dp / 4 + leftSite.x * leftSite.x / dp;
			
			dp = 2 * (sweepLineY - rightSite.y);
			var a2:Number = 1 / dp;
			var b2:Number = -2 * rightSite.x / dp;
			var c2:Number = sweepLineY + dp / 4 + rightSite.x * rightSite.x / dp;
			
			var a:Number = a1 - a2;
			var b:Number = b1 - b2;
			var c:Number = c1 - c2;
			
			var disc:Number = b * b - 4 * a * c;
			var x1:Number = (-b + Math.sqrt(disc)) / (2 * a);
			var x2:Number = (-b - Math.sqrt(disc)) / (2 * a);
			
			var ry:Number;
			if ( leftSite.y > rightSite.y )
				ry = Math.max(x1, x2);
			else
				ry = Math.min(x1, x2);
			
			return ry;
		}
		
		/**
		 * Gets the intersection point between ray from the sweep-line and an arc
		 * 
		 * @param	arc
		 * Arc to intersect
		 * @param	point
		 * Point containing the spot on the sweep line to send the ray from
		 * @return
		 * Y-coordinate of intersection
		 */
		private function GetIntersectY( arc:Arc, point:Point ):Number
		{
			var site:Point = arc.site.site;
			
			/*var dp:Number = 2 * (site.y - point.y);
			var b1:Number = -2 * site.x / dp;
			var c1:Number = point.y + dp / 4 + site.x * site.x / dp;
			
			return ( point.x * point.x / dp + b1 * point.x + c1);*/
			
			return ((point.x - site.x) * (point.x - site.x) + site.y * site.y - point.y * point.y) / (2 * (site.y - point.y));
		}
		
		/**
		 * Clip edges to viewport
		 */
		private function ClipEdges():void
		{
			if ( edges.length == 0 )
				return;
			
			// Clip edges to screen using Liang-Barsky algorithm
			for ( var i:int = 0; i < edges.length; i++ )
			{
				// Clip edge to viewport
				if ( !edges[i].LiangBarsky( viewport ) )
				{
					// If clip returns false, edge is entirely outside of viewport, remove it from edge list
					edges[i].face.edges.splice( edges[i].face.edges.indexOf( edges[i] ), 1 );
					edges[i].twin.face.edges.splice( edges[i].twin.face.edges.indexOf( edges[i].twin ), 1 );
					edges[i].twin.twin = null;
					edges[i].twin = null;
					edges.splice( i, 1 );
					i--;
				}
				else if ( edges[i].clipEdge > -1 || edges[i].twin.clipEdge > -1 )
				{
					edges[i].face.border = true;
					edges[i].twin.face.border = true;
				}
			}
			/*
			// Loop through all cells and sort their edge lists in counter-clockwise order
			for each( var cell:Cell in cells )
			{
				//cell.edges.sort( edgeCompare );
				
				if ( cell.border )
				{
					for ( i = 0; i < cell.edges.length; i++ )
					{
						if ( cell.edges[i].clipEdge > -1 )
						{
							var nextEdge:Edge = cell.edges[(i + 1) % cell.edges.length];
							if ( nextEdge.clipEdge == -1 )
								nextEdge = nextEdge.twin;
							
							if ( nextEdge != null &&  nextEdge.clipEdge == cell.edges[i].clipEdge )
							{
								// Add edge (2 half-edges)
								var leftEdge:Edge = new Edge();
								var rightEdge:Edge = new Edge();
								
								leftEdge.origin = cell.edges[i].origin;
								leftEdge.face = cell;
								leftEdge.twin = rightEdge;
								//leftEdge.face.edges.push( leftEdge );
								rightEdge.origin = nextEdge.origin;
								rightEdge.face = cell;
								rightEdge.twin = leftEdge;
								edges.push( rightEdge );
								//rightEdge.face.edges.push( rightEdge );
							}
						}
					}
				}
			}
			*/
		}
		
		private function edgeCompare( edge1:Edge, edge2:Edge ):Number
		{
			var a1:Number = Math.atan2(edge1.origin.y - edge1.face.site.y, edge1.origin.x - edge1.face.site.x);
			var a2:Number = Math.atan2(edge2.origin.y - edge2.face.site.y, edge2.origin.x - edge2.face.site.x);
			
			return a2 - a1;
		}
		
		//
		// Properties
		//
		
		public function get edges():Vector.<Edge>
		{
			return _edges;
		}
		
		public function get cells():Vector.<Cell>
		{
			return _cells;
		}
		
	}

}