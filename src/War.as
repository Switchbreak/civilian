package  
{
	import net.flashpunk.FP;
	import Types.*;
	
	/**
	 * Object containing information on ongoing wars
	 * 
	 * @author Switchbreak
	 */
	public class War 
	{
		
		public var invader:Country;
		public var invaded:Country;
		public var warLength:int = 0;
		public var townsTaken:int = 0;
		public var ended:Boolean = false;
		
		private var sim:Sim;
		
		/**
		 * Start a new war
		 * 
		 * @param	setInvader
		 * Country invading
		 * @param	setInvaded
		 * Country being invaded
		 */
		public function War( setInvader:Country, setInvaded:Country, setSim:Sim )
		{
			// Set war values
			invader = setInvader;
			invaded = setInvaded;
			sim = setSim;
			
			// Add references to invading country and invaded country
			invader.wars.push( this );
			invaded.wars.push( this );
		}
		
		/**
		 * Capture a town in the invading country
		 * 
		 * @return
		 * Returns the town that has been captured
		 */
		public function CaptureTown():Town
		{
			// Build a list of towns not owned by the invading country as potential targets
			var targetList:Vector.<Town> = new Vector.<Town>();
			for each( var town:Town in invaded.towns )
			{
				if ( town.occupiedBy != this )
					targetList.push( town );
			}
			
			// Sanity check to avoid crashing if no towns are not occupied by the invader
			if ( targetList.length == 0 )
			{
				trace( "Warning: CaptureTown() called when all towns occupied" );
				return null;
			}
			
			// Choose a random target town from the list of potentials
			var target:Town = targetList[FP.rand( targetList.length )];
			
			// Check if the target is being held by another invasion
			if ( target.occupiedBy != null )
			{
				// If the town is held by another invasion, take it away
				target.occupiedBy.Retreat( target );
			}
			
			// Take the town
			target.occupiedBy = this;
			townsTaken++;
			
			// If the town is being defended by the invader, check if it is
			// the last town held by the invader and if so, disable
			// defending forces
			invaded.defending = false;
			for each( town in invaded.towns )
			{
				if ( town.occupiedBy == null )
					invaded.defending = true;
			}
			
			// If all defense capabilities were lost, retreat from all
			// current foreign invasions
			if ( !invaded.defending )
			{
				// End all ongoing invasions the country was engaged in
				for each( var war:War in invaded.wars )
				{
					if ( war.invader == invaded )
					{
						while ( !war.ended )
							war.Retreat();
					}
				}
			}
				
			// If we have captured all towns in the invaded country, the war is
			// a victory
			if ( townsTaken >= invaded.towns.length )
			{
				// Reset country to peaceful state, occupied by the invader
				invaded.occupiedBy = invader.occupiedBy;
				invaded.defending = true;
				
				for each( town in invaded.towns )
				{
					town.occupiedBy = null;
				}
				
				EndWar();
			}
			
			return target;
		}
		
		/**
		 * Retreat from an occupied town
		 * 
		 * @param	town
		 * Town to retreat from
		 * @return
		 * Returns the town that has been captured
		 */
		public function Retreat( town:Town = null ):Town
		{
			// If no town was selected to retreat from, pick one at random
			if ( town == null )
			{
				// Build list of currently occupied towns
				var occupiedTowns:Vector.<Town> = new Vector.<Town>();
				for each( var invadedTown:Town in invaded.towns )
				{
					if ( invadedTown.occupiedBy == this )
						occupiedTowns.push( invadedTown );
				}
				
				// Sanity check to avoid crashing if no towns are occupied
				if ( occupiedTowns.length == 0 )
				{
					trace( "Warning: Retreat() called when no towns occupied" );
					return null;
				}
				
				// Choose a random town from the list of occupied towns
				town = occupiedTowns[FP.rand( occupiedTowns.length )];
			}
			
			// Retreat from town
			town.occupiedBy = null;
			townsTaken--;
			
			invaded.defending = true;
			
			// If that was the last town held, the invasion has been defeated
			if ( townsTaken <= 0 )
			{
				EndWar();
			}
			
			return town;
		}
		
		/**
		 * Removes all references to a war
		 */
		public function EndWar():void
		{
			ended = true;
			
			sim.RemoveWar( this );
			invader.wars.splice( invader.wars.indexOf( this ), 1 );
			invaded.wars.splice( invaded.wars.indexOf( this ), 1 );
		}
		
	}

}